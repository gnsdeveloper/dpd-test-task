<?php

$requestDataExample = [
    'OrderAction' => 'startOrder',
    'OrderSettings'=> [
        'ShipDate' => date("Y-m-d"),
        'LabelSize' => 'PDF_A4',
        'LabelStartPosition' => 'UpperLeft'
    ],
    'OrderDataList' => [
        '0' =>  [
            'ShipAddress' => [
                'Company' => 'Mustermann AG',
                'Salutation' => 'Mr',
                'Name' => 'Max Mustermann',
                'Street' => 'Luitpoldstr.',
                'HouseNo' => '3',
                'ZipCode' => '97318',
                'City' => 'Kitzingen',
                'Country' => 'DEU',
                'State' => '',
                'Phone' => '+49 171 111 444',
                'Mail' => 'm.mustermann@mustermann.com'
            ],
            'ParcelData' => [
                'YourInternalID' => '123',
                'Content' => 'Smartphone',
                'Weight' => '13.5',
                'Reference1' => 'Customer email',
                'Reference2' => 'Order number',
                'ShipService' => 'Classic'
            ],
        ],
    ],
];
$auction = '';

$requestDataExample = [
    'OrderAction' => 'startOrder',
    'OrderSettings'=> [
        'ShipDate' => date("Y-m-d"),
        'LabelSize' => 'PDF_A4',
        'LabelStartPosition' => 'UpperLeft'
    ],
    'OrderDataList' => [
        '0' =>  [
            'ShipAddress' => [
                'Company' => 'Mustermann AG',
                'Salutation' => 'Mr',
                'Name' => 'Max Mustermann',
                'Street' => 'Luitpoldstr.',
                'HouseNo' => '3',
                'ZipCode' => '97318',
                'City' => 'Kitzingen',
                'Country' => 'DEU',
                'State' => '',
                'Phone' => '+49 171 111 444',
                'Mail' => 'm.mustermann@mustermann.com'
            ],
            'ParcelData' => [
                'YourInternalID' => '123',
                'Content' => 'Smartphone',
                'Weight' => '13.5',
                'Reference1' => 'Customer email',
                'Reference2' => 'Order number',
                'ShipService' => 'Classic'
            ],
        ],
    ],
];