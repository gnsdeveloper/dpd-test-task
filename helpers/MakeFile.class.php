<?php

class MakeFile
{
    public function __construct($data, $filename = 'test.pdf')
    {
        $this->makeFile($data, $filename);
    }

    public function makeFile($data, $filename = 'test.pdf')
    {
        $decoded = base64_decode($this->getSpecificElementLabel($data));
        file_put_contents($filename, $decoded);
    }

    public function getSpecificElementLabel($data)
    {
        return $data['LabelResponse']['LabelPDF'];
    }
}
