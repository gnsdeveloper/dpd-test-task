<?php

/**
 * Responsible for building forms
 *
 * @param array $elements renderable array containing form elements
 * @return void
 */
class DataStructure
{
    public $elements;

    public $form_number = 1;

    public function __construct($elements)
    {
        $this->elements = $elements;
    }

    /**
     * DataStructure class method to build a form from an array
     *
     * @return string $output contains the form as HTML
     */
    public function buildSpecificFormStructure()
    {
        $output = '';

        // For multiple forms, create a counter.
        $this->form_number++;

        // Loop through each form element and render it.
        foreach ($this->getSpecificNeededElementData() as $name => $elements) {
            $input ='<td>'.$name.'</td><td>'.$elements.'</td><td>'.$elements.'</td>';
            $output .= '<tr>' . $input . '</tr>';
        }

        // Wrap a form around the inputs.
        $output = '
            <form action="/makepdf.php" method="post" id="form1">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                          <th></th>
                          <th>Billing</th>
                          <th>Shipping</th>
                        </tr>
                    </thead>
                    <tbody>
                        ' . $output . '
                    </tbody>
                </table>
            </form>'
        ;

        return $output;
    }

    protected function getSpecificNeededElementData()
    {
        return $this->elements['OrderDataList']['0']['ShipAddress'];
    }
}
