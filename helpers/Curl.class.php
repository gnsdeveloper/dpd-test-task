<?php

require_once __DIR__ . "/../config/parameters.php";

class CurlHelperService
{
    private $url = CURL_BASE_URL;

    private $data = [];

    private $curl;

    public function __construct($data = [])
    {
        $this->data = $data;
        $this->curl = curl_init();
    }

    /**
     * @param $url
     * @return $this
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * @return resource
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Get curl data
     *
     * @param array $data
     * @param bool $jsonResult
     * @return bool|mixed
     * @throws Exception
     */
    public function getParsingData($data = [], $jsonResult = true)
    {
        $result = $this->execute($data);

        if (key_exists('response', $result) && !$result['response']) {
            throw new Exception($result['message']);
        }

        if (!$jsonResult) {
            $result = json_decode($result, true);
        }

        return $result;
    }

    /**
     * Executing Curl
     *
     * @param array $data
     * @return bool|mixed
     */
    public function execute($data = [])
    {
        $this->setCurlOptUrl();
        $this->setRequiredHeaders();
        $this->setRequiredOptions();
        $this->setCustomPostData($data);
        $result = curl_exec($this->curl);

        if (!$result) {
            return false;
        } else {
            $response = json_decode($result, true);

            if ($response['ErrorDataList'][0]['ErrorID']) {
                return [
                    'response' => false,
                    'message' => $response['ErrorDataList'][0]['ErrorMsgShort'],
                ];
            }
        }

        curl_close($this->curl);

        return $result;
    }

    /**
     * Set url for executing Curl
     *
     * @param string $url - type of api call
     */
    protected function setCurlOptUrl($url = '')
    {
        $url = $url ?: $this->url;
        curl_setopt($this->curl, CURLOPT_URL, $url);
    }

    /**
     * Set specific headers for Curl for data providers
     */
    protected function setRequiredHeaders()
    {
        $headers = [];
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Version: 100';
        $headers[] = 'Language: de_DE';
        $headers[] = 'PartnerCredentials-Name: '.CURL_PARTNER_CREDENTIALS_NAME;
        $headers[] = 'PartnerCredentials-Token: '.CURL_PARTNER_CREDENTIALS_TOKEN;
        $headers[] = 'UserCredentials-cloudUserID: '.CURL_USER_CREDENTIALS_CLOUD_USER_ID;
        $headers[] = 'UserCredentials-Token: '.CURL_USER_CREDENTIALS_TOKEN;

        curl_setopt($this->curl, CURLOPT_HTTPHEADER, $headers);
    }

    /**
     * Set required options for Curl for get data
     */
    protected function setRequiredOptions()
    {
        curl_setopt($this->curl, CURLOPT_POST, true);
        curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($this->curl, CURLOPT_SSL_VERIFYHOST, false);
    }

    /**
     * Set post parameters for receive data
     *
     * @param array $data
     */
    protected function setCustomPostData($data = [])
    {
        $data = count($data) ? $data : $this->data;
        curl_setopt($this->curl, CURLOPT_POSTFIELDS, $data);
    }
}
