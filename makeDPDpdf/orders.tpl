{if $reserve || 1}
{literal}
<script type="text/javascript">
function show_order_stock(number, btn) {
	btn.disabled = true;
		 JsHttpRequest.query(
		            'js_backend.php', // backend
		            {
		                // pass a text value 
						'fn': 'get_orders',
						'stock': 1,
						'reserve': {/literal}{$reserve|default:0}{literal},
		                'number': number
		            },
		            // Function is called when an answer arrives. 
		            function(result, errors) {
		                // Write the answer.
						document.getElementById('divorders').innerHTML = result['res'];
						btn.disabled = false;
		            },
		            true  // do not disable caching
		        );
}

function changeReserveWare(warehouse_id, id) {
		 JsHttpRequest.query(
		            'js_backend.php', // backend
		            {
		                // pass a text value 
						'fn': 'change_order_warehouse',
		                'reserve_warehouse_id': warehouse_id,
		                'id': id
		            },
		            // Function is called when an answer arrives. 
		            function(result, errors) {
		                // Write the answer.
		            },
		            true  // do not disable caching
		        );
}

function checkAllId(obj) {
     form = obj.form;
	 id = obj.id;
     for (i=0; i<form.length; i++) {
	     key = new String(form.elements[i].id);
	     if (key==id) { 
		 	form.elements[i].checked = obj.checked;
		}      
     }
}

function valueAllId(obj) {
     form = obj.form;
	 id = obj.id;
     for (i=0; i<form.length; i++) {
	     key = new String(form.elements[i].id);
	     if (key==id) { 
		 	form.elements[i].value = obj.value;
		}      
     }
}

function checkAll(name, obj) {
     form = obj.form;
     for (i=0; i<form.length; i++) {
	     key = new String(form.elements[i].name);
	     if (key==name) { 
		 	form.elements[i].checked = obj.checked;
		}      
     }
}

function changeAllReserveWares(obj, name, value) {
  for (i=0; i<obj.length; i++) {
	key = new String(obj.elements[i].name);
	strname = new String(name);
	if (key.indexOf(name+'[')==0) {
		obj.elements[i].value = value;  
		id = key.substring(strname.length+1,key.length-1);
		changeReserveWare(value, id);
	}	
  }
};
function changeAllSendWares(obj, name, value) {
  for (i=0; i<obj.length; i++) {
	key = new String(obj.elements[i].name);
	strname = new String(name);
	if (key.indexOf(name+'[')==0) {
		obj.elements[i].value = value;  
		id = key.substring(strname.length+1,key.length-1);
		changeSendWare(value, id);
	}	
  }
};
function changeAllSendWaresEmpty(obj, name, value) {
  for (i=0; i<obj.length; i++) {
	key = new String(obj.elements[i].name);
	strname = new String(name);
	if (key.indexOf(name+'[')==0) {
		alert(obj.elements[i].value);
		obj.elements[i].value = value;  
		id = key.substring(strname.length+1,key.length-1);
		changeSendWare(value, id);
	}	
  }
};
</script>
{/literal} 
{/if}

	<table border="1">
<tr><td colspan="10" align="center"><b>Articles</b></td></tr>
	<tr>
	<td>ID</td>
	<td>Article</td>
	<td>Total Quantity</td>
	<td>Quantity to process</td>
	<td>Tracking numbers</td>
	<td>Barcodes</td>
	<td>Pack
		{if !$loggedUser->mobile_only}<input type="checkbox" onClick="checkAll('pack[]', this)">{/if}
	</td>
	<td>Shipped from Warehouse
	{if $auction->delivery_date_real=='0000-00-00 00:00:00'}
		<select id="AllSendWares" name="AllSendWares" onChange="changeAllSendWares(this.form, 'send_warehouse_id', this.value);">
	    <option value="">---</option>
	    {html_options options=$warehouses2ship selected=$AllSendWares}
	    </select>
		<br><input type="button" id="stock_btn" value="Activate stock" onClick="show_order_stock('{$auction->auction_number}/{$auction->txnid}', this)"/>
	{/if}
	</td>
{if $reserve}
	<td>Reserve at Warehouse
	{if $auction->delivery_date_real=='0000-00-00 00:00:00'}
		<select onChange="changeAllReserveWares(this.form, 'reserve_warehouse_id', this.value);">
	    <option value="">---</option>
	    {html_options options=$warehouses2ship}
	    </select>
		<br><input type="button" id="stock_btn" value="Activate stock" onClick="show_order_stock('{$auction->auction_number}/{$auction->txnid}', this)"/>
	{/if}
	</td>
{/if}
{if $pickup}
	<td>State</td>
	<td>Ready to pickup<input type="checkbox" onClick="checkAll('ready2pickup[]', this)"></td>
	<td>State</td>
	<td>Picked Up
		{if !$loggedUser->mobile_only}<input type="checkbox" onClick="checkAll('send[]', this)">{/if}
	</td>
{else}
	<td>State</td>
	<td>Send
		{if !$loggedUser->mobile_only}<input type="checkbox" onClick="checkAll('send[]', this)">{/if}
	</td>
{/if}
	</tr>
	{foreach from=$allorder item="row"}
	{if $row->show_in_table}
		{assign var="rowid" value=$row->id}
		{assign var="row_reserve_warehouse_id" value=$row->reserve_warehouse_id}
	<tr>
	{if $warehouses2ship.$row_reserve_warehouse_id!='' || !$row_reserve_warehouse_id || $row->res_mobile_only}
	<input type="hidden" name="quantity_max[{$row->id}]" value="{$row->quantity}"/>
		{if !$row->manual}
			<td>
			  	{article_pic article_id=$row->article_id title=$row->article_id width=200 color=$row->color}
				<input type="hidden" name="orders[{$row->id}][your_internal_id]" value="{$row->article_id}">
			</td>
			<td>
			  	{article_pic article_id=$row->article_id title=$row->name width=200 color=$row->color noid=1}
				{if $row->barcode_type=='A'}<b>{$row->barcode_type}</b>{/if}
				<input type="hidden" name="orders[{$row->id}][content]" value="{$row->name}">
			</td>
		{else}
			<td>&nbsp;</td>
			<td>
		    {if $row->manual==3}
				<a target="_blank" href="shop_voucher.php?id={$row->code_id}&shop_id={$row->shop_id}" style="color:{$row->color}">{$row->custom_title|default:$row->name|escape}</a>
			{else}
				<font style="color:{$row->color}">{$row->name}</font>
			{/if}
			</td>
		{/if}
		<td align="center">
			{$row->quantity}
			<input type="hidden" name="orders[{$row->id}][quantity]" value="{$row->quantity}">
		</td>
		<td>
				<select name="quantity2process[{$row->id}]" 
				{if $row->main_id}id="q{$row->main_id}"{else}id="q{$row->id}"{/if} onClick="valueAllId(this)">
			    {html_options options=$row->quantities selected=$row->quantity}
			    </select>
		</td>
		<td>
			{$row->numbers}
			<input type="hidden" name="orders[{$row->id}][reference_1]" value="">
			<input type="hidden" name="orders[{$row->id}][reference_2]" value="">
		</td>
		<td><div id="bar_order_{$row->id}">{$row->barcodes}</div>
		{if $row->no_barcodes}NO BARCODES ALERTED{/if}</td>
		<td align="center">
			{if !$loggedUser->mobile_only}
				<input type="checkbox" name="pack[]" value="{$row->id}" 
				{if $row->main_id}id="p{$row->main_id}"{else}id="p{$row->id}"{/if} onClick="checkAllId(this)" 
				{if ($pack.$rowid || $default_orders) && !$row->sent}checked{/if}>
			{/if}
		</td>
		{if !$row->sent}
			<td>
				<select name="send_warehouse_id[{$row->id}]" id="send_warehouse_id[{$row->id}]" onChange="changeSendWare(this.value, {$row->id});">
			    <option value="">---</option>
					{if $stock}
				    {html_options options=$row->warehouses2ship_stock selected=$row->send_warehouse_id_|default:$AllSendWares colored=$row->warehouses2ship_colors}
					{else}
				    {html_options options=$warehouses2ship selected=$row->send_warehouse_id_|default:$AllSendWares}
					{/if}
			    </select>
			</td>
			{if $reserve}
				<td>
					<select name="reserve_warehouse_id[{$row->id}]" id="reserve_warehouse_id[{$row->id}]" onChange="changeReserveWare(this.value, {$row->id});">
				    <option value="">---</option>
					{if $stock}
				    {html_options options=$row->warehouses2ship_stock selected=$row->reserve_warehouse_id colored=$row->warehouses2ship_colors}
					{else}
				    {html_options options=$warehouses2ship selected=$row->reserve_warehouse_id}
					{/if}
				    </select>
					<br>{$row->reserve_last}
				</td>
			{/if}
			{if $pickup}
				{if !$row->ready2pickup}
					<td>{if $warehouses2ship.$reserve_warehouse_id || !$reserve_warehouse_id}{$row->state_pickup}{else}set ready on...{/if}</td>
					<td align="center">
						{if !$loggedUser->mobile_only}
							<input type="checkbox" name="ready2pickup[]" value="{$row->id}" 
							{if $row->main_id}id="r{$row->main_id}"{else}id="r{$row->id}"{/if} onClick="checkAllId(this)" 
							{if $ready2pickup.$rowid || $default_orders}checked{/if}>
						{/if}
					</td>
				{else}
					<td colspan="2">{if $warehouses2ship.$reserve_warehouse_id || !$reserve_warehouse_id}{$row->state_pickup}{else}set ready on...{/if}</td>
				{/if}
			{/if}
			<td>{if $warehouses2ship.$send_warehouse_id || !$send_warehouse_id}
					{if $row->sent}
					Mark as shipped by {employee_pic username=$row->state_username width=200} on {$row->state_updated}
					{else}
					Ready to ship
					{/if}
			{else}sent on...{/if}</td>
			<td align="center">
				{if !$loggedUser->mobile_only}
					<input type="checkbox" name="send[]" value="{$row->id}" 
					{if $row->main_id}id="{$row->main_id}"{else}id="{$row->id}"{/if} onClick="checkAllId(this)" 
					{if $send.$rowid || $default_orders}checked{/if}>
				{/if}
			</td>
		{else}
			<td>
				{assign var="send_warehouse_id" value=$row->send_warehouse_id}
				{$warehouses2ship.$send_warehouse_id|default:hidden}
			</td>
			{if $reserve}
				<td>
					{assign var="reserve_warehouse_id" value=$row->reserve_warehouse_id}
					{$warehouses2ship.$reserve_warehouse_id|default:hidden}
				</td>
			{/if}
			{if $pickup}
				{if !$row->ready2pickup}
					<td>{if $warehouses2ship.$reserve_warehouse_id || !$reserve_warehouse_id}{$row->state_pickup}{else}set ready on...{/if}</td>
					<td align="center">
						{if !$loggedUser->mobile_only}
							<input type="checkbox" name="ready2pickup[]" value="{$row->id}" 
							{if $row->main_id}id="r{$row->main_id}"{else}id="r{$row->id}"{/if} onClick="checkAllId(this)" 
							{if $ready2pickup.$rowid || $default_orders}checked{/if}>
						{/if}
					</td>
				{else}
					<td colspan="2">{if $warehouses2ship.$reserve_warehouse_id || !$reserve_warehouse_id}{$row->state_pickup}{else}set ready on...{/if}
					{if $loggedUser->admin}
						{if !$loggedUser->mobile_only}	
							<input type="checkbox" name="ready2pickup[]" value="{$row->id}" 
							{if $row->main_id}id="r{$row->main_id}"{else}id="r{$row->id}"{/if} onClick="checkAllId(this)" 
							{if $ready2pickup.$rowid || $default_orders}checked{/if}>
						{/if}
					{/if}
					</td>
				{/if}
			{/if}
			<td colspan="2">{if $warehouses2ship.$send_warehouse_id || !$send_warehouse_id}
					{if $row->sent}
					Mark as shipped by {employee_pic username=$row->state_username width=200} on {$row->state_updated}
					{else}
					Ready to ship
					{/if}
				{else}sent on...{/if}
					{if $loggedUser->unship && $row->manual<=1}
						{if !$loggedUser->mobile_only}
							<input type="checkbox" name="send[]" value="{$row->id}" 
							{if $row->main_id}id="{$row->main_id}"{else}id="{$row->id}"{/if} onClick="checkAllId(this)" 
							{if $send.$rowid || $default_orders}checked{/if}>
						{/if}
					{/if}
			</td>
		{/if}
	{else}
			<td colspan="9" align="center">
			hidden
			</td>
			{if $reserve}
				<td></td>
			{/if}
			{if $pickup}
					<td></td>
					<td></td>
			{/if}
	{/if}

	</tr>
	{/if}
	{/foreach}
	</table>
	<input type="hidden" id="auction_id" value="{$auction->id|default:$auction->data->id}">
