<?php

require_once __DIR__.'/helpers/Curl.class.php';
require_once __DIR__.'/helpers/MakeFile.class.php';
require_once __DIR__.'/helpers/DataStructure.class.php';

try {
    if (!isset($_POST['secure_hash'])) {
        throw new Exception("Request is not valid!!!");
    }

    $dataService = new DataStructure();
    $requestData = $dataService->buildSpecificFormStructureFromPost($_POST);
    $restApiGetCall = new CurlHelperService();
    $response = $restApiGetCall->getParsingData($requestData, false);
    $fileHelper = new MakeFile($response, $_POST['dpd_pdf_filename'].'.pdf');

//    $fileHelper->downloadFile();
} catch (Exception $e) {
    echo '<div class="alert alert-danger">' . $e->getMessage() . '</div>';
    exit;
}
