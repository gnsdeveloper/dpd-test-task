<?php

/**
 * Class DataStructure
 */
class DataStructure
{
    private $orderAction = DPD_LABELS_ORDER_ACTION_PARAMETER;

    private $labelSize = DPD_LABELS_LABEL_SIZE_PARAMETER;

    private $startPosition = DPD_LABELS_START_POSITION_PARAMETER;

    private $shipServiceMethod = DPD_LABELS_SHIP_SERVICE_METHOD_PARAMETER;

    /**
     * @param array $post
     * @return array
     */
    public function buildSpecificFormStructureFromPost($post = [])
    {
        $result = [];
        $generalData = $this->makeGeneralPostVariables($post);
        $ordersData = $this->getOrdersVariables($post);
        $result['OrderAction'] = $this->orderAction;
        $result['OrderSettings']['ShipDate'] = date("Y-m-d");
        $result['OrderSettings']['LabelSize'] = $this->labelSize;
        $result['OrderSettings']['LabelStartPosition'] = $this->startPosition;

        foreach($ordersData as $key => $orderInfo) {
            $result['OrderDataList'][$key]['ShipAddress'] = $generalData;
            $result['OrderDataList'][$key]['ParcelData'] = $orderInfo;
        }

        return $result;
    }

    /**
     * @param array $post
     * @return array
     */
    private function makeGeneralPostVariables($post = [])
    {
        $result = [];
        $result['Company']      = 'Mustermann AG';//$this->checkIfExistAndSetField($post, 'company');
        $result['Salutation']   = 'Mr';//$this->checkIfExistAndSetField($post, 'salutation');
        $result['Name']         = 'Max Mustermann';//$this->checkIfExistAndSetField($post, 'name');
        $result['Street']       = 'Luitpoldstr.';//$this->checkIfExistAndSetField($post, 'street');
        $result['HouseNo']      = '3';//$this->checkIfExistAndSetField($post, 'house_no');
        $result['ZipCode']      = '97318';//$this->checkIfExistAndSetField($post, 'zip_code');
        $result['City']         = 'Kitzingen';//$this->checkIfExistAndSetField($post, 'city');
        $result['Country']      = 'DEU'; //$this->checkIfExistAndSetField($post, 'country');
        $result['State']        = $this->checkIfExistAndSetField($post, 'state');
        $result['Phone']        = '+49 171 111 444';//$this->checkIfExistAndSetField($post, 'phone');
        $result['Mail']         = 'm.mustermann@mustermann.com';//$this->checkIfExistAndSetField($post, 'mail');

        return $result;
    }

    /**
     * @param array $post
     * @return array
     */
    private function getOrdersVariables($post = [])
    {
        $result = [];

        $result[0]['YourInternalID']     = $post['number'];
        $result[0]['Content']            = "furniture";
        $result[0]['Weight']             = 0;
        $result[0]['Reference1']         = "mail";//$this->checkIfExistAndSetField($order, 'reference_1');
        $result[0]['Reference2']         = $post['number'];//$this->checkIfExistAndSetField($order, 'reference_2');
        $result[0]['ShipService']        = $this->shipServiceMethod;

        return $result;
    }

    /**
     * @param $array
     * @param $value
     * @return string
     */
    private function checkIfExistAndSetField($array, $value)
    {
        return key_exists($value, $array) && isset($array[$value])
            ? $array[$value]
            : ''
        ;
    }
}
