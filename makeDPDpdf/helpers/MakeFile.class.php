<?php

require_once __DIR__ . "/../config/parameters.php";

class MakeFile
{
    private $filename;

    private $filepath;

    private $webpath = DPD_PDF_FILES_GLOBAL_WEBPATH;

    private $dpdPdfFilesFolder = DPD_PDF_FILES_FOLDER;

    /**
     * MakeFile constructor.
     * @param $data
     * @param null $filename
     */
    public function __construct($data, $filename = null)
    {
        $this->filename = is_null($filename) ? $this->filename : $filename;
        $this->filepath = __DIR__.'/../'.$this->dpdPdfFilesFolder.'/';
        $this->makeFile($data);
    }

    /**
     * @param $data
     * @param null $filename
     */
    public function makeFile($data, $filename = null)
    {
        $filename = is_null($filename) ? $this->filename : $filename;
        $decoded = base64_decode($this->getSpecificElementLabel($data));
        file_put_contents($this->filepath.$filename, $decoded);

        return;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function getSpecificElementLabel($data)
    {
        return $data['LabelResponse']['LabelPDF'];
    }

    /**
     * @param null $filename
     */
    public function downloadFile($filename = null)
    {
        $filename = is_null($filename) ? $this->filename : $filename;
        $file = $this->webpath.$filename;
        $content = file_get_contents($file);
        header("Content-type: application/pdf");
        header("Content-Disposition: attachment; filename=".urlencode($filename));
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Description: File Transfer");
        echo $content;
        exit;
    }

    /**
     * @param $url
     */
    public function redirect($url, $statusCode = 303)
    {
        header('Location: ' . $url, true, $statusCode);
        die();
    }
}
