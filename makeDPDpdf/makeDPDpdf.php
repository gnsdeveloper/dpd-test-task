<?php

error_reporting(E_ALL);
ini_set('display_errors',1);

require_once 'connect.php';
require_once 'util.php';
$loggedIn = false;
include 'login.php';
if (!$loggedIn) {
    exit;
}
checkPermission($loggedUser, 'auction', ACCESS_FULL);

require_once 'lib/Auction.php';
require_once 'lib/SellerInfo.php';
require_once 'lib/Invoice.php';
require_once 'lib/PrinterLog.php';
require_once 'config.php';
include 'english.php';

require_once './makeDPDpdf/helpers/Curl.class.php';
require_once './makeDPDpdf/helpers/MakeFile.class.php';
require_once './makeDPDpdf/helpers/DataStructure.class.php';
require_once 'connect.php';
require_once 'util.php';

try {
    if (!isset($_POST['secure_hash'])) {
        throw new Exception("Request is not valid!!!");
    }
    //09980525306539

    $dataService = new DataStructure();
    $txnid = 3;
    $auction_number = "212804";
    $tracking_number = 212804;
    $doc = "makeDPDpdf/files/dpd_label_212804_3.pdf";
    $requestData = $dataService->buildSpecificFormStructureFromPost($_POST);
    $restApiGetCall = new CurlHelperService();
    $response = $restApiGetCall->getParsingData($requestData, false);
    $fileHelper = new MakeFile($response, $_POST['dpd_pdf_filename'].'.pdf');
    PrinterLog::Log($db, $dbr, $auction_number,  $txnid, $loggedUser->get('username'),
        "Printed shipping labels dpd");
    PrinterLog::LogDPD($db, $dbr, $auction_number, $tracking_number, $txnid, $doc);
    $fileHelper->redirect('/makeDPDpdf/files/'.$_POST['dpd_pdf_filename'].'.pdf');

} catch (Exception $e) {
    echo '<div class="alert alert-danger">' . $e->getMessage() . '</div>';
    exit;
}
