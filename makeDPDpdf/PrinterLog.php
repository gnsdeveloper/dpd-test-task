<?php
require_once 'PEAR.php';

class PrinterLog
{
    var $data;
    var $_db;
var $_dbr;
    var $_error;
    var $_isNew;

    static function Log($db, $dbr, $auction_number, $txnid, $username, $action)
    {
		$q = "INSERT INTO printer_log SET log_date = NOW(), auction_number=$auction_number, txnid=$txnid, username='$username', action='$action'";
        $db->query($q);
    }

    static function LogDPD($db, $dbr, $auction_number, $tracking_number, $txnid, $doc)
    {
        $q = "INSERT INTO auction_label (`auction_number`,`txnid`,`tracking_number`,`doc`) VALUES (" . $auction_number . ", " . $txnid . ", '$tracking_number', '" . $doc . "')";
        $db->query($q);
    }


    static function listAll($db, $dbr, $auction_number, $txnid)
    {
        $r = $db->query("SELECT printer_log.auction_number
			, printer_log.action
			, printer_log.log_date
			, printer_log.txnid
			, IFNULL(u.name, printer_log.username) fullusername
			, u.username username
			FROM printer_log 
			join users u on printer_log.username=u.username
			WHERE auction_number = $auction_number AND txnid=$txnid ORDER BY log_date");
        if (PEAR::isError($r)) {
            return;
        }
        $list = array();
        while ($log = $r->fetchRow()) {
            $list[] = $log;
        }
        return $list;
    }
}
?>