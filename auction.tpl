{include file="header.tpl"}
{literal}
<link rel="stylesheet" href="css/autosuggest_inquisitor.css" type="text/css" media="screen" charset="utf-8" />
<link rel="stylesheet" type="text/css" media="all" href="jscalendar-1.0/skins/aqua/theme.css" title="Aqua" />

<script type="text/javascript" src="js/comments.js"></script>
<script type="text/javascript" src="js/bsn.AutoSuggest_c_2.0.js"></script>
<script type="text/javascript" src="CalendarPopup.js"></script>
<script type="text/javascript" src="CalendarPopupAction.js"></script>
<script type="text/javascript" src="JsHttpRequest/JsHttpRequest.js"></script>
<script type="text/javascript" src="wz_tooltip.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$(".new_article_qnt").change(function() {
    	var k = $(this).val();
    	var article_id = $(this).data('aid');
    	var append_str = '';
		$("#barcodes_container").html('');
		for (i = 1; i <= k; i++) {
			append_str = append_str+'Decompleted barcode #'+i+': <input type="text" name="decompleted_barcodes['+article_id+']['+i+']"/><br/>';
    	}
    	$("#barcodes_container").append(append_str);
	});
});
	function get_wwos(username) {
		 JsHttpRequest.query(
		            'js_backend.php', // backend
		            {
		                // pass a text value 
		                'fn': 'get_wwos',
		                'username': username
		            },
		            // Function is called when an answer arrives. 
		            function(result, errors) {
		                // Write the answer.
						document.getElementById('wwos_div').innerHTML = result['res'];
		            },
		            true  // do not disable caching
		        );
	}
	function add2wwo(wwo_id, auction_id, btn) {
		btn.disabled = true;
		if (wwo_id=='') return;
		 JsHttpRequest.query(
		            'js_backend.php', // backend
		            {
		                // pass a text value 
		                'fn': 'add_order2wwo',
		                'wwo_id': wwo_id,
		                'auction_id': auction_id
		            },
		            // Function is called when an answer arrives. 
		            function(result, errors) {
		                // Write the answer.
						alert(result['res']+" rows added");
//						btn.disabled = false;
						document.getElementById('wwo_username').style.display = 'none';
						document.getElementById('wwo_id').disabled = true;
		            },
		            true  // do not disable caching
		        );
}
	var cal_action = new CalendarPopupAction();
	///////////////////////////////////////////////////////////////////////////////
	//						Tracking numbers
	function change_auction_db(field, auction_id, value, id, type, id1, type1) {
		 JsHttpRequest.query(
		            'js_backend.php', // backend
		            {
		                // pass a text value 
		                'fn': 'change_auction',
		                'field': field,
		                'id': auction_id,
		                'value': value
		            },
		            // Function is called when an answer arrives. 
		            function(result, errors) {
		                // Write the answer.
						if (id) {
							if (type=='value') {
		        				document.getElementById(id).value = result["res"];
							} else {if (type=='style.color') {
		        					document.getElementById(id).style.color = result["res"];
								} else {if (type=='checkbox') {
			        					document.getElementById(id).checked = result["res"];
									} else {
			        					document.getElementById(id).innerHTML = result["res"];
									}
								}
							}
						}
						if (id1) {
							if (type1=='value') {
		        				document.getElementById(id1).value = result["res1"];
							} else {if (type1=='style.color') {
		        					document.getElementById(id1).style.color = result["res1"];
								} else {
		        					document.getElementById(id1).innerHTML = result["res1"];
								}
							}
						}
		            },
		            true  // do not disable caching
		        );
}
function unship(id) {
  obj = document.getElementById('frm_shipped');
  document.getElementById('shipnumbers').value = 0;
  oldvalue = document.getElementById('unship_button').value;
  document.getElementById('unship_button').disabled = true;
  document.getElementById('unship_button').value = '...processing...';
  order_ids = '0'; 
  for (i=0; i<obj.length; i++) {
	key = new String(obj.elements[i].name);
	if (key=='send[]' && obj.elements[i].checked) {
		order_id = obj.elements[i].value;  
		order_id_id = obj.elements[i].id;  
		if (document.getElementById('q'+order_id) || document.getElementById('q'+order_id_id)) {
			order_ids = order_ids + ',' + order_id;
		} // if we have quantity
	} // if send checkbox
  }// foreach object
//  alert(order_ids);
  if (order_ids != '0') {
		JsHttpRequest.query(
		            'js_backend.php', // backend
		            {
		                // pass a text value 
						'fn': 'unship',
		                'order_ids': order_ids
		            },
		            // Function is called when an answer arrives. 
		            function(result, errors) {
						if (result["res"]!='') {
							{/literal}
							{if !$loggedUser->ship_overstocked}
								alert(result["res"]+' Article is not in stock, please talk to purchasing department');
						         document.getElementById('unship_button').disabled = false;
						         document.getElementById('unship_button').value = oldvalue;
								return false;
							{/if}
							{literal}
							if (!confirm(result["res"]+'Ship anyway?')) { 
						         document.getElementById('unship_button').disabled = false;
						         document.getElementById('unship_button').value = oldvalue;
								return false;
							} 
						}
						document.getElementById('unship_button').disabled = false;
  						document.getElementById('unship_button').value = oldvalue;
  						document.getElementById('shipnumbers').value = 1;
						location.href = 'auction.php?number='+document.getElementById('number').value+'&txnid='+document.getElementById('txnid').value;
		            },
		            true  // do not disable caching
		        );
  } else {
	document.getElementById('unship_button').disabled = false;
	document.getElementById('unship_button').value = oldvalue;
  	document.getElementById('shipnumbers').value = 1;
	location.href = 'auction.php?number='+document.getElementById('number').value+'&txnid='+document.getElementById('txnid').value;
  }
}

function checkOrderWares(id, obj) {
	if (obj.value=='Special ordered') {
		 JsHttpRequest.query(
		            'js_backend.php', // backend
		            {
		                // pass a text value 
						'fn': 'checkOrderWares',
		                'id': id
		            },
		            // Function is called when an answer arrives. 
		            function(result, errors) {
		                // Write the answer.
						if (result["res"]!='') {
							if (confirm('This item is available in: '+result["res"]+'Continue?')) {
								sp_order(id, obj);
							}
						} else {
							sp_order(id, obj);
						}
		            },
		            true  // do not disable caching
		        );
	} else {
						sp_order(id, obj);
	}
}

function changeSendWare(warehouse_id, id) {
		 JsHttpRequest.query(
		            'js_backend.php', // backend
		            {
		                // pass a text value 
						'fn': 'change_order_warehouse',
		                'send_warehouse_id': warehouse_id,
		                'id': id
		            },
		            // Function is called when an answer arrives. 
		            function(result, errors) {
		                // Write the answer.
		            },
		            true  // do not disable caching
		        );
}

function priority(btn, id, comment) {
	btn.disabled = true;
	if (btn.value=='Undo priority') document.getElementById('btn_priority_del').style.display = 'none'; 
	if (btn.value=='Set priority') document.getElementById('btn_priority_del').style.display = '';
	JsHttpRequest.query(
            'js_backend.php', // backend
            {
                // pass a text value 
				'fn': 'priority',
				'comment': comment,
				'status': btn.value,
		        'id': id
            },
            // Function is called when an answer arrives. 
            function(result, errors) {
                // Write the answer.
				document.getElementById('div_priority').innerHTML = result["res"];
                btn.disabled = false;
            },
            true  // do not disable caching
        );
}

function showhiderating(number, txnid) {
	document.getElementById('btn_showhiderating').disabled = true;
	JsHttpRequest.query(
            'js_backend.php', // backend
            {
                // pass a text value 
				'fn': 'showhiderating',
				'number': number,
				'button': document.getElementById('btn_showhiderating').value,
				'txnid': txnid
            },
            // Function is called when an answer arrives. 
            function(result, errors) {
                // Write the answer.
				if (document.getElementById('btn_showhiderating').value=='Show rating in shop') 
					document.getElementById('btn_showhiderating').value = 'Hide rating in shop'; 
				else
					document.getElementById('btn_showhiderating').value = 'Show rating in shop'; 
				document.getElementById('btn_showhiderating').disabled = false;
            },
            true  // do not disable caching
        );
}

function check_shipping_articles() {
  obj = document.getElementById('frm_shipped');
  document.getElementById('shipnumbers').value = 0;
  oldvalue = document.getElementById('createnumber').value;
  document.getElementById('createnumber').disabled = true;
  document.getElementById('createnumber').value = '...processing...';
  order_ids = '0'; quantities = '0';
  send_warehouse_ids = '0';
  for (i=0; i<obj.length; i++) {
	key = new String(obj.elements[i].name);
	if (key=='send[]' && obj.elements[i].checked) {
		order_id = obj.elements[i].value;  
		order_id_id = obj.elements[i].id;  
		if (document.getElementById('q'+order_id) || document.getElementById('q'+order_id_id)) {
			order_ids = order_ids + ',' + order_id;
			send_warehouse_id = document.getElementById('send_warehouse_id['+order_id+']').value;
			send_warehouse_ids = send_warehouse_ids + ',' + send_warehouse_id;
			if (order_id_id!=order_id)
				quantity = document.getElementById('q'+order_id_id).value;
			else
				quantity = document.getElementById('q'+order_id).value;
			quantities = quantities + ',' + quantity;
			reserve_warehouse_id = document.getElementById('reserve_warehouse_id['+order_id+']').value;
			if (send_warehouse_id=='') {
				alert('Please choose a warehouse to ship from');
				document.getElementById('createnumber').disabled = false;
	  			document.getElementById('createnumber').value = oldvalue;
				return;
			}
		} // if we have quantity
	} // if send checkbox
  }// foreach object
  if (order_ids != '0') {
		JsHttpRequest.query(
		            'js_backend.php', // backend
		            {
		                // pass a text value 
						'fn': 'packed_scan_check_shipping_articles',
		                'order_ids': order_ids,
		                'quantities': quantities,
		                'send_warehouse_ids': send_warehouse_ids
		            },
		            // Function is called when an answer arrives. 
		            function(result, errors) {
						if (result["res1"]!='') {
								alert(result["res1"]);
						         document.getElementById('createnumber').disabled = false;
						         document.getElementById('createnumber').value = 'Ship selected articles';
								return false;
						}
						if (result["res"]!='') {
							{/literal}
							{if !$loggedUser->ship_overstocked}
								alert(result["res"]+' Article is not in stock, please talk to purchasing department');
						         document.getElementById('createnumber').disabled = false;
						         document.getElementById('createnumber').value = oldvalue;
								return false;
							{/if}
							{literal}
							if (!confirm(result["res"]+'Ship anyway??')) { 
						         document.getElementById('createnumber').disabled = false;
						         document.getElementById('createnumber').value = oldvalue;
								return false;
							} 
						}
						document.getElementById('createnumber').disabled = false;
  						document.getElementById('createnumber').value = oldvalue;
  						document.getElementById('shipnumbers').value = 1;
						my_submit();
		            },
		            true  // do not disable caching
		        );
  } else {
	document.getElementById('createnumber').disabled = false;
	document.getElementById('createnumber').value = oldvalue;
  	document.getElementById('shipnumbers').value = 1;
    my_submit();
  }
}

function check_shipping_articles1() {
  obj = document.getElementById('frm_shipped');
  document.getElementById('shipnumbers').value = 0;
  oldvalue = document.getElementById('createnumber1').value;
  document.getElementById('createnumber1').disabled = true;
  document.getElementById('createnumber1').value = '...processing...';
  order_ids = '0'; quantities = '0';
  send_warehouse_ids = '0';
/*  for (i=0; i<obj.length; i++) {
	key = new String(obj.elements[i].name);
	if (key=='send[]' && obj.elements[i].checked) {
		order_id = obj.elements[i].value;  
		order_ids = order_ids + ',' + order_id;
		send_warehouse_id = document.getElementById('send_warehouse_id['+order_id+']').value;
		send_warehouse_ids = send_warehouse_ids + ',' + send_warehouse_id;
		reserve_warehouse_id = document.getElementById('reserve_warehouse_id['+order_id+']').value;
		if (send_warehouse_id=='') {
			alert('Please choose a warehouse to ship from');
			document.getElementById('createnumber1').disabled = false;
  			document.getElementById('createnumber1').value = oldvalue;
			return;
		}
	}	
  }*/
  if (order_ids != '0') {
		JsHttpRequest.query(
		            'js_backend.php', // backend
		            {
		                // pass a text value 
						'fn': 'packed_scan_check_shipping_articles',
		                'order_ids': order_ids,
		                'send_warehouse_ids': send_warehouse_ids
		            },
		            // Function is called when an answer arrives. 
		            function(result, errors) {
						if (result["res"]!='') {
							{/literal}
							{if !$loggedUser->ship_overstocked}
								alert(result["res"]+' Article is not in stock, please talk to purchasing department');
						         document.getElementById('createnumber1').disabled = false;
						         document.getElementById('createnumber1').value = oldvalue;
								return false;
							{/if}
							{literal}
							if (!confirm(result["res"]+'Ship anyway???')) { 
						         document.getElementById('createnumber1').disabled = false;
						         document.getElementById('createnumber1').value = oldvalue;
								return false;
							} 
						}
						document.getElementById('createnumber1').disabled = false;
  						document.getElementById('createnumber1').value = oldvalue;
  						document.getElementById('shipnumbers').value = 2;
						my_submit();
		            },
		            true  // do not disable caching
		        );
  } else {
	document.getElementById('createnumber1').disabled = false;
	document.getElementById('createnumber1').value = oldvalue;
  	document.getElementById('shipnumbers').value = 2;
    my_submit();
  }
}

function addnumber() {
var tns = document.getElementsByName('tracking_number[]');
var myTable = document.getElementById('tracking_number_table');
var tBody = myTable.getElementsByTagName('tbody')[0];
var newTR = document.createElement('tr');
var newTD = document.createElement('td');
newTD.innerHTML = '<input type="text" name="tracking_number[]" onChange="set_method(this);" id="'+(tns.length)+'"> <input type="button" value="+" onClick="addnumber()">';
newTR.appendChild (newTD);
var newTD = document.createElement('td');
newTD.innerHTML = document.getElementById('method_td').innerHTML;
newTR.appendChild (newTD);
var newTD = document.createElement('td');
newTD.innerHTML = document.getElementById('packet_td').innerHTML;
newTR.appendChild (newTD);
tBody.appendChild(newTR);
}

function set_method(thisvalueobj){
	thisvalue = thisvalueobj.value;
	key=thisvalueobj.id;
  obj = document.getElementById('frm_methods');
  var method, found=0;
  var name, value;
  uservalue = thisvalue;
//  if (thisvalue=='') uservalue = document.getElementsByName('tracking_number').item(0).value;
  for (i=0; i<obj.length; i++) {
  	name = obj.elements[i].name;
	value = obj.elements[i].value;
  	method = name.substring(20, name.length-1);
	if (value!='' && value == thisvalue.substring(0, value.length) && !found) {
		  document.getElementsByName('method[]').item(key).value = method;
		  found = 1;
	};	  
  };
  if (found==0) alert('Shipping method has not been recognized automatic. Please select it manual')
}

function checkAllMethods() {
     obj = document.getElementById('frm_shipped');
     for (i=0; i<obj.length; i++) {
	     key = new String(obj.elements[i].name);
	     if (key.indexOf('tracking_number[]')==0 && obj.elements[i].value!='') {
		     if (document.getElementsByName('method[]').item(obj.elements[i].id).value == '') { 
		 		return false
			}	
		 }
	}
	return true;
}

function my_submit(){
//  if (document.getElementsByName('method').item(0).value == "") set_method('');
  if (document.getElementById('frm_shipped').checked.value==1) document.getElementById('frm_shipped').submit();
  else alert('Parameters are not Ok');
}
//						Tracking numbers
///////////////////////////////////////////////////////////////////////////////

function shipping_order_check_shipping(number, username) {
	if (username) {
         document.getElementById('shipping_order_submit').disabled = true;
         document.getElementById('shipping_order_submit').value = '...processing...';
		 JsHttpRequest.query(
		            'js_backend.php', // backend
		            {
		                // pass a text value 
						'fn': 'shipping_order_check_shipping',
		                'number': number,
		                'username': username
		            },
		            // Function is called when an answer arrives. 
		            function(result, errors) {
		                // Write the answer.
//						alert(result["res"]);
						if (result["res"]!='') {
							if (!confirm('Not all goods in stock now'+result["res"])) { 
						         document.getElementById('shipping_order_submit').disabled = false;
						         document.getElementById('shipping_order_submit').value = 'Reassign';
								return false;
							}	
						}
						document.getElementById('reassign').value = 'Reassign';
						document.getElementById('form1').submit();
		            },
		            true  // do not disable caching
		        );
	} else {
			document.getElementById('reassign').value = 'Reassign';
			document.getElementById('form1').submit();
	}
}

function print(content){
  myWin= open('', "printEmail", "");
  myWin.document.open();
  myWin.document.write(content);
  myWin.document.write('<script>');
  myWin.document.write('window.print();');
  myWin.document.write('<\/script>');
  myWin.document.close();
}
function check_shipping(number, warehouse, form_name, button_name) {
//<a onclick="document.getElementById('ship65721').value=1;document.getElementById('tn65721').submit();">Mark as shipped</a>
// name="shipped"
	if (button_name) {
         document.getElementById(button_name).disabled = true;
         document.getElementById(button_name).value = '...processing...';
	}	 
		 JsHttpRequest.query(
		            'js_backend.php', // backend
		            {
		                // pass a text value 
						'fn': 'packed_scan_check_shipping',
		                'number': number,
		                'warehouse': warehouse
		            },
		            // Function is called when an answer arrives. 
		            function(result, errors) {
		                // Write the answer.
//						alert(result);
						if (result["res"]!='') {
							if (!confirm(result["res"]+'Ship anyway????')) { 
								if (button_name) {
						    	     document.getElementById(button_name).disabled = false;
						        	 document.getElementById(button_name).value = 'Mark as shipped';
								}	 
								document.getElementById(form_name).ship.value=0;
								return false;
							}	
						}
						document.getElementById(form_name).ship.value=1;
						document.getElementById(form_name).submit();
		            },
		            true  // do not disable caching
		        );
}

function deleteDoc(doc_id)   {
	msg='Delete this file?'; 
	if (confirm(msg)) {
	    document.getElementById('form1').update.value='deldoc';
		document.getElementById('form1').deleted_doc.value=doc_id;
	   	document.getElementById('form1').submit();
	};
};	

/*function deleteComment(id)   {
	if (confirm('Delete this comment?')) {
		document.getElementById('comments').update.value='delcomm';
		document.getElementById('comments').deleted_obj.value=id;
	   	document.getElementById('comments').submit();
	};
};*/	
function changePrice()
{
    title = prompt('New price', document.getElementById('winning_bid').value);
    if (title) {
        document.getElementById('winning_bid').value=title
        document.getElementById('form1').submit()
    }
}
function changeQuantity()
{
    title = prompt('New Quantity', document.getElementById('quantity').value);
    if (title) {
        document.getElementById('quantity').value=title
        document.getElementById('form1').submit()
    }
}

function ch_order(id)
{
    orderid = prompt('Order ID', document.getElementById('spec_order_id['+id+']').value);
	if (orderid) {
        document.getElementById('spec_order_id['+id+']').value=orderid
        document.getElementById('invoice_form').submit()
    }
}
function sp_order(id,btn)
{
	if (btn.value=='Special ordered') {
	   document.getElementById('spec_order['+id+']').value='1';
//	   document.getElementById('new_article['+id+']').value='0';
	} else {
	   document.getElementById('spec_order['+id+']').value='0';
	}
   document.getElementById('invoice_form').submit();
}
function new_article(id,btn)
{
	if (btn.value=='Taken from new article') {
		if (document.getElementById('new_article_other_qty['+id+']').value>0)
			alert('Warning! We already have decompleted article on stock');
	   document.getElementById('new_article['+id+']').value='1';
//	   document.getElementById('spec_order['+id+']').value='0';
	} else {
	   document.getElementById('new_article['+id+']').value='0';
	}
   document.getElementById('invoice_form').submit();
}
function lost_new_article(id,btn)
{
	if (btn.value=='Carton reported lost') {
		document.getElementById('lost_new_article['+id+']').value='1';
	} else {
		document.getElementById('lost_new_article['+id+']').value='0';
	}
	document.getElementById('invoice_form').submit();
}

function new_article_completed_checking(id,btn) {
	quantity = document.getElementById('new_article_qnt'+id).value;
	article_id = document.getElementById('new_article_id'+id).value;
	unc_article_id = document.getElementById('uncomplete_article_order_id'+id).value;
	if (eval(quantity)<=0 || (article_id=='' && unc_article_id=='')) {
		alert('Choose article and set quantity first');
		return;
	}
	if (article_id!='') 
		JsHttpRequest.query(
		            'js_backend.php', // backend
		            {
		                // pass a text value 
						'fn': 'new_article_completed_checking',
		                'id': id,
		                'btn_value': btn.value,
		                'quantity': quantity,
		                'article_id': article_id
		            },
		            // Function is called when an answer arrives. 
		            function(result, errors) {
						if (result["res"]!='') {
							alert(result["res"]);
						} else {
							new_article_completed(id,btn);
						}
		            },
		            true  // do not disable caching
		        );
	else
		new_article_completed(id,btn);
}
function new_article_completed(id,btn)
{
	if (btn.value=='Article completed') {
	   document.getElementById('new_article_completed['+id+']').value='1';
	} else {
	   document.getElementById('new_article_completed['+id+']').value='0';
	}
   document.getElementById('invoice_form').submit();
}

function changeCur()
{
    title = prompt('New siteID', document.getElementById('siteid').value);
    if (title) {
        document.getElementById('siteid').value=title
        document.getElementById('form1').submit()
    }
}
function changeOffer()
{
    title = prompt('New Offer ID', document.getElementById('offer_id').value);
    if (title) {
        document.getElementById('offer_id').value=title
        document.getElementById('form1').submit()
    }
}
function changeEmail()
{
    title = prompt('New email', document.getElementById('email').value);
    if (title) {
        document.getElementById('email').value=title
        document.getElementById('form1').submit()
    }
}                                                        
function checkDelete()   {
{/literal} 
{if $is_shipped} alert('There are some articles shipped'); return; {/if}
{literal} 
/*	if ((document.getElementById('comments').update.value=='0' && document.getElementById('newcomment').value=='')
	&& document.getElementById('deleteau').value == "Delete auction") {
	   alert('Add reason for DELETE the auction into comments, before deleting the auction.');
	   return;
	} */
	document.getElementById('newcomment_del').value = document.getElementById('newcomment').value;
	if (document.getElementById('no_complain').value == "1"
	&& document.getElementById('deleteau').value == "Delete auction") {
	    if (confirm('You didn�t start a complain. Do you really want to delete auction?')) {
			document.getElementById('delete').value = '1';
	 		document.getElementById('form1').submit();
		}
	} else {
		document.getElementById('delete').value = '1';
 		document.getElementById('form1').submit();
	}	 
}

function complain(){
	document.getElementById('complain').submit(); 
}
function addRating() {
	document.getElementById('manualRating_div').style.display="inline";
}
function openWin(src) {
  window.open(src, 'sd')
}
</script>
{/literal} 

{if ! $nodisplay}
<table width="600" colspasing="0" colpadding="0" border="0">
      <tr>
        <td colspan=2 align="left"><h1><span style="color:green; font-weight:bold">{$auction->username} = {$info->data->seller_name}</span></h1></td>
      </tr>
{if ($auction->deleted)}
      <tr>
        <td colspan=2 align="center"><h1><span style="color:red; font-weight:bold">DELETED</span></h1></td>
      </tr>
{/if}	  
  <tr>
    <td valign="top" width="20%">
    <table  border="0" width="100%">
	<tr>
	<td valign="top" width="50%"><table>
      <tr>
        <td nowrap valign="top" width="10%"><b>Auftrag #</b>
		<br>{$source_seller->name}</td>
        <td nowrap valign="top" width="30%" {if $auction->no_emails}bgcolor="#FFDDDD"{/if}>
      <form name="scan_auction" action="auction.php" method="post" id="form1">
        <input type="hidden" name="number" id="number" value="{$auction->auction_number}">
        <input type="hidden" name="txnid" id="txnid" value="{$auction->txnid}">
        <input type="hidden" name="return" value="{$return|escape:"html"}">
		<input type="hidden" name="update" value="1">
			{$auction->auction_number} / {$auction->txnid} 
			{if $auction->rma_id}&nbsp;&nbsp;
			Generated from Ticket#<a href="rma.php?rma_id={$auction->rma_id}&number={$rma_auction_number}&txnid={$rma_txnid}">{$auction->rma_id}</a>
			{/if}
			{if $auction->based_on_auction_id}&nbsp;&nbsp;
			Generated from Auction <a href="auction.php?number={$based_on->auction_number}&txnid={$based_on->txnid}">{$based_on->auction_number}/{$based_on->txnid}</a>
			{/if}
			{if $auction->txnid}<input type="submit" value="Read auction" name="scan">{/if}
			{foreach from=$driver_tasks item="next_task"}
				<br />
				linked with driver task: <a href="route_task.php?auction_number={$next_task->auction_number}&route_id={$next_task->route_id}">{$next_task->auction_number}</a>
			{/foreach}
		</td>
      </tr>
		<tr bgcolor="#CCCCCC"><td valign="top"><b>Responsible person Auftrag</b></td>
		<td valign="top">
		      <select name="responsible_uname" id="responsible_uname">
				<option label="" value="">---</option>
		        {html_options options=$users selected=$main_auction->data->responsible_uname}
		      </select>
        <input type="hidden" name="newcomment_reassign" id="newcomment_reassign" value="">
			<input type="button" value="Reassign" 
				onClick="reassignComment(document.getElementById('responsible_uname').value);">
		</td>
		</tr>
		<tr><td valign="top">
		<b><a target="_blank" href="shipping_auction.php?number={$auction->auction_number}&txnid={$auction->txnid}">Shipping company</a></b></td>
		</td>
		<td valign="top">
        <input type="hidden" name="reassign" id="reassign" value="">
		      <select name="shipping_username" id="shipping_order_username">
				<option label="" value="">---</option>
		        {html_options options=$shipping_users selected=$main_auction->data->shipping_username}
		      </select>
			<input type="button" value="Reassign" id="shipping_order_submit" 
			onClick="shipping_order_check_shipping('{$auction->auction_number}/{$auction->txnid}', document.getElementById('shipping_order_username').value)">
			{if $shipping_username_changed}
			changed by {employee_pic username=$shipping_username_changed->username width=200} on {$shipping_username_changed->updated}
			{/if}
			<br/>
{if $near_text}
	<span style="font-size:8px;">{foreach from=$near_text item="text"}<br/>{$text}{/foreach}</span>
{/if}
		</td>
		</tr>
		<tr bgcolor="#CCCCCC"><td valign="top"><b>Add to WWO</b></td>
		<td valign="top">
		      {if !$auction->wwo_id}<select id="wwo_username" onChange="get_wwos(this.value)">
				<option label="" value="">---</option>
		        {html_options options=$users_wwos}
		      </select>
			  <div id="wwos_div">
		      <select id="wwo_id">
				<option label="" value="">---</option>
		        {html_options options=$active_wwos}
		      </select>
			  </div>
			<input type="button" value="Add" onClick="add2wwo(document.getElementById('wwo_id').value, {$auction->id}, this);">
			  {else}
		      <select disabled>
		        {html_options options=$all_wwos selected=$auction->wwo_id}
		      </select>
			  {/if}
		</td>
		</tr>
      <tr >
        <td valign="top"><b>Username seller</b></td>
        <td valign="top"><a href="sellerinfo.php?original_username={$auction->username}" target="_blank">
		{$auction->username} = {$info->data->seller_name}</a>
		</td>
      </tr>
      <tr bgcolor="#CCCCCC">
        <td valign="top"><b>Purchased:</b></td>
        <td valign="top">{$purchased}
		</td>
      </tr>
      <tr>
        <td valign="top"><b>Source Seller:</b></td>
        <td valign="top">
		<select name="source_seller_id" id="source_seller_id">
        	<option value="" selected>---</option>
        	{html_options options=$sources selected=$auction->source_seller_id}
	    </select>
		<input type="submit" value="Change Source" name="change_source">
		</td>
      </tr>
	  {if $source_seller->use_ff_number}
      <tr>
        <td valign="top"><b>Fulfillement Number:</b></td>
        <td valign="top">
		<input type="text" name="ff_number" value="{$auction->ff_number}">
		<input type="submit" value="Change number" name="change_number">
		</td>
      </tr>
	  {/if}
      <tr bgcolor="#CCCCCC">
        <td valign="top"><b>Username buyer</b></td>
		<td valign="top"><a target="_blank" href="search.php?what=buyer&buyer_name={$auction->username_buyer}">{$auction->username_buyer}</a></td>
      </tr>
	  {if !$customer->dontshow_other_list}
      <tr>
        <td valign="top"><b>Other orders</b></td>
		<td valign="top">
		<table>
		{if $other_orders_from>0}
		<script>
		function showAllRows_{$auction_number}() {ldelim}
		    cbs = document.getElementsByName('tr_rows_{$auction_number}[]');
		    for (i=0; i< cbs.length; i++)
		    {ldelim}
		        cbs.item(i).style.display = '';
		    {rdelim}
			document.getElementById('tr_some_actions_{$auction_number}').style.display = 'none';
		{rdelim}
		</script>
   {math equation="a+b" a=$other_orders_from b=10 assign="all_actions"}
  <tr id="tr_some_actions_{$auction_number}">
    <td colspan="4">You see 10 of {$all_actions} orders, click to see <a href="javascript:showAllRows_{$auction_number}()">All orders</a></td>
  </tr>
		{/if}
		{foreach from=$other_orders item="other" key="i"}
	  <tr {if $i<$other_orders_from}name="tr_rows_{$auction_number}[]" style="display:none"{/if}>
		<td><a target="_blank" href="auction.php?number={$other->auction_number}&txnid={$other->txnid}">{$other->auction_number}/{$other->txnid}</a></td>
	  </tr>
		{/foreach}
		</table>
		</td>
      </tr>
	  {/if}
      <tr bgcolor="#CCCCCC">
        <td valign="top"><b>Paid price</b></td>
		<td valign="top">
        <input type="hidden" id="winning_bid" name="winning_bid" value="{$auction->winning_bid}">
        <input type="hidden" id="quantity" name="quantity" value="{$auction->quantity}">
        <input type="hidden" id="siteid" name="siteid" value="{$auction->siteid}">
		{$auction->quantity} x {$auction->winning_bid} {$currency}
        [<a href="javascript:changeQuantity()" style="font-size:9px;">Change quantity</a>]
        [<a href="javascript:changePrice()" style="font-size:9px;">Change price</a>]
        [<a href="javascript:changeCur()" style="font-size:9px;">Change seller (API) country</a>]
		</td>
      </tr>
      <tr>
        <td valign="top"><b>End time</b></td>
{if ! $end_time_90_reached}
        <td valign="top"><span style="color:green; font-weight:bold">{$auction->end_time}</span></td>
{else}
        <td valign="top"><span style="color:red; font-weight:bold">{$auction->end_time}</span></td>
{/if}		
      </tr>
      <tr bgcolor="#CCCCCC">
        <td valign="top"><b>Offer</b></td>
        <td valign="top">
		{foreach from=$offers item="offer"}
		{$offer->name|escape} <a target="_blank" href="offer.php?id={$offer->offer_id}">(id={$offer->offer_id})</a> 
			{if $offer->saved_id}
			SA#<a target="_blank" href="newauction.php?edit={$offer->saved_id}">{$offer->saved_id}</a><br>
			{else}
			SA# none<br>
			{/if}
		{/foreach}
		</td>
      </tr>
      <tr bgcolor="#CCCCCC">
        <td valign="top"><b>Alias</b></td>
        <td valign="top">{$alias|escape}</td>
      </tr>
{if $auction->delivery_date_real=='0000-00-00 00:00:00'}
      <tr bgcolor="#CCCCCC">
        <td valign="top">or enter new offer id </td>
		<td valign="top">
		<input type="text" name="newoffer_id">
		<input type="submit" value="Change offer" name="change_offer">
		</td>
      </tr>{/if}
	<tr><td><table>
		{include file="_alarm.tpl"}
		</table>
	</td><td valign="top">
<script type="text/javascript">
{literal}
function rma_notif(btn, id) {
	btn.disabled = true;
	JsHttpRequest.query(
            'js_backend.php', // backend
            {
                // pass a text value 
				'fn': 'rma_notif',
				'obj': 'auction',
				'id': id
            },
            // Function is called when an answer arrives. 
            function(result, errors) {
                // Write the answer.
				document.getElementById('div_rma_notif').innerHTML = result['res'];
                btn.disabled = false;
            },
            true  // do not disable caching
        );
}
{/literal}
</script>
		<input type="button" value="Ticket Notification" id="btn_rma_notif" onClick="rma_notif(this, {$auction->id})"/>
		<div id="div_rma_notif"><b>{if !$rma_notif}<font color="red">Off</font>{else}<font color="green">On</font>{/if}</b></div>
</td></tr>
      <tr>
        <td valign="top"><b>{$delivery_date_title}</b></td>
        {if 1 || $delivery_date!='As soon as possible'}
        <td valign="top">
<SCRIPT LANGUAGE="JavaScript">
var cal_delivery_date = new CalendarPopup();
cal_delivery_date.setReturnFunction("setMultipleValues_delivery_date");
function setMultipleValues_delivery_date(y,m,d) {ldelim}
	 change_auction_db('delivery_date_customer', {$auction->id}, y+'-'+m+'-'+d, 'anchor_delivery_date');
	{rdelim}
</SCRIPT>
<A HREF="#" onClick="cal_delivery_date.showCalendar('anchor_delivery_date'{if $delivery_date!='As soon as possible'}, '{$delivery_date}'{/if}); return false;" 
TITLE="cal_delivery_date.showCalendar('anchor_delivery_date'{if $delivery_date!='As soon as possible'}, '{$delivery_date}'{/if}); return false;" 
NAME="anchor_delivery_date" ID="anchor_delivery_date"
{if $delivery_date!='As soon as possible'}style="color:red; {if $delivery_date_reached}text-decoration:blink;{/if}font-weight:bold"{/if}>{$delivery_date}</A>
		</td>
        {else}
        <td valign="top">{$delivery_date}</td>
        {/if}
	  </tr>	
		<tr bgcolor="#CCCCCC"><td  valign="top"><b>Possible rating until</b></td>
		<td  valign="top"><span style="color:{$rating_color|escape}">{$rating_date|escape}</span></td></tr>
      <tr>
        <td valign="top"><b>Temporary rating:</b></td>
        <td valign="top">
            {if $auction->temp_rating_received == 1}<font color="green">"{$auction->temp_rating_received_text}" (Positive)</font>{/if}
            {if $auction->temp_rating_received == 2}<font color="darkgray">"{$temp_auction->rating_received_text}" (Neutral)</font>{/if}
            {if $auction->temp_rating_received == 3}<font color="red">"{$auction->temp_rating_received_text}" (Negative)</font>{/if}
	</td>
      </tr>	
      <tr bgcolor="#CCCCCC">
        <td valign='top'>
            <b>Rating {if ! $auction->rating_received}is not received {else} received:{/if}</b>
		</td>
		<td>
		<table border="0">
		{foreach from=$ratings_recived item="rating"}
		<tr>
			<td>{if $rating->rating_received_color == 'red'}
			<font color="{$rating->rating_received_color}">
			<a  style="color:{$rating->rating_received_color}" 
			target="_blank" href='rating_case.php?id={$rating_case_id}'>"{$rating->rating_text_received|escape|nl2br}"</a> 
			{else}
			<font color="{$rating->rating_received_color}">"{$rating->rating_text_received}"
			{/if}
            ({$rating->rating_received_date})</font></td>
		</tr>
		{/foreach}
		{if $disabled!='disabled'}
		<tr><td><a href="javascript:addRating()">Add rating manual</a></td></tr>
		<tr><td><div id="manualRating_div" style="display:none">
		<textarea name="manualRating" cols="30" rows="5" style="width:100%"></textarea>
		{if $auction->txnid==3}
		<br><input type="radio" name="rating" value="1"> negative
		<br><input type="radio" name="rating" value="3"> neutral
		<br><input type="radio" name="rating" value="5" selected> positive
		{else}
		<br><input type="radio" name="rating" value="3"> negative
		<br><input type="radio" name="rating" value="2"> neutral
		<br><input type="radio" name="rating" value="1" selected> positive
		{/if}
		<br><input type="submit" value="Update" name="rating_case">
		</div></td></tr>
		{/if}
		</table>	
		<br>
		{if $rating_case_id}
		<a target="_blank" href='rating_case.php?id={$rating_case_id}'>Rating case #{$rating_case_id}</a> 
		{/if}
		<br><input type="button" value="{if !$auction->hiderating && !$rating->hidden}Hide{else}Show{/if} rating in shop" 
		id="btn_showhiderating" onClick="showhiderating('{$auction->auction_number}','{$auction->txnid}')"><br>
		<input type="submit" value="Get feedback" name="get_rating"><br>

<br><br>
<table>
	<tr>
		<td>
			<b>Send voucher to customer</b> 
			for amount
			<input type="Text" name="voucher_amount"/>
			with validity of
			<select name="voucher_days">
		        {html_options options=$voucher_days selected=90}
		    </select> days
			<input type="submit" name="ticket_voucher_email_send" value="Send to customer"/>
		</td>
	</tr>
	<tr>
		<td>
			<table border="1">
			<tr>
			<td><b>ID</b></td>
			<td><b>Name</b></td>
			<td><b>Code</b></td>
			<td><b>Template</b></td>
			<td><b>User</b></td>
			<td><b>Date</b></td>
			</tr>
			{foreach from=$voucher_log item="log"}
			<tr>
			<td><a target="_blank" href="shop_voucher.php?id={$log->code_id}">{$log->code_id}</a></td>
			<td><a target="_blank" href="shop_voucher.php?id={$info->data->rma_shop_promo_code_id}">{$log->name}</a></td>
			<td>{$log->code}</td>
			<td><a target="_blank" href="shop_promo_template.php?id={$log->template_id}&shop_id={$log->code_template_shop_id}">{$log->code_template}</a></td>
			<td>{$log->username}</td>
			<td>{$log->date}</td>
			</tr>
			{/foreach}
			</table>
		</td>
	</tr>
</table>
        </td>
      </tr>
      <tr>
        <td><b>Rating {if ! $auction->rating_given}is not given</b>{else}is given: </b>{/if}</td>
		<td>
		{if $auction->rating_given}"{$auction->rating_text_given}"{/if}
		<input type="submit" value="Leave feedback" name="set_rating">
		</td>
      </tr>
      <tr bgcolor="#CCCCCC">
        <td valign='top' colspan="2">
        {if !$auction->complained && $auction->complain_set_by!=''}
	  	Complain cleared on {$auction->complain_set_date|escape} by {$auction->complain_set_by_name}
		{/if}
        {if $auction->complained && $auction->complain_set_date && $auction->complain_set_date != '0000-00-00 00:00:00'}
	  	Complain started on {$auction->complain_set_date|escape} by {$auction->complain_set_by_name}
		<input type="submit" name="complain_submit" value="Clear complain" 
			{if (($auction->complain_set_date == '0000-00-00 00:00:00') || (!$auction->complain_set_date))} disabled {/if} {$startComplainOver}>
	  	{else}
		<input type="submit" name="complain_submit" value="Start complaint" onclick="window.open('{$_cgi_eBay_complain}', 'Start complaint'); complain();" 
			{$startComplainOver}>
		{/if}
		<input type="hidden" name="complain_set_date" value="{$auction->complain_set_date|escape}">
		<input type="hidden" id="no_complain" name="no_complain" value="{if (($auction->complain_set_date == '0000-00-00 00:00:00') || (!$auction->complain_set_date))}1{else}0{/if}">
    </td>
  </tr>  
  <tr>
    <td colspan="2">
          <input type="hidden" id="newcomment_del" name="newcomment_del">
          <input type="hidden" id="delete" name="delete">
            {if $auction->deleted}Auftrag deleted on {$auction->deleted_date} by {$auction->deleted_by_name|default:$auction->deleted_by} {/if}
            {if !$auction->deleted && $auction->deleted_by}Auftrag restored on {$auction->deleted_date} by {$auction->deleted_by_name|default:$auction->deleted_by} {/if}
	  {if ($auction_delete) }
            <input type="button" id="deleteau" onClick="checkDelete()"  {if ($auction->deleted)} value="Restore Auftrag" {else} value="Delete Auftrag" {/if}
	    	   {if $auction->delivery_date_real!='0000-00-00 00:00:00'} disabled {/if}
	    >
	  {else}
            <input type="button" onClick="alert('You can`t delete this Auftrag. Please ask supervisor.')" {if ($auction->deleted)} value="Restore Auftrag" {else} value="Delete Auftrag" {/if}
	    	   {if $auction->delivery_date_real!='0000-00-00 00:00:00'} disabled {/if}
	    >
	  {/if}
	</td>
  </tr>
  <tr bgcolor="#CCCCCC">
        <td valign='top' colspan="2">
    {if $auction->no_emails}
		<input type="button" onClick='location.href="auction.php?number={$auction->auction_number}&txnid={$auction->txnid}&freeze=0&return={$return|escape:"url"}"' 
		value="Resume emails" {if $sellerInfo->no_emails}disabled{/if}/>
		Emails stopped on {$auction->freeze_date} by {$auction->no_emails_by_name}
    {else}
        <input type="button" onClick='location.href="auction.php?number={$auction->auction_number}&txnid={$auction->txnid}&freeze=1&return={$return|escape:"url"}"' value="Stop emails">
		{if $auction->freeze_date && $auction->freeze_date!='0000-00-00'}
			Emails resumed on {$auction->freeze_date} by {$auction->no_emails_by_name}
		{/if}
	{/if}
    </td>
  </tr>  
  <tr>
        <td valign="top" colspan="2">
		<table border="0"><tr>
		<td valign="top" colspan="2">
		<div id="div_priority">
    {if $priority}
		Priority set on {$priority_row->updated} <br>by {$priority_row->username}
    {else}
		{if $priority_row->username}
		Priority undone on {$priority_row->updated} <br>by {$priority_row->username}
		{/if}
    {/if}
		</div>
		</td>
		</tr><tr>
		<td valign="top">
		<input type="button" value="Set priority" id="btn_priority_set" onClick="priority(this, {$auction->id}, document.getElementById('comment_priority').value)">
		<input type="button" value="Undo priority" id="btn_priority_del" onClick="priority(this, {$auction->id}, document.getElementById('comment_priority').value)"
		{if !$auction->priority}style="display:none"{/if}>
		</td>
		<td valign="top">
		<textarea id="comment_priority">{$auction->priority_comment}</textarea>
		</td>
		</tr></table>
    </td>
  </tr>  
<tr>
<td colspan="2">
<input type="hidden" name="deleted_doc" value="">
<table border="0" width="300"><tr><td valign="top">
<table border="0" width="100%">
<tr><td align="left"><b>Attached documents</b></td>
<td align="left" nowrap>
{if $disabled!='disabled'}
<a href="load_docs.php?number={$auction->auction_number}&txnid={$auction->txnid}">(Add new document)</a>
{/if}
</td></tr>
<tr>
    <td align="left"><b>File name</b></td>
    <td align="left"><b>Date/time</b></td>
    <td align="left"><b>Responsible person</b></td>
    <td align="center">&nbsp;</td>
</tr>
<tr>
    <td colspan="4"><hr></td>
</tr>
{foreach from=$docs item="doc"}
<tr>
    <td align="left"><a href="doc.php?number={$auction->auction_number}&doc_id={$doc->doc_id}">{$doc->name}</a> </td>
    <td align="left">&nbsp;{$doc->date}</td>
    <td align="left">&nbsp;{$doc->fullusername}</td>
    <td align="center"><input type="button" onClick="deleteDoc({$doc->doc_id})" name="deldoc" value="Delete" ></td>
</tr>
{/foreach}
</table>
</td></tr>
</table>
</td>
</tr>
      <tr>
        <td valign='top' colspan="2">
   Customer URL : <a tagret="_blank" href="{$order_url}">{$order_url}</a>
    </td>
  </tr>  
      <tr bgcolor="#CCCCCC">
        <td valign='top' colspan="2">
        <input type="hidden" id="email" name="email" value="{$auction->email}">
   Customer email : <a href="mailto:{$auction->email}?subject={$auction->auction_number}">{$auction->email}</a>
        [<a href="javascript:changeEmail()" style="font-size:9px;">Change email</a>] 
		{if $auction->customer_id}<a href="customer.php?id={$auction->customer_id}&src={$src}" target="_blank">Customer #{$auction->customer_id}</a>{/if}
    </td>
  </tr>  
      <tr>
        <td valign='top' colspan="2">
   eBay URL : <a target="_blank" href="{$_cgi_eBay}{$auction->auction_number}">{$_cgi_eBay}{$auction->auction_number}</a>
    </td>
  </tr>  
  {if $auction->shipping_method_str}
      <tr bgcolor="#CCCCCC">
    <td><b>Payment & shipping</b></td>
    <td>{$auction->shipping_method_str}</td>
  </tr>
  {if $method}
      <tr>
    <td><b>Shipping date</b></td>
    <td>{$auction->delivery_date_real}</td>
  </tr>
  {/if}
  {/if}
  <tr>
    <td valign="top"><b>Employee invoice</b></td>
    <td>
			<select name="emp_id">
				<option value="">---</option>
		        {html_options options=$employees selected=$auction->emp_id}
		    </select> <input type="submit" value="Update" name="emp_invoice"/>
			<br>
			changed by {employee_pic username=$emp_id_log_row->username width=200} on {$emp_id_log_row->updated}
	</td>
  </tr>
	</table>
      </form>
	</td>    
<!-- SECOND COLUMN -->

	<td bgcolor="" align="center" valign="top" width="50%" >
		<b>Comments</b>
		<div id="div_comments" data-obj="auction" data-obj-id="{$auction->id}" data-email-log="true">{include file="comments_block_ajax.tpl"}</div>
	  	<input type="button" value="Add comment" onClick="addComment();">
		<input type="button" value="Add comment from customer" style="background-color:{$config.comment_customer_color}" onclick="addComment('customer');">
		<input type="button" value="Add comment from CS" style="background-color:{$config.comment_CS_color}" onclick="addComment('CS');">
		<input type="hidden" id="auction_id" value="{$auction->id}"/>
		
		<script type="text/javascript">
		{literal}
		function comment_notif(btn, id) {
			btn.disabled = true;
			JsHttpRequest.query(
	            'js_backend.php', // backend
	            {
	                // pass a text value 
					'fn': 'comment_notif',
					'obj': 'auction',
					'id': id
	            },
	            // Function is called when an answer arrives. 
	            function(result, errors) {
	                // Write the answer.
					document.getElementById('div_comment_notif').innerHTML = result['res'];
	                btn.disabled = false;
	            },
	            true  // do not disable caching
	        );
		}
		{/literal}
		</script>

		<input type="button" value="Comment Notification" id="btn_comment_notif" onClick="comment_notif(this, {$auction->id})"/>
		<div id="div_comment_notif"><b>{if !$comment_notif}<font color="red">Off</font>{else}<font color="green">On</font>{/if}</b></div>

    <table border="1">
      <tr>
        <td colspan="3" align="center"><h3>History of change order</h3></td>
      </tr>
      <tr>
        <td><b>Date</b></td>
        <td><b>Action</b></td>
        <td><b>User</b></td>
      </tr>
      {foreach from=$auctionLog item="logentry"}
      <tr>
        <td>{$logentry->time}</td>
        <td>{$logentry->action}</td>
        <td>{employee_pic username=$logentry->username width=200 deftitle='Customer'}</td>
      </tr>
      {/foreach}
    </table>

<br>
<div id="email_log">{include file="email_log_block.tpl"}</div>

{if $smsLog}
<br>
    <table border="1">
      <tr>
        <td colspan="5" align="center"><h3>SMS protocoll</h3></td>
      </tr>
      <tr>
        <td><b>Date</b></td>
        <td><b>Action</b></td>
        <td style="width:50px"><b>To</b></td>
        <td><b>Provider</b></td>
      </tr>
      {foreach from=$smsLog item="logentry"}
      <tr>
        <td>{$logentry->date}</td>
    {if $logentry->content}    
		<td><a href="javascript:print('{$logentry->content}')"><IMG src="print.png"></a>&nbsp;<a target="_self" onmouseover="Tip('{$logentry->content}')" onmouseout="UnTip()">{$logentry->template}</a></td>
	{else}	
        <td>{$logentry->template}</td>
    {/if}    
		<td style="width:50px">&nbsp;{$logentry->number}</td>
        <td><a target="_blank" href="https://central.clickatell.com/report/sent/view/{$logentry->message_id}">{$logentry->provider|escape}</a></td>
      </tr>
      {/foreach}
    </table>
{/if}
    </td>
</tr>
  <tr>	<td></td><td></td><td></td> </tr>
</table>


<tr>
<td width="50%" align="right">

<table width="100%" border="0">
      <tr bgcolor="#CCCCCC">
        <td valign='top' colspan="1">
{if ($auction->delivery_date_real=='0000-00-00 00:00:00') }
   <a href="order.php?auction={$auction->auction_number}&txnid={$auction->txnid}&admin=1">Change order</a>
{/if}   
    </td>
  </tr>  
  {if $auction->shipping_method_str}
  {if $auction->pickedup}
  <tr bgcolor="#CCCCCC">
    <td><b>Picked up</b></td>
    <td>{$auction->delivery_date_real}</td>
  </tr>
  {/if}

  {/if}
<tr><td valign="top">
{if $auction->payment_method}
 {if $auction->main_auction_number}
 {assign var="kinderstyle" value='style="color:gray"'}
 {else}
 {assign var="kinderstyle" value=''}
 {/if}
<table border="1" {$kinderstyle}>
  <tr>
    <td>&nbsp;</td>
    <td>
	<a {$kinderstyle} href="gmap_aufaddr.php?mode=_invoice&auction_number={$auction->auction_number}&txnid={$auction->txnid}" target="_blank"><b>Billing</b><!--<img src="image003.jpg"/>--></a></td>
    {if 1}<td>
	<a {$kinderstyle} href="gmap_aufaddr.php?mode=_shipping&auction_number={$auction->auction_number}&txnid={$auction->txnid}" target="_blank"><b>Shipping</b></a></td>
	{/if}
  </tr>
  <tr>
    <td><b>Company</b></td>
    <td>{$auction->company_invoice|escape|default:""}</td>
    {if 1}<td {$kinderstyle}>{$auction->company_shipping|escape|default:""}</td>{/if}
  </tr>
  <tr>
    <td><b>Gender</b></td>
			{assign var="gender_invoice" value=$auction->gender_invoice}
    <td>{$english.$gender_invoice|escape|default:""}</td>
    {if 1}
			{assign var="gender_shipping" value=$auction->gender_shipping}
	<td>{$english.$gender_shipping|escape|default:""}</td>
	{/if}
  </tr>
  <tr>
    <td><b>First Name</b></td>
    <td>{$auction->firstname_invoice|escape|default:""}</td>
    {if 1}<td>{$auction->firstname_shipping|escape|default:""}</td>{/if}
  </tr>
  <tr>
    <td><b>Last Name</b></td>
    <td>{$auction->name_invoice|escape|default:""}</td>
    {if 1}<td>{$auction->name_shipping|escape|default:""}</td>{/if}
  </tr>
  <tr>
    <td><b>Street</b></td>
    <td>{$auction->street_invoice|escape|default:""}</td>
    {if 1}<td>{$auction->street_shipping|escape|default:""}</td>{/if}
  </tr>
  <tr>
    <td><b>House</b></td>
    <td>{$auction->house_invoice|escape|default:""}</td>
    {if 1}<td>{$auction->house_shipping|escape|default:""}</td>{/if}
  </tr>
  <tr>
    <td><b>Zip</b></td>
    <td>{$auction->zip_invoice|escape|default:""}</td>
    {if 1}<td>{$auction->zip_shipping|escape|default:""}</td>{/if}
  </tr>
  <tr>
    <td><b>City</b></td>
    <td>{$auction->city_invoice|escape|default:""}</td>
    {if 1}<td>{$auction->city_shipping|escape|default:""}</td>{/if}
  </tr>
  <tr>
    <td><b>Country</b></td>
    <td>{$auction->country_invoice|escape|default:""}</td>
    {if 1}<td>{$auction->country_shipping|escape|default:""}</td>{/if}
  </tr>
  {if $ask_for_state}
  <tr>
    <td><b>State</b></td>
    <td>{$auction->state_invoice|escape|default:""}</td>
    {if 1}<td>{$auction->state_shipping|escape|default:""}</td>{/if}
  </tr>
  {/if}
  <tr>
    <td><b>Email</b></td>
    <td><a {$kinderstyle} href="mailto:{$auction->email_invoice|escape}?subject={$auction->auction_number}">{$auction->email_invoice|escape}</a></td>
    {if 1}<td><a {$kinderstyle} href="mailto:{$auction->email_shipping|escape}?subject={$auction->auction_number}">{$auction->email_shipping|escape}</a></td>{/if}
  </tr>
  <tr>
    <td><b>Phone</b></td>
	{assign var="tel_country_code_invoice" value=$auction->tel_country_code_invoice}
    <td>{$countries.$tel_country_code_invoice.phone_prefix} {$auction->tel_invoice|escape|default:""}</td>
	{assign var="tel_country_code_shipping" value=$auction->tel_country_code_shipping}
    {if 1}<td>{$countries.$tel_country_code_shipping.phone_prefix} {$auction->tel_shipping|escape|default:""}</td>{/if}
  </tr>
  <tr>
    <td><b>Mobile</b></td>
	{assign var="cel_country_code_invoice" value=$auction->cel_country_code_invoice}
    <td>{$countries.$cel_country_code_invoice.phone_prefix} {$auction->cel_invoice|escape|default:""}</td>
	{assign var="cel_country_code_shipping" value=$auction->cel_country_code_shipping}
    {if 1}<td>{$countries.$cel_country_code_shipping.phone_prefix} {$auction->cel_shipping|escape|default:""}</td>{/if}
  </tr>
{if $auction->customer_id}
  <tr>
    <td colspan="1" align="left" style="border-style:none">
	<a {$kinderstyle} href="change_log.php?table_name=customer{$src}&tableid={$auction->customer_id}" target="_blank">ChangeLog</a>
	</td>
    <td colspan="1" align="center" style="border-style:none">
	Customer Language: <a {$kinderstyle} href="customer.php?id={$auction->customer_id}&src={$src}" target="_blank">{$customer_lang}</a>
	{if !$auction->main_auction_number}
	<br>
      <form action="auction.php" method="post" >
        <input type="hidden" name="number" value="{$auction->auction_number}">
        <input type="hidden" name="txnid" value="{$auction->txnid}">
        <input type="hidden" name="return" value="{$return|escape:"html"}">
	Auftrag language <select name="lang" onChange="this.form.submit();">
        {html_options options=$langs selected=$lang}
    </select>
      </form>
	 {/if}
	</td>
    <td colspan="1" align="right" style="border-style:none">
	<a {$kinderstyle} href="customer.php?id={$auction->customer_id}&src={$src}" target="_blank">Customer #{$auction->customer_id}</a>
	</td>
  </tr>
  {if $seller_warehouses}
  <tr>
    <td colspan="3">
	Distance to warehouses:<br>
	{foreach from=$seller_warehouses item="ware"}
	<font {if !$auction->main_auction_number}color="{$ware->color}"{/if}><b>{$ware->fullname}:</b> {$ware->distance_txt}, {$ware->duration_txt}</font><br>
	{/foreach}
	{if !$auction->main_auction_number}
		<input type="button" value="Recalculate distances" onClick="window.open('cron_auction_warehouse_distance.php?id={$auction->id}')"/>
	{/if}
	</td>
  </tr>
  {/if}
{/if}
</table>
<br><br>
            <input type="button" value="New invoice" name="newinvoice"
onClick="window.open('auction.php?number={$auction->auction_number}&txnid={$auction->txnid}&newinvoice=1')">
<br>
{if $invoices}
<table border="1">
<tr>
<td><b>Date</b></td>
<td><b>Author</b></td>
<td><b>Invoice#</b></td>
<td><b>Packed by</b></td>
<td><b>Parcel type</b></td>
<td><b>Tracking number</b></td>
<td><b>Shipping date</b></td>
</tr>
{foreach from=$invoices item="inv"}
<tr {if $inv->deleted}style="text-decoration:line-through"{/if}>
<td>{$inv->end_time}</td>
<td>{$inv->response_username}</td>
<td><a href="auction.php?number={$inv->auction_number}&txnid={$inv->txnid}">{$inv->auction_number}/{$inv->txnid}</a></td>
<td align="center">{if !$inv->shipping}{$inv->shipping_data->shipping_username}{else}<table border="0" {if $inv->deleted}style="text-decoration:line-through"{/if}>
{foreach from=$inv->shipping item="sh"}
<tr><td>{$sh->username}</td></tr>
{/foreach}
</table>{/if}</td>
<td align="center">{if !$inv->shipping}no{else}<table border="0" {if $inv->deleted}style="text-decoration:line-through"{/if}>
{foreach from=$inv->shipping item="sh"}
<tr><td>{$sh->packet_name}</td></tr>
{/foreach}
</table>{/if}</td>
<td align="center">{if !$inv->shipping}no{else}<table border="0" {if $inv->deleted}style="text-decoration:line-through"{/if}>
{foreach from=$inv->shipping item="sh"}
<tr><td>{$sh->number}</td></tr>
{/foreach}
</table>{/if}</td>
<td align="center">{if !$inv->shipping}{$inv->shipping_data->delivery_date_real}{else}<table border="0" {if $inv->deleted}style="text-decoration:line-through"{/if}>
{foreach from=$inv->shipping item="sh"}
<tr><td>{if $inv->shipping_data->delivery_date_real=='0000-00-00 00:00:00'}No{else}{$inv->shipping_data->delivery_date_real} by {$inv->shipping_data->shipping_username}{/if}</td></tr>
{/foreach}
</table>{/if}</td>
</tr>
{/foreach}
</table>
{/if}
<br><br>

    <table>
      <tr>
        <td>
          {if $auction->rating_reminder_sent}
          Will not ask for rating
          {else}
          <form method="post" action="auction.php">
            <input type="submit" value="Do not ask for rating" name="noask">
            <input type="hidden" name="number" value="{$auction->auction_number}">
            <input type="hidden" name="txnid" value="{$auction->txnid}">
          </form>
          {/if}
        </td>
    </table>

    <form method="post" action="auction.php" target="_blank">
		<input type="hidden" id="wincnt" name="wincnt" value="0">
{if ($auction->delivery_date_real!='0000-00-00 00:00:00') }
		{assign var="static" value="static"}
{/if}
	<table><tr>
	<td valign="top">
		<input type="button" onClick="{if $is_neighbour}alert('This customer live in the neighborhood');{/if} openWin('order_confirmation.php?auction_number={$auction->auction_number}&txnid={$auction->txnid}&print=1')" value="Print order confirmation"><br/>
		<input type="button" onClick="{if $is_neighbour}alert('This customer live in the neighborhood');{/if} openWin('static_invoice_HTML.php?number={$auction->auction_number}&txnid={$auction->txnid}&shipping_list=0&static={$static}&print=1')" value="Print {$static} invoice"><br/>
		<input type="button" onClick="{if $is_neighbour}alert('This customer live in the neighborhood');{/if} {if $shipping_list_already_printed}if (confirm('Warning! The invoice was already printed')) {/if}openWin('static_invoice_HTML.php?number={$auction->auction_number}&txnid={$auction->txnid}&shipping_list=1&static={$static}&print=1')" value="Print {$static} packing list"><br/>
	    {if ($auction->delivery_date_real=='0000-00-00 00:00:00')}
		<input type="button" onClick="{if $is_neighbour}alert('This customer live in the neighborhood');{/if} {if $shipping_list_already_printed}if (confirm('Warning! The invoice was already printed')) {/if}openWin('static_invoice_HTML.php?number={$auction->auction_number}&txnid={$auction->txnid}&shipping_list=1&static={$static}&print=1&notsent=1')" value="Print not sent packing list"><br/>
		<input type="button" onClick="{if $is_neighbour}alert('This customer live in the neighborhood');{/if} openWin('static_invoice_HTML.php?number={$auction->auction_number}&txnid={$auction->txnid}&outside_invoice=1&static={$static}&print=1')" value="Print {$static} outside EU invoice"><br/>
		{/if}
		<input type="button" onClick="{if $is_neighbour}alert('This customer live in the neighborhood');{/if} openWin('servicequitting.php?auction_number={$auction->auction_number}&txnid={$auction->txnid}&print=1')" value="Print {$static} servicequitting"><br/>
		<input type="button" onClick="openWin('payment_slip.php?auction_number={$auction->auction_number}&txnid={$auction->txnid}&print=1')" value="Print Payment Slip"><br/>
	</td>
	<td valign="top">
		<input type="hidden" id="wincnt" name="wincnt" value="0">
		<input type="button" onClick="openWin('order_confirmation.php?auction_number={$auction->auction_number}&txnid={$auction->txnid}')" value="Show order confirmation"><br/>
		<input type="button" onClick="openWin('static_invoice_HTML.php?number={$auction->auction_number}&txnid={$auction->txnid}&shipping_list=0&static={$static}')" value="Show {$static} invoice"><br/>
		<input type="button" onClick="openWin('static_invoice_HTML.php?number={$auction->auction_number}&txnid={$auction->txnid}&shipping_list=1&static={$static}')" value="Show {$static} packing list"><br/>
	    {if ($auction->delivery_date_real=='0000-00-00 00:00:00')}
		<input type="button" onClick="openWin('static_invoice_HTML.php?number={$auction->auction_number}&txnid={$auction->txnid}&shipping_list=1&static={$static}&notsent=1')" value="Show not sent packing list"><br/>
		<input type="button" onClick="openWin('static_invoice_HTML.php?number={$auction->auction_number}&txnid={$auction->txnid}&outside_invoice=1&static={$static}')" value="Show outside EU invoice"><br/>
		{/if}
		<input type="button" onClick="openWin('servicequitting.php?auction_number={$auction->auction_number}&txnid={$auction->txnid}')" value="Show servicequitting"><br/>
	</td>
	<td valign="top">
		<input type="button" onClick="openWin('order_confirmation.php?auction_number={$auction->auction_number}&txnid={$auction->txnid}&email=1')" value="Send order confirmation by email"><br/>
		<input type="button" onClick="openWin('static_invoice_HTML.php?number={$auction->auction_number}&txnid={$auction->txnid}&shipping_list=0&email=1&static={$static}')" value="Send {$static} invoice by email"><br/>
		<input type="button" onClick="openWin('static_invoice_HTML.php?number={$auction->auction_number}&txnid={$auction->txnid}&shipping_list=1&email=1&static={$static}')" value="Send {$static} packing list by email"><br/>
	    {if ($auction->delivery_date_real=='0000-00-00 00:00:00')}
		<input type="button" onClick="openWin('static_invoice_HTML.php?number={$auction->auction_number}&txnid={$auction->txnid}&shipping_list=1&email=1&static={$static}&notsent=1')" value="Send not sent packing list by email"><br/>
<!--		<input type="button" onClick="openWin('static_invoice_HTML.php?number={$auction->auction_number}&txnid={$auction->txnid}&outside_invoice=1&email=1&static={$static}')" value="Send {$static} outside EU invoice by email"><br/>-->
		{/if}
		<input type="button" onClick="openWin('servicequitting.php?auction_number={$auction->auction_number}&txnid={$auction->txnid}&email=1')" value="Send {$static} servicequitting by email"><br/>
	</td>
</tr>
</table>
<br> Seller account
      <select name="customseller">
		<option value=''>---</option>
        {html_options options=$sellers}
      </select>
{literal}
    <input type="button" value="Produce invoice" onClick="if (this.form.customseller.value!='') { this.form.produce_invoice.value=1; this.form.submit();}">
    <input type="button" value="Produce outside EU invoice" onClick="if (this.form.customseller.value!='') { this.form.produce_outside_invoice.value=1; this.form.submit();}">
{/literal}
    <input type="hidden" name="produce_invoice" value="0">
    <input type="hidden" name="produce_outside_invoice" value="0">
    <input type="hidden" name="number" value="{$auction->auction_number}">
    <input type="hidden" name="txnid" value="{$auction->txnid}">
        <input type="hidden" name="return" value="{$return|escape:"html"}">
    </form>
{/if}
	    {if ($auction->delivery_date_real!='0000-00-00 00:00:00')}
<br>
    <form method="post" action="auction.php">
		<input type="submit" name="restatic" value="Re-generate statis docs">
    <input type="hidden" name="number" value="{$auction->auction_number}">
    <input type="hidden" name="txnid" value="{$auction->txnid}">
    </form>
		{/if}

    <form method="post" action="auction.php">
    <select name="resend">
    {html_options options=$template_names selected=$resent}
    </select> <input type="submit" value="Resend email"> 
<!--	<br/>
    {if $auction->no_emails}
    <input type="button" name="resume" value="Resume emails"
    onClick="location.href = 'auction.php?number={$auction->auction_number}&txnid={$auction->txnid}&freeze=0&return={$return|escape:"url"}'">
	Stopped on {$auction->freeze_date} by {$auction->no_emails_by}
    {else}
    <input type="button" name="resume" value="Stop emails"
    onClick="location.href = 'auction.php?number={$auction->auction_number}&txnid={$auction->txnid}&freeze=1&return={$return|escape:"url"}'">
    	  {if $auction->freeze_date && $auction->freeze_date!='0000-00-00' && $auction->no_emails_by}
	Emails resumed on {$auction->freeze_date} by {$auction->no_emails_by}
	{/if}
    {/if}-->
    {if $resent}<br/><b><font color="blue">{$resent} resent</font></b>{/if}
    <input type="hidden" name="number" value="{$auction->auction_number}">
    <input type="hidden" name="txnid" value="{$auction->txnid}">
        <input type="hidden" name="return" value="{$return|escape:"html"}">
    </form>

{if $auction->payment_method && !$auction->main_auction_number}
    <table border="1">
      <tr>
        <td colspan="10" align="center"><h3>Payments</h3></td>
      </tr>
      <tr>
        <td><b>Date</b></td>
        <td><b>Account</b></td>
        <td><b>Amount</b></td>
        <td><b>Exported</b></td>
        <td><b>Selling account</b></td>
        <td><b>VAT account</b></td>
        <td><b>VAT %</b></td>
        <td><b>Username</b></td>
        <td><b>Comment</b></td>
        <td>&nbsp;</td>
      </tr>
      {foreach from=$payments item="payment"}
      <tr>
        <td>{$payment->payment_date}</td>
        <td>{$payment->account|escape}</td>
        <td>{$payment->amount}</td>
        <td>{if $payment->exported}Yes{else}No{/if}</td>
        <td>{$payment->selling_account_number}</td>
        <td>{$payment->vat_account_number}</td>
        <td><font {if $auction->customer_vat}color="red"{/if}>{$payment->vat_percent}</font></td>
        <td>{employee_pic username=$payment->username width=200}</td>
        <td>&nbsp;{$payment->comment}</td>
        <td><a href="auction.php?number={$auction->auction_number}&txnid={$auction->txnid}&delpay={$payment->payment_id}" onclick="return confirm('Are you sure?');">Delete</a></td>
      </tr>
      {/foreach}
      <tr>
        <td colspan="10" align="right"><b>Total</b> : {$currency} {$payment_total}</td>
      </tr>
      <tr>
        <td colspan="10" align="right"><b>{if $auction->main_auction_number}Subinvoice{else}Open{/if} amount</b> : {$currency} {$open_amount}</td>
      </tr>
    </table>
    <form method="post" action="auction.php">
    Date {html_select_date end_year=2030 start_year=2003 time=$payment_default_date}<br>
    Account <select name="account">
    {html_options options=$accounts selected=$default_account}
    </select><br>
    VAT Account: {if $auction->customer_vat}{$vat_info->out_vat_v}{else}{$vat_info->vat_account_number_v}{/if} <br>
    Selling Account: {if $auction->customer_vat}{$vat_info->out_selling_v}{else}{$vat_info->selling_account_number_v}{/if} <br>
	{$currency} <input type="text" name="amount" size="6"> <input type="submit" value="Make payment" 
	{if $seller_warehouses_neigbourhood}onClick="alert('{$seller_warehouses_neigbourhood}')"{/if}>
	Comment <input type="text" name="paycomment" size="20">
	<br><input type="submit" name="safer" value="Make this payment by Saferpay (cc)" 
	onClick="if (this.form.amount.value!='') return true; alert('Enter amount to charge on CC first'); return false;"> only for Beliani customers
	<br><input type="submit" name="paypal" value="Make this payment by PayPal" 
	onClick="if (this.form.amount.value!='') return true; alert('Enter amount to charge on PP first'); return false;"> only for Beliani customers
    <input type="hidden" name="number" value="{$auction->auction_number}">
    <input type="hidden" name="txnid" value="{$auction->txnid}">
    </form>
<br>
{if $sofo_payments}
    <table border="1">
      <tr>
        <td colspan="10" align="center"><h3>Sofort Payments</h3></td>
      </tr>
      <tr>
        <td><b>ID</b></td>
        <td><b>Date</b></td>
        <td><b>Status</b></td>
        <td><b>Amount</b></td>
        <td>&nbsp;</td>
      </tr>
		{assign var="pppayment_total" value=0}
      {foreach from=$sofo_payments item="payment"}
      <tr>
	    {math equation="a+b" a=$payment->getAmount b=$pppayment_total assign="pppayment_total"}
        <td>{$payment->getTransaction}</td>
        <td>{$payment->getTime}</td>
        <td>{$payment->getStatus} {$payment->getStatusReason}</td>
        <td>{$payment->getCurrency} {$payment->getAmount}</td>
        {if $payment->booked}
		<td>
		Booked on {$payment->booked_date} by {$payment->booked_by_name}
		<input type="button"value="unBook" 
		onClick="location.href='auction.php?number={$auction->auction_number}&txnid={$auction->txnid}&sfp={$payment->id}&book=0';"/>
		</td>
		{else}
		<td>
		<input type="button"value="Book" 
		onClick="location.href='auction.php?number={$auction->auction_number}&txnid={$auction->txnid}&sfp={$payment->id}&book=1';"/>
		</td>
		{/if}
      </tr>
      {/foreach}
      <tr>
        <td colspan="10" align="right"><b>Total</b> : {$payment->getCurrency} {$pppayment_total|string_format:"%8.2f"}</td>
      </tr>
    </table>
<br/>
{/if}
{if $pp_payments}
    <table border="1">
      <tr>
        <td colspan="10" align="center"><h3>Paypal Payments</h3></td>
      </tr>
      <tr>
        <td><b>ID</b></td>
        <td><b>Date</b></td>
        <td><b>Type</b></td>
        <td><b>Amount</b></td>
        <td><b>Fee</b></td>
        <td>&nbsp;</td>
      </tr>
		{assign var="pppayment_total" value=0}
      {foreach from=$pp_payments item="payment"}
      <tr>
	    {math equation="a+b" a=$payment->mc_gross b=$pppayment_total assign="pppayment_total"}
        <td>{$payment->txn_id}</td>
        <td>{$payment->payment_date_MEZ}</td>
        <td>{$payment->txn_type}</td>
        <td>{$payment->mc_currency} {$payment->mc_gross}</td>
        <td>{$payment->mc_currency} {$payment->mc_fee}</td>
        {if $payment->booked}
		<td>
		Booked on {$payment->booked_date} by {$payment->booked_by_name}
		<input type="button"value="unBook" 
		onClick="location.href='auction.php?number={$auction->auction_number}&txnid={$auction->txnid}&ppp={$payment->id}&book=0';"/>
		</td>
		{else}
		<td>
		<input type="button"value="Book" 
		onClick="location.href='auction.php?number={$auction->auction_number}&txnid={$auction->txnid}&ppp={$payment->id}&book=1';"/>
		</td>
		{/if}
      </tr>
      {/foreach}
      <tr>
        <td colspan="10" align="right"><b>Total</b> : {$payment->mc_currency} {$pppayment_total|string_format:"%8.2f"}</td>
      </tr>
    </table>
<br/>
{/if}
{if $ps_payments}
    <table border="1">
      <tr>
        <td colspan="10" align="center"><h3>{if $auction->payment_method=='ideal_shp'}iDEAL payments{else}Saferpay Payments{/if}</h3></td>
      </tr>
      <tr>
        <td><b>ID</b></td>
        <td><b>Authorisation Date</b></td>
        <td><b>Amount</b></td>
        <td><b>AVS result</b></td>
        <td>&nbsp;</td>
      </tr>
		{assign var="pspayment_total" value=0}
      {foreach from=$ps_payments item="payment"}
      <tr>
	    {math equation="a+b" a=$payment->amount b=$pspayment_total assign="pspayment_total"}
        <td><a href="https://www.saferpay.com/BO/Commerce/JournalDetail?gxid={$payment->txn_id}" target="_blank">{$payment->txn_id}</a></td>
        <td>{$payment->authorisation_date_MEZ}</td>
        <td>{$payment->currency} {$payment->amount}</td>
        <td>{$payment->avs_descr}</td>
        {if $payment->booked}
		<td>
		Booked on {$payment->booked_date} by {employee_pic username=$payment->booked_by_username width=200}
		<input type="button"value="unBook" 
		onClick="location.href='auction.php?number={$auction->auction_number}&txnid={$auction->txnid}&psp={$payment->id}&book=0';"/>
		</td>
		{else}
		<td>
		<input type="button"value="Book" 
		onClick="location.href='auction.php?number={$auction->auction_number}&txnid={$auction->txnid}&psp={$payment->id}&book=1';"/>
		</td>
		{/if}
      </tr>
      {/foreach}
      <tr>
        <td colspan="10" align="right"><b>Total</b> : {$payment->currency} {$pspayment_total|string_format:"%8.2f"}</td>
      </tr>
    </table>
<br/>
{/if}
{if $bill_payments}
    <table border="1">
      <tr>
        <td colspan="10" align="center"><h3>Billpay Payments</h3></td>
      </tr>
      <tr>
        <td><b>ID</b></td>
        <td><b>Authorisation Date</b></td>
        <td><b>Amount</b></td>
        <td><b>AVS result</b></td>
        <td>&nbsp;</td>
      </tr>
		{assign var="pspayment_total" value=0}
      {foreach from=$bill_payments item="payment"}
      <tr>
	    {math equation="a+b" a=$payment->amount b=$pspayment_total assign="pspayment_total"}
        <td><a href="https://www.saferpay.com/BO/Commerce/JournalDetail?gxid={$payment->txn_id}" target="_blank">{$payment->txn_id}</a></td>
        <td>{$payment->authorisation_date_MEZ}</td>
        <td>{$payment->currency} {$payment->amount}</td>
        <td>{$payment->avs_descr}</td>
        {if $payment->booked}
		<td>
		Booked on {$payment->booked_date} by {$payment->booked_by_name}
		<input type="button"value="unBook" 
		onClick="location.href='auction.php?number={$auction->auction_number}&txnid={$auction->txnid}&bill={$payment->id}&book=0';"/>
		</td>
		{else}
		<td>
		<input type="button"value="Book" 
		onClick="location.href='auction.php?number={$auction->auction_number}&txnid={$auction->txnid}&bill={$payment->id}&book=1';"/>
		</td>
		{/if}
      </tr>
      {/foreach}
      <tr>
        <td colspan="10" align="right"><b>Total</b> : {$payment->currency} {$pspayment_total|string_format:"%8.2f"}</td>
      </tr>
    </table>
<br/>
{/if}
<br>
{if $gc_payments}
    <table border="1">
      <tr>
        <td colspan="10" align="center"><h3>Google Checkout Payments</h3></td>
      </tr>
      <tr>
        <td><b>Date</b></td>
        <td><b>Amount</b></td>
        <td>&nbsp;</td>
      </tr>
		{assign var="gcpayment_total" value=0}
      {foreach from=$gc_payments item="payment"}
      <tr>
	    {math equation="a+b" a=$payment->amount b=$gcpayment_total assign="gcpayment_total"}
        <td>{$payment->payment_date}</td>
        <td>{$payment->amount} {$currency}</td>
        {if $payment->booked}
		<td>
		Booked on {$payment->booked_date} by {$payment->booked_by_name}
		<input type="button"value="unBook" 
		onClick="location.href='auction.php?number={$auction->auction_number}&txnid={$auction->txnid}&gcp={$payment->id}&book=0';"/>
		</td>
		{else}
		<td>
		<input type="button"value="Book" 
		onClick="location.href='auction.php?number={$auction->auction_number}&txnid={$auction->txnid}&gcp={$payment->id}&book=1';"/>
		</td>
		{/if}
      </tr>
      {/foreach}
      <tr>
        <td colspan="10" align="right"><b>Total</b> : {$currency} {$gcpayment_total|string_format:"%8.2f"}</td>
      </tr>
    </table>
<br/>
{/if}
{if $bs_payments}
    <table border="1">
      <tr>
        <td colspan="10" align="center"><h3>BeanStream Payments</h3></td>
      </tr>
      <tr>
        <td><b>Transaction ID</b></td>
        <td><b>Date</b></td>
        <td><b>Amount</b></td>
        <td>&nbsp;</td>
      </tr>
		{assign var="bspayment_total" value=0}
      {foreach from=$bs_payments item="payment"}
      <tr>
	    {math equation="a+b" a=$payment->amount b=$bspayment_total assign="bspayment_total"}
        <td>{$payment->trnId}</td>
        <td>{$payment->payment_date}</td>
        <td>{$payment->amount} {$currency}</td>
        {if $payment->booked}
		<td>
		Booked on {$payment->booked_date} by {$payment->booked_by_name}
		<input type="button"value="unBook" 
		onClick="location.href='auction.php?number={$auction->auction_number}&txnid={$auction->txnid}&bsp={$payment->id}&book=0';"/>
		</td>
		{else}
		<td>
		<input type="button"value="Book" 
		onClick="location.href='auction.php?number={$auction->auction_number}&txnid={$auction->txnid}&bsp={$payment->id}&book=1';"/>
		</td>
		{/if}
      </tr>
      {/foreach}
      <tr>
        <td colspan="10" align="right"><b>Total</b> : {$currency} {$bspayment_total|string_format:"%8.2f"}</td>
      </tr>
    </table>
<br/>
{/if}
{if $p24_payments}
    <table border="1">
      <tr>
        <td colspan="10" align="center"><h3>P24 Payments</h3></td>
      </tr>
      <tr>
        <td><b>Date</b></td>
        <td><b>Amount</b></td>
        <td>&nbsp;</td>
      </tr>
		{assign var="p24payment_total" value=0}
      {foreach from=$p24_payments item="payment"}
      <tr>
	    {math equation="a+b" a=$payment->amount b=$p24payment_total assign="p24payment_total"}
        <td>{$payment->payment_date}</td>
        <td>{$payment->amount} {$currency}</td>
        {if $payment->booked}
		<td>
		Booked on {$payment->booked_date} by {$payment->booked_by_name}
		<input type="button"value="unBook" 
		onClick="location.href='auction.php?number={$auction->auction_number}&txnid={$auction->txnid}&p24={$payment->id}&book=0';"/>
		</td>
		{else}
		<td>
		<input type="button"value="Book" 
		onClick="location.href='auction.php?number={$auction->auction_number}&txnid={$auction->txnid}&p24={$payment->id}&book=1';"/>
		</td>
		{/if}
      </tr>
      {/foreach}
      <tr>
        <td colspan="10" align="right"><b>Total</b> : {$currency} {$p24payment_total|string_format:"%8.2f"}</td>
      </tr>
    </table>
<br/>
{/if}
{if 0}
    <form method="post" action="auction.php">
<table>
  <tr>
    <td align="right">
      <b>Send Auftrag marked as shipped email</b>
    </td>
    <td>
      <select name="dontsend_marked_as_Shipped" onChange="this.form.submit()">
        <option value="1" {if $auction->dontsend_marked_as_Shipped}selected{/if}>No</option>
        <option value="0" {if !$auction->dontsend_marked_as_Shipped}selected{/if}>Yes</option>
      </select>
    </td>
  </tr>
</table>
    <input type="hidden" name="number" value="{$auction->auction_number}">
    <input type="hidden" name="txnid" value="{$auction->txnid}">
    </form>
<br>
{/if}
    <form method="post" action="auction.php">
	<input type="submit" name="send" value="Send invoice" ><br>
	<input type="submit" name="send" value="Send Packing list" >
    to <select name="method">
				<option label="" value="">---</option>
		    {html_options options=$methods}
				    </select>
    <input type="hidden" name="number" value="{$auction->auction_number}">
    <input type="hidden" name="txnid" value="{$auction->txnid}">
    </form>
{/if}
    <table border="1">
      <tr>
        <td colspan="4" align="center"><h3>Ticket</h3></td>
      </tr>
      <tr>
        <td><b>Date</b></td>
        <td><b>Ticket#</b></td>
        <td><b>Complain</b></td>
        <td><b>Responsible person</b></td>
      </tr>
      {foreach from=$rmas item="rma"}
      <tr>
        <td>{$rma->create_date}</td>
{if $rma->close_date && $rma->close_date!='0000-00-00'}
        <td>
        <a style="color:gray" href="rma.php?rma_id={$rma->rma_id}&number={$auction->auction_number}&txnid={$auction->txnid}">{$rma->rma_id}</a>
		</td>
{else}
        <td>
        <a style="color:green" href="rma.php?rma_id={$rma->rma_id}&number={$auction->auction_number}&txnid={$auction->txnid}">{$rma->rma_id}</a>
		</td>
{/if}		
        <td>{if $rma->complain_succ}Yes{else}No{/if}</td>
        <td>&nbsp;{$rma->responsible_name}</td>
      </tr>
      {/foreach}
    </table>
    <form method="post" action="auction.php">
    <input type="hidden" name="number" value="{$auction->auction_number}">
    <input type="hidden" name="txnid" value="{$auction->txnid}">
    <input type="hidden" name="CRM_Ticket" value="0">
	<input type="submit" value="New case" onclick="this.form.CRM_Ticket.value='1';">
    </form>
<br>
    <table border="1">
      <tr>
        <td colspan="3" align="center"><h3>Insurance cases</h3></td>
      </tr>
      <tr>
        <td><b>#</b></td>
        <td><b>Date</b></td>
        <td><b>Responsible person</b></td>
      </tr>
      {foreach from=$inss item="ins"}
      <tr>
        <td><a {if $ins->closed}style="color:gray"{/if} href="insurance.php?id={$ins->id}">{$ins->id}</a></td>
        <td><span {if $ins->closed}style="color:gray"{/if}>{$ins->date}</span></td>
        <td><span {if $ins->closed}style="color:gray"{/if}>&nbsp;{$ins->responsible_name}</span></td>
<!--        <td><a href="auction.php?number={$auction->auction_number}&txnid={$auction->txnid}&delins={$ins->id}">Delete</a></td>-->
      </tr>
      {/foreach}
    </table>
		{if $rating_case_id}
<br>
		<a target="_blank" href='rating_case.php?id={$rating_case_id}'>Rating case #{$rating_case_id}</a> 
		{/if}

{if $second_chanced}
<br>
    <table border="1">
      <tr>
        <td colspan="2" align="center"><h3>Second chanced for this auction</h3></td>
      </tr>
      <tr>
        <td><b>Aucton number</b></td>
        <td><b>Buyer</b></td>
      </tr>
      {foreach from=$second_chanced item="row"}
      <tr>
        <td><a href="auction.php?number={$row->auction_number}&txnid={$row->txnid}">{$row->auction_number}/{$row->txnid}</a></td>
        <td>&nbsp;{$row->expected_buyer}</td>
      </tr>
      {/foreach}
    </table>
{/if}
<br>

{if $auction->subinvoices}
    <table border="1">
      <tr>
        <td colspan="2" align="center"><h3><font color="red">Subinvoices</font></h3></td>
      </tr>
<!--      <tr>
        <td><b>Aucton number</b></td>
        <td>&nbsp;</td>
      </tr>-->
      {foreach from=$auction->subinvoices item="subinvoice"}
      <tr {if $subinvoice->deleted}style="text-decoration:line-through"{/if}>
        <td><a href="auction.php?number={$subinvoice->auction_number}&txnid={$subinvoice->txnid}">{$subinvoice->auction_number}/{$subinvoice->txnid}</a></td>
<!--        <td><a href="auction.php?number={$auction->auction_number}&txnid={$auction->txnid}&rnumber={$subinvoice->auction_number}&rtxnid={$subinvoice->txnid}&release=1">Release</a></td>-->
      </tr>
      {/foreach}
    </table>
{/if}
    <form method="post" action="auction.php">
    <input type="hidden" name="number" value="{$auction->auction_number}">
    <input type="hidden" name="txnid" value="{$auction->txnid}">
    <input type="hidden" id="username" value="{$auction->username}">
    <input type="hidden" id="shop_id" value="{$shop_id}">
    <input type="hidden" id="type" value="{if $auction->txnid==3}3{elseif $auction->txnid<=1}1{else}2{/if}">
Add a new subinvoice: 
<table border="0">
	<tr>
        <td nowrap valign="top">
            <input type="text" name="new_sa_quantity" maxlength="2" size="2">X
        </td>
        <td>
<input type="text" id="offer" name="offer" size="70" value=""/>
<input type="hidden" name="offer_id" id="offer_id" value=""/>
<script>
{literal}
function fill_sas(offer_id) {
		 JsHttpRequest.query(
		            'js_backend.php', // backend
		            {
		                // pass a text value 
		                'fn': 'fill_sas',
		                'username': document.getElementById('username').value,
		                'shop_id': document.getElementById('shop_id').value,
		                'offer_id': offer_id,
		                'type': document.getElementById('type').value
		            },
		            // Function is called when an answer arrives. 
		            function(result, errors) {
		                // Write the answer.
		                document.getElementById('div_sa').innerHTML = result["res"];
		            },
		            true  // do not disable caching
		        );
}
function chooseSiteid(saved_id) {
		 JsHttpRequest.query(
		            'js_backend.php', // backend
		            {
		                // pass a text value 
		                'fn': 'get_by_sa',
		                'saved_id': saved_id,
		                'field': 'siteid'
		            },
		            // Function is called when an answer arrives. 
		            function(result, errors) {
		                // Write the answer.
		                document.getElementById('new_sa_price').value = result["res1"];
		            },
		            true  // do not disable caching
		        );
}
					    var item_id_xml1 = {
							 script:"getOffer.php?utf=1&username="+document.getElementById('username').value+"&shop_id="+document.getElementById('shop_id').value+"&type="+document.getElementById('type').value+"&",
							 cache:false,
							 minchars:2,
							 varname:"input",
							 callback:function (obj) {resarr=obj.info.split("|"); document.getElementById('offer').value = resarr[0]+': '+resarr[1]; document.getElementById('offer_id').value = resarr[0]; fill_sas(resarr[0]);} 
						 };
						 var it_id_xml = new AutoSuggest('offer', item_id_xml1);
{/literal}
</script>
<div id="div_sa">
      <select name="saved_id" id="saved_id" onChange="chooseSiteid(this.value)">
<option label="no sa" value="0" selected>no sa</option>
        {html_options options=$sas selected=$vars.saved_id}
      </select>
	</div>
        </td>
        <td nowrap valign="top">
            <input type="text" id="new_sa_price" name="new_sa_price" maxlength="12" size="10" value="0" style="text-align:right">{$currency}
        </td>
        <td valign="top">
            <input type="submit" name="new_ai_add" value="Add">
        </td>
	</tr>
</table>
    </form>

{if $auction->main_auction_number}
<h3><font color="red">Main Auftrag number</font></h3>
<!--    <form method="post" action="auction.php">
    <input type="hidden" name="number" value="{$auction->auction_number}">
    <input type="hidden" name="txnid" value="{$auction->txnid}">
    <input type="hidden" name="main_number" value="{$auction->main_auction_number}/{$auction->main_txnid}">-->
	<a href="auction.php?number={$auction->main_auction_number}&txnid={$auction->main_txnid}">{$auction->main_auction_number}/{$auction->main_txnid}</a>
<!--	<input type="submit" value="Release" name="releasesubinvoice">
    </form>-->
{else}
<!--	<input type="submit" value="Make it subinvoice" name="subinvoice">-->
{/if}
</td>
<td valign="top" align="right">


{if !$auction->main_auction_number}
    <table border="1">
      <tr>
        <td colspan="5" align="center"><h3>Open fees</h3></td>
      </tr>
      <tr>
        <td><b>Date</b></td>
        <td><b>Amount</b></td>
        <td><b>Open</b></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      {foreach from=$listingfees item="listingfee"}
      <tr>
        <td>{$listingfee->open_date}</td>
        <td>{$listingfee->amount}</td>
        <td>{if $listingfee->close_date}Closed {$listingfee->close_date} by {$listingfee->close_username}{else}Open{/if}</td>
        <td>{if !$listingfee->close_date}
			<a href="auction.php?number={$auction->auction_number}&txnid={$auction->txnid}&closelistingfee={$listingfee->id}">Close</a>
			{else}{/if}
		</td>
        <td><a href="auction.php?number={$auction->auction_number}&txnid={$auction->txnid}&dellistingfee={$listingfee->id}">Delete</a></td>
      </tr>
      {/foreach}
      <tr>
        <td colspan="5" align="right">
    <form method="post" action="auction.php">
		{$currency} <input type="text" name="listingfeeamount" size="6"> <input type="submit" name="open_listingfee" value="Open">
    <input type="hidden" name="number" value="{$auction->auction_number}">
    <input type="hidden" name="txnid" value="{$auction->txnid}">
    </form>
		</td>
      </tr>
	  <tr><td colspan="5" >
    <table border="1" width="100%">
      <tr>
        <td colspan="10" align="center"><h3>Payments</h3></td>
      </tr>
      <tr>
        <td><b>Date</b></td>
        <td><b>Account</b></td>
        <td><b>Amount</b></td>
        <td><b>Exported</b></td>
        <td><b>Selling account</b></td>
        <td><b>VAT account</b></td>
        <td><b>VAT %</b></td>
        <td><b>Username</b></td>
        <td><b>Comment</b></td>
        <td>&nbsp;</td>
      </tr>
      {foreach from=$feepayments item="payment"}
      <tr>
        <td>{$payment->payment_date}</td>
        <td>{$payment->account|escape}</td>
        <td>{$payment->amount}</td>
        <td>{if $payment->exported}Yes{else}No{/if}</td>
        <td>{$payment->selling_account_number}</td>
        <td>{$payment->vat_account_number}</td>
        <td>{$payment->vat_percent}</td>
        <td>&nbsp;{$payment->username}</td>
        <td>&nbsp;{$payment->comment}</td>
        <td><a href="auction.php?number={$auction->auction_number}&txnid={$auction->txnid}&delpay={$payment->payment_id}" onclick="return confirm('Are you sure?');">Delete</a></td>
      </tr>
      {/foreach}
      <tr>
        <td colspan="10" align="right"><b>Total</b> : {$currency} {$feepayment_total}</td>
      </tr>
      <tr>
        <td colspan="10" align="right"><b>Open amount</b> : {$currency} {$feeopen_amount}</td>
      </tr>
    </table>
    <form method="post" action="auction.php">
    Date {html_select_date end_year=2030 time=$payment_default_date_free}<br>
    Account <select name="feepayaccount">
    {html_options options=$accounts selected=$default_account_free}
    </select><br>
    VAT Account: <select name="feevataccount">
		<option label="" value="">---</option>
	    {html_options options=$accounts selected=$default_vat_account_free}
   		</select><br>
    Selling Account: <select name="feesellingaccount">
		<option label="" value="">---</option>
	    {html_options options=$accounts selected=$default_selling_account_free}
    	</select><br>
	{$currency} <input type="text" name="feepayamount" size="6"> <input type="submit" value="Make payment" 
		onClick="if (this.form.feevataccount.value!='' && this.form.feesellingaccount.value!='') return true; alert('Fill account fields first'); return false;">
	Comment <input type="text" name="feepaycomment" size="20">
    <input type="hidden" name="number" value="{$auction->auction_number}">
    <input type="hidden" name="txnid" value="{$auction->txnid}">
    </form>
	  </td></tr>
    </table>

{if $auction->payment_method}
     <table border="1" width="100%">
      <tr>
        <td colspan="9" align="center"><b>Tracking numbers</b>
	{if ($auction->delivery_date_real!='0000-00-00 00:00:00') }
  		<br>
		Auction 
		<a href='auction.php?number={$auction->auction_number}&txnid={$auction->txnid}'>{$auction->auction_number}/{$auction->txnid}</a>
		marked as shipped on {$auction->delivery_date_real} {if !$tracking_numbers} with NO tracking number and NO parcel {/if}
	{/if}   
		</td>
      </tr>
      <tr>
        <td nowrap><b>Packing date</b></td>
        <td><b>Shipping method</b></td>
        <td><b>Packet</b></td>
        <td><b>Number</b></td>
        <td><b>Packed by</b></td>
        <td><b>Document</b></td>
        <td></td>
        <td></td>
      </tr>
    {if $tracking_numbers}
      {foreach from=$tracking_numbers item="tn"}
      <form method="post"  enctype="multipart/form-data"  action="auction.php" id="tn{$tn->id|escape}">
      <input type="hidden" name="tnid" value="{$tn->id|escape}">
      <input type="hidden" name="ship{$tn->id}" id="ship{$tn->id}" value="0">
      <input type="hidden" name="ship" id="ship" value="0">
      <input type="hidden" name="script_url" value="auction.php?number={$auction->auction_number}&txnid={$auction->txnid}">
      <input type="hidden" name="number" value="{$auction->auction_number}">
      <input type="hidden" name="txnid" value="{$auction->txnid}">
      <tr>
        <td nowrap>{$tn->date_time}</td>
        <td><a href="method.php?id={$tn->shipping_method}" target="_blank">{$tn->company_name|escape}</a></td>
        <td>{$tn->packet_name|escape}</td>
        <td><a href="{$tn->tracking_url|escape}" target="_blank">{$tn->number|escape}</a></td>
        <td>{employee_pic username=$tn->username title=$tn->fullusername width=200}</td>
        <td>{if ($tn->doc)}<a href="doc.php?tnid={$tn->id}">{$tn->doc_name}</a>
			{else}<input type="file" name="tndoc" value="Add" size="10" value="" onChange="this.form.submit()">{/if}</td>
        <td align="center">{if ($tn->monitored)}Monitored on {$tn->monitored_date} by {$tn->monitored_by_name}
		<input type="button" value="Remove monitoring" onClick="location.href='auction.php?number={$auction->auction_number}&txnid={$auction->txnid}&tnid={$tn->id}&monitor=0';">
		{else}
		<input type="button" value="Monitor the number"	onClick="location.href='auction.php?number={$auction->auction_number}&txnid={$auction->txnid}&tnid={$tn->id}&monitor=1';">
		{/if}</td>
        <td><a href="auction.php?number={$auction->auction_number}&txnid={$auction->txnid}&d={$tn->date_time|escape}&delnum={$tn->number|escape}"
		onClick="return confirm('Do you realy want to delete?');">Delete</a></td>
      </tr>
      </form>
      {/foreach}
    {/if}
	</table>
	<form method="post" action="auction.php" id="frm_shipped" enctype="multipart/form-data">
    	<input type="hidden" name="number" value="{$auction->auction_number}">
    	<input type="hidden" name="txnid" value="{$auction->txnid}">
	<table border="1">
      <tr>
        <td colspan="9" align="left">
		<div id="divorders">
		{include file="orders.tpl"}
		</div>
		</td>
	  </tr>	
	  <tr>
      <tr>
        <td colspan="9" align="left">

			<select name="shipping_method_id">
					{html_options options=$methods}
			</select>
			<input type="submit" name="labels" value="Produce shipping labels" onclick="this.form.target='_blank'">
    <input type="submit" name="labeldefault" value="Shipping label express" onclick="this.form.target='_blank'"><br>
		</td>
	  </tr>
{if !$loggedUser->mobile_only && !$loggedUser->ware_mobile_only}
      <tr>
        <td colspan="9" align="left">
    {if (1 ||
				$auction->payment_method == 'cc_shp'  
				|| $auction->payment_method == 'pofi_shp'   
				|| $auction->payment_method == 'pp_shp'   
				|| $auction->payment_method == 'gc_shp'  
				|| $auction->payment_method == 1  
			)
	}
		<table>
  <tr>
    <td colspan="2">
		<table id="tracking_number_table" border="0">
		<tbody>
		<tr>
	    <td align="right">
	      <b>Tracking #</b>
	    </td>
	    <td align="right">
	      <b>Shipping method</b>
	    </td>
	    <td align="right">
	      <b>Parcel type</b>
	    </td>
		</tr>
		<tr>
		<td id="tracking_number_td">
		<input type="text" name="tracking_number[]" onChange="set_method(this);" id="0"> <input type="button" value="+" onClick="addnumber()">
		</td>
	    <td align="right" id="method_td">
		<select name="method[]">
	    <option value="" selected>Automatic</option>
	    {html_options options=$methods}
	    </select>    </td>
	    <td align="right" id="packet_td">
		<select name="packet_id[]" id="packet_id">
	    <option value="" selected>---</option>
	    {html_options options=$packets}
	    </select>    </td>
		</tr>
		</tbody>
		</table>
    </td>
  </tr>
		</table>
  		Packed by <select id="username" name="username">
		        {html_options options=$users selected=$username}
		      </select>
		<div id="testdiv"></div>	  
<table border="0" width="100%"><tr><td>
<input type="submit" name="createnumber" value="Enter tracking number to parcel" onClick="if (checkAllMethods()) return true; alert('All methods should be set'); return false;"  {if $loggedUser->ware_mobile_only}disabled{/if}>
{if $pickup}
<input type="button" id="createnumber1" value="Ready to pickup" onClick="check_shipping_articles1();"  {if $loggedUser->ware_mobile_only}disabled{/if}>
<input type="button" id="createnumber" value="Picked up" onClick="check_shipping_articles();"  {if $loggedUser->ware_mobile_only}disabled{/if}>
{else}
<input type="button" id="createnumber" value="Ship selected articles" onClick="if (!checkAllMethods()) {ldelim} alert('All methods should be set'); return false; {rdelim} check_shipping_articles(); "  {if $loggedUser->ware_mobile_only}disabled{/if}>
{/if}
{if $loggedUser->unship}
</td><td align="right">
<input type="button" id="unship_button" value="unShip selected articles" onClick="unship();"/></td>
{/if}
</td></tr></table>
		<input type="hidden" name="shipnumbers" id="shipnumbers" value="0">
		<input type="hidden" name="tracking_number_submit" value="1">

		<input type="hidden" name="shipped" value="1"> 
      	<input type="hidden" name="ship" id="ship" value="0">
		<input type="hidden" name="checked" value="1">
<br>	
<script>
{literal}
function nobarcodes_clear(boid) {
		 JsHttpRequest.query(
		            'js_backend.php', // backend
		            {
		                // pass a text value 
		                'fn': 'clear_order_barcode',
		                'boid': boid
		            },
		            // Function is called when an answer arrives. 
		            function(result, errors) {
		                // Write the answer.
	       					document.getElementById('bar_order_'+result["res1"]).innerHTML = result["res"];
		            },
		            true  // do not disable caching
		        );
}
function clear_order_barcode(barcode, boid) {
		 JsHttpRequest.query(
		            'js_backend.php', // backend
		            {
		                // pass a text value 
		                'fn': 'clear_order_barcode',
		                'barcode': barcode,
		                'boid': boid
		            },
		            // Function is called when an answer arrives. 
		            function(result, errors) {
							if (result["res2"]!='') {
								alert(result["res2"]);
								return;
							}
		                // Write the answer.
	       					document.getElementById('bar_order_'+result["res1"]).innerHTML = result["res"];
		            },
		            true  // do not disable caching
		        );
}
function add_barcode(auction_id, barcode) {
		 JsHttpRequest.query(
		            'js_backend.php', // backend
		            {
		                // pass a text value 
		                'fn': 'add_barcode',
		                'auction_id': auction_id,
		                'barcode': barcode
		            },
		            // Function is called when an answer arrives. 
		            function(result, errors) {
		                // Write the answer.
						if (result["res"]=='')
							alert("Wrong barcode");
						else {
	       					document.getElementById('bar_order_'+result["res1"]).innerHTML = result["res"];
							document.getElementById('barcode').value = '';
							document.getElementById('barcode').focus();
						}
		            },
		            true  // do not disable caching
		        );
}
{/literal}
</script>
<input type="text" id="barcode" onkeydown="if (event.keyCode == 13) document.getElementById('btnbarcode').click()"/>
<input type="button"  id="btnbarcode" value="Add barcode" onClick="add_barcode(document.getElementById('auction_id').value, document.getElementById('barcode').value);"/>

    {/if}
		</td>
      </tr>
{/if}
</form>
    </table>
<form id="frm_methods">
{foreach from=$methodsall key="id" item="method"}
<input type="hidden" name="method_first_digits[{$method->shipping_method_id}]" value="{$method->first_digits}">
{/foreach}
</form>

    {if 
        ($auction->payment_method == 4 || $auction->payment_method == 3) && ! $auction->ready_to_pickup
    }
    <form method="post" action="auction.php">
    Warehouse <select name="pickup_warehouse">
     {html_options options=$warehouses}
     </select><br>
    <input type="submit" name="ready_to_pickup" value="Ready to pickup">
    <input type="hidden" name="number" value="{$auction->auction_number}">
    <input type="hidden" name="txnid" value="{$auction->txnid}">
    </form>
    {/if}
    
    {if 
        $auction->ready_to_pickup && ! $auction->pickedup
    }
    <form method="post" action="auction.php">
    <input type="submit" name="pickedup" value="Picked up">
    <input type="hidden" name="number" value="{$auction->auction_number}">
    <input type="hidden" name="txnid" value="{$auction->txnid}">
    </form>
    {/if}
{/if}    
    <table border="1">
      <tr>
        <td colspan="3" align="center"><h3>Printer Log</h3></td>
      </tr>
      <tr>
        <td><b>Date</b></td>
        <td><b>Person</b></td>
        <td><b>Action</b></td>
      </tr>
      {foreach from=$printerLog item="logentry"}
      <tr>
        <td>{$logentry->log_date}</td>
        <td>{employee_pic username=$logentry->username width=200}</td>
        <td>{$logentry->action|escape}</td>
      </tr>
      {/foreach}
    </table>

<br>
    <table border="1">
      <tr>
        <td colspan="4" align="center"><h3>Generated labels</h3></td>
      </tr>
      <tr>
        <td><b>Date</b></td>
        <td><b>Person</b></td>
        <td><b>Tracking number</b></td>
		<td><b>Label PDF</b></td>
      </tr>
      {foreach from=$labelLog item="logentry"}
      <tr>
        <td>&nbsp;{$logentry->updated}</td>
        <td>{employee_pic username=$logentry->username width=200}</td>
        <td>{$logentry->tracking_number}</td>
        <td>{if ($logentry->doc)}<a href="doc.php?auction_label={$logentry->id}">PDF</a>{/if}</td>
      </tr>
      {/foreach}
    </table>
</td>
</tr>
</table>
{/if}

</td></tr></table>
</td></tr></table>
</td></tr></table>
<table><tr><td>
<table><tr><td>
<table>
<tr><td colspan=2>
{if $acls.Calculation>0}
<br>
    <table border="1" width="100%">
      <tr>
        <td colspan="20" align="center"><h3>Calculations</h3></td>
      </tr>
      <tr>
        <td><b>Article</b></td>
        <td>Quantity</td>
        <td>Currency</td>
        <td>Seller account</td>
        <td>Source seller</td>
        <td>Country</td>
        <td>Seller type</td>
        <td>Price sold EUR ({$currency})</td>
        <td>Listing fee EUR ({$currency})</td>
        <td>Additional Listing fee EUR</td>
        <td>Commission EUR ({$currency})</td>
        <td>VAT EUR ({$currency})</td>
        <td><b>Netto sales price EUR ({$currency})</b></td>
        <td>Purchase price EUR ({$currency})</td>
        <td><b>Brutto income EUR ({$currency})</b></td>
        <td>Shipping price EUR ({$currency})</td>
        <td>Shipping VAT ({$currency})</td>
        <td>Effective shipping cost ({$currency})</td>
        <td>COD cost + payment fee EUR ({$currency})</td>
        <td>COD cost + payment fee VAT ({$currency})</td>
        <td>Effective COD cost ({$currency})</td>
        <td>Income shipping cost EUR ({$currency})</td>
        <td>Cost packing matherial EUR ({$currency})</td>
        <td><b>Revenue</b></td>
        <td><b>Margin %</b></td>
        <td><b>Brutto income 2</b></td>
        <td><b>Brutto income per item</b></td>
      </tr>
      {foreach from=$calcs item="calc"}
	  {if !$calc->hidden}
{if $calc->sum}
      <tr>
        <td colspan="20"></td>
      </tr>
{/if}      
{if $calc->article_admin || $calc->article_admin==''}
        <td><b>{$calc->name}</b></td>
{else}		
        <td>{article_pic article_id=$calc->article_id title=$calc->name width=200}</td>
{/if}		
        <td>{$calc->quantity}</td>
        <td>{$currency}</td>
        <td>{$calc->username}</td>
		<td>{$calc->source_seller}</td>
        <td>{$calc->site_country}</td>
        <td>{$calc->txnid_type}</td>
        <td nowrap>{$calc->price_sold_EUR|string_format:"%8.2f"}  <br>({$calc->price_sold|string_format:"%8.2f"})</td>
        <td nowrap>{$calc->ebay_listing_fee_EUR|string_format:"%8.2f"}  <br>({$calc->ebay_listing_fee|string_format:"%8.2f"})</td>
        <td nowrap>{$calc->additional_listing_fee_EUR|string_format:"%8.2f"}  <br>({$calc->additional_listing_fee|string_format:"%8.2f"})</td>
        <td nowrap>{$calc->ebay_commission_EUR|string_format:"%8.2f"}  <br>({$calc->ebay_commission|string_format:"%8.2f"})</td>
        <td nowrap>{$calc->vat_EUR|string_format:"%8.2f"}  <br>({$calc->vat|string_format:"%8.2f"})</td>
        <td nowrap><b>{$calc->netto_sales_price_EUR|string_format:"%8.2f"}  <br>({$calc->netto_sales_price|string_format:"%8.2f"})</b></td>
        <td nowrap>{$calc->purchase_price_EUR|string_format:"%8.2f"}  <br>({$calc->purchase_price|string_format:"%8.2f"})</td>
        <td nowrap><b>{$calc->brutto_income_EUR|string_format:"%8.2f"}  <br>({$calc->brutto_income|string_format:"%8.2f"})</b></td>
        <td nowrap>{$calc->shipping_cost_EUR|string_format:"%8.2f"}  <br>({$calc->shipping_cost|string_format:"%8.2f"})</td>
        <td nowrap>{$calc->vat_shipping_EUR|string_format:"%8.2f"}  <br>({$calc->vat_shipping|string_format:"%8.2f"})</td>
        <td nowrap>{$calc->effective_shipping_cost_EUR|string_format:"%8.2f"}  <br>({$calc->effective_shipping_cost|string_format:"%8.2f"})</td>
        <td nowrap>{$calc->COD_cost_EUR|string_format:"%8.2f"}  <br>({$calc->COD_cost|string_format:"%8.2f"})</td>
        <td nowrap>{$calc->vat_COD_EUR|string_format:"%8.2f"}  <br>({$calc->vat_COD|string_format:"%8.2f"})</td>
        <td nowrap>{$calc->effective_COD_cost_EUR|string_format:"%8.2f"}  <br>({$calc->effective_COD_cost|string_format:"%8.2f"})</td>
        <td nowrap>{$calc->income_shipping_cost_EUR|string_format:"%8.2f"}  <br>({$calc->income_shipping_cost|string_format:"%8.2f"})</td>
        <td nowrap>{$calc->packing_cost_EUR|string_format:"%8.2f"}  <br>({$calc->packing_cost|string_format:"%8.2f"})</td>
        <td nowrap><b>{$calc->revenue_EUR|string_format:"%8.2f"}  <br>({$calc->revenue|string_format:"%8.2f"})</b></td>
    {math equation="a*100/b" a=$calc->brutto_income_2|default:0 b=$calc->revenue|default:0 assign="margin"}
        <td nowrap><b>{$margin|string_format:"%8.2f"}</td>
        <td nowrap><b>{$calc->brutto_income_2_EUR|string_format:"%8.2f"}  <br>({$calc->brutto_income_2|string_format:"%8.2f"})</b></td>
        <td nowrap><b>{$calc->brutto_income_3_EUR|string_format:"%8.2f"}  <br>({$calc->brutto_income_3|string_format:"%8.2f"})</b></td>
      </tr>
	  {/if}
      {/foreach}

{if $auction->subinvoices}
      <tr>
        <td colspan="20" align="center"><h3>Subinvoice Calculations</h3></td>
      </tr>
      <tr>
        <td><b>Article</b></td>
        <td>Quantity</td>
        <td>Currency</td>
        <td>Seller account</td>
        <td>Source seller</td>
        <td>Country</td>
        <td>Seller type</td>
        <td>Price sold EUR ({$currency})</td>
        <td>Listing fee EUR ({$currency})</td>
        <td>Additional Listing fee EUR</td>
        <td>Commission EUR ({$currency})</td>
        <td>VAT EUR ({$currency})</td>
        <td><b>Netto sales price EUR ({$currency})</b></td>
        <td>Purchase price EUR ({$currency})</td>
        <td><b>Brutto income EUR ({$currency})</b></td>
        <td>Shipping price EUR ({$currency})</td>
        <td>Shipping VAT ({$currency})</td>
        <td>Effective shipping cost ({$currency})</td>
        <td>COD cost + payment fee EUR ({$currency})</td>
        <td>COD cost + payment fee VAT ({$currency})</td>
        <td>Effective COD cost ({$currency})</td>
        <td>Income shipping cost EUR ({$currency})</td>
        <td>Cost packing matherial EUR ({$currency})</td>
        <td><b>Revenue</b></td>
        <td><b>Margin %</b></td>
        <td><b>Brutto income 2</b></td>
        <td><b>Brutto income per item</b></td>
      </tr>
      {foreach from=$sub_calcs item="calc"}
	  {if !$calc->hidden}
{if $calc->sum}
      <tr>
        <td colspan="20"></td>
      </tr>
{/if}      
{if $calc->article_admin}
        <td><b>{$calc->name}</b></td>
{else}		
        <td>{article_pic article_id=$calc->article_id title=$calc->name width=200}</td>
{/if}		
        <td>{$calc->quantity}</td>
        <td>{$currency}</td>
        <td>{$calc->username}</td>
		<td>{$calc->source_seller}</td>
        <td>{$calc->site_country}</td>
        <td>{$calc->txnid_type}</td>
        <td nowrap>{$calc->price_sold_EUR|string_format:"%8.2f"}  <br>({$calc->price_sold|string_format:"%8.2f"})</td>
        <td nowrap>{$calc->ebay_listing_fee_EUR|string_format:"%8.2f"}  <br>({$calc->ebay_listing_fee|string_format:"%8.2f"})</td>
        <td nowrap>{$calc->additional_listing_fee_EUR|string_format:"%8.2f"}  <br>({$calc->additional_listing_fee|string_format:"%8.2f"})</td>
        <td nowrap>{$calc->ebay_commission_EUR|string_format:"%8.2f"}  <br>({$calc->ebay_commission|string_format:"%8.2f"})</td>
        <td nowrap>{$calc->vat_EUR|string_format:"%8.2f"}  <br>({$calc->vat|string_format:"%8.2f"})</td>
        <td nowrap><b>{$calc->netto_sales_price_EUR|string_format:"%8.2f"}  <br>({$calc->netto_sales_price|string_format:"%8.2f"})</b></td>
        <td nowrap>{$calc->purchase_price_EUR|string_format:"%8.2f"}  <br>({$calc->purchase_price|string_format:"%8.2f"})</td>
        <td nowrap><b>{$calc->brutto_income_EUR|string_format:"%8.2f"}  <br>({$calc->brutto_income|string_format:"%8.2f"})</b></td>
        <td nowrap>{$calc->shipping_cost_EUR|string_format:"%8.2f"}  <br>({$calc->shipping_cost|string_format:"%8.2f"})</td>
        <td nowrap>{$calc->vat_shipping_EUR|string_format:"%8.2f"}  <br>({$calc->vat_shipping|string_format:"%8.2f"})</td>
        <td nowrap>{$calc->effective_shipping_cost_EUR|string_format:"%8.2f"}  <br>({$calc->effective_shipping_cost|string_format:"%8.2f"})</td>
        <td nowrap>{$calc->COD_cost_EUR|string_format:"%8.2f"}  <br>({$calc->COD_cost|string_format:"%8.2f"})</td>
        <td nowrap>{$calc->vat_COD_EUR|string_format:"%8.2f"}  <br>({$calc->vat_COD|string_format:"%8.2f"})</td>
        <td nowrap>{$calc->effective_COD_cost_EUR|string_format:"%8.2f"}  <br>({$calc->effective_COD_cost|string_format:"%8.2f"})</td>
        <td nowrap>{$calc->income_shipping_cost_EUR|string_format:"%8.2f"}  <br>({$calc->income_shipping_cost|string_format:"%8.2f"})</td>
        <td nowrap>{$calc->packing_cost_EUR|string_format:"%8.2f"}  <br>({$calc->packing_cost|string_format:"%8.2f"})</td>
        <td nowrap><b>{$calc->revenue_EUR|string_format:"%8.2f"}  <br>({$calc->revenue|string_format:"%8.2f"})</b></td>
    {math equation="a*100/b" a=$calc->brutto_income_2|default:0 b=$calc->revenue|default:0 assign="margin"}
        <td nowrap><b>{$margin|string_format:"%8.2f"}</td>
        <td nowrap><b>{$calc->brutto_income_2_EUR|string_format:"%8.2f"}  <br>({$calc->brutto_income_2|string_format:"%8.2f"})</b></td>
        <td nowrap><b>{$calc->brutto_income_3_EUR|string_format:"%8.2f"}  <br>({$calc->brutto_income_3|string_format:"%8.2f"})</b></td>
      </tr>
	{/if}		
      {/foreach}
      {foreach from=$total_calcs item="calc"}
{if $calc->sum}
      <tr bgcolor="#c0c0c0">
        <td colspan="20"></td>
      </tr>
      <tr bgcolor="#c0c0c0">
        <td><b>Total both</b></td>
        <td>{$calc->quantity}</td>
        <td>{$currency}</td>
        <td>{$calc->username}</td>
		<td>{$calc->source_seller}</td>
        <td>{$calc->site_country}</td>
        <td>{$calc->txnid_type}</td>
        <td nowrap>{$calc->price_sold_EUR|string_format:"%8.2f"}  <br>({$calc->price_sold|string_format:"%8.2f"})</td>
        <td nowrap>{$calc->ebay_listing_fee_EUR|string_format:"%8.2f"}  <br>({$calc->ebay_listing_fee|string_format:"%8.2f"})</td>
        <td nowrap>{$calc->additional_listing_fee_EUR|string_format:"%8.2f"}  <br>({$calc->additional_listing_fee|string_format:"%8.2f"})</td>
        <td nowrap>{$calc->ebay_commission_EUR|string_format:"%8.2f"}  <br>({$calc->ebay_commission|string_format:"%8.2f"})</td>
        <td nowrap>{$calc->vat_EUR|string_format:"%8.2f"}  <br>({$calc->vat|string_format:"%8.2f"})</td>
        <td nowrap><b>{$calc->netto_sales_price_EUR|string_format:"%8.2f"}  <br>({$calc->netto_sales_price|string_format:"%8.2f"})</b></td>
        <td nowrap>{$calc->purchase_price_EUR|string_format:"%8.2f"}  <br>({$calc->purchase_price|string_format:"%8.2f"})</td>
        <td nowrap><b>{$calc->brutto_income_EUR|string_format:"%8.2f"}  <br>({$calc->brutto_income|string_format:"%8.2f"})</b></td>
        <td nowrap>{$calc->shipping_cost_EUR|string_format:"%8.2f"}  <br>({$calc->shipping_cost|string_format:"%8.2f"})</td>
        <td nowrap>{$calc->vat_shipping_EUR|string_format:"%8.2f"}  <br>({$calc->vat_shipping|string_format:"%8.2f"})</td>
        <td nowrap>{$calc->effective_shipping_cost_EUR|string_format:"%8.2f"}  <br>({$calc->effective_shipping_cost|string_format:"%8.2f"})</td>
        <td nowrap>{$calc->COD_cost_EUR|string_format:"%8.2f"}  <br>({$calc->COD_cost|string_format:"%8.2f"})</td>
        <td nowrap>{$calc->vat_COD_EUR|string_format:"%8.2f"}  <br>({$calc->vat_COD|string_format:"%8.2f"})</td>
        <td nowrap>{$calc->effective_COD_cost_EUR|string_format:"%8.2f"}  <br>({$calc->effective_COD_cost|string_format:"%8.2f"})</td>
        <td nowrap>{$calc->income_shipping_cost_EUR|string_format:"%8.2f"}  <br>({$calc->income_shipping_cost|string_format:"%8.2f"})</td>
        <td nowrap>{$calc->packing_cost_EUR|string_format:"%8.2f"}  <br>({$calc->packing_cost|string_format:"%8.2f"})</td>
        <td nowrap><b>{$calc->revenue_EUR|string_format:"%8.2f"}  <br>({$calc->revenue|string_format:"%8.2f"})</b></td>
    {math equation="a*100/b" a=$calc->brutto_income_2|default:0 b=$calc->revenue|default:0 assign="margin"}
        <td nowrap><b>{$margin|string_format:"%8.2f"}</td>
        <td nowrap><b>{$calc->brutto_income_2_EUR|string_format:"%8.2f"}  <br>({$calc->brutto_income_2|string_format:"%8.2f"})</b></td>
        <td nowrap><b>{$calc->brutto_income_3_EUR|string_format:"%8.2f"}  <br>({$calc->brutto_income_3|string_format:"%8.2f"})</b></td>
      </tr>
{/if}	
      {/foreach}
{/if}
    </table>
    <form method="post" action="auction.php">
    <input type="hidden" name="number" value="{$auction->auction_number}">
    <input type="hidden" name="txnid" value="{$auction->txnid}">
<table border="0" width="100%"><tr>
<td align="left"><input type="submit" value="Recalculate" name="recalculate"></td>
<td align="right"><input type="submit" value="Recalculate" name="recalculate"></td>
</tr></table>
</form>
{/if}
<p align="left">
<table border="0" width="100%"><tr><td align="left">
{if $auction->payment_method}
{if 1 || ($auction->delivery_date_real=='0000-00-00 00:00:00') }

<a name="invoice"></a>
    {literal}
<style>
    body, table, td {
      font-family: verdana;
      font-size:3mm;
    }
    b {
      font-size:3mm;
    }
    </style>
    {/literal}
<table style="width:170mm">
  <tr>
    <td>
    <img src="{$sellerInfo->logo_url}">
    </td>
    <td>
      {if $auction->name_shipping}
      <b>{$english.60}</b><br>
       <a style="color:black;text-decoration:none" href="gmap.php?id[]={$auction->id}">{$auction->company_shipping|escape}<br>
       {$english.$gender_shipping|escape} {$auction->firstname_shipping|escape} {$auction->name_shipping|escape}<br>
       {$auction->street_shipping|escape} {$auction->house_shipping|escape}<br>
       {$auction->zip_shipping|escape} {$auction->city_shipping|escape}<br>
       {$auction->country_shipping|escape}<br>
	   {if $auction->state_shipping}{$auction->state_shipping|escape}<br>{/if}
	   </a>
       {else}
       
       {/if}
    </td>
  </tr>
  <tr>
  <td colspan="2" style="height:1cm">&nbsp;
  
  </td>
  </tr>
  <tr>
    <td width="50%" valign="top">
      <b>{$english.61}</b><br>
       {$auction->company_invoice|escape}<br>
       {$english.$gender_invoice|escape} {$auction->firstname_invoice|escape} {$auction->name_invoice|escape}<br>
       {$auction->street_invoice|escape} {$auction->house_invoice|escape}<br>
       {if $auction->company_invoice!='' && $auction->state!='' && $ask_for_state}
		       {$auction->city_invoice|escape} {$auction->state|escape} {$auction->zip_invoice|escape} <br>
		{else}
		       {$auction->zip_invoice|escape} {$auction->city_invoice|escape}<br>
	   {/if}
       {$auction->country_invoice|escape}<br>
	   {if $auction->state_invoice}{$auction->state_invoice|escape}<br>{/if}
       {if $auction->company_invoice!='' && $auction->vat!='' && $ask_for_vat}{$english.225} {$auction->vat}<br>{/if}
    </td>
    <td width="50%" valign="top">
    <b>{$english.62}</b><br>
    {$sellerInfo->seller_name|escape}<br>
    {$sellerInfo->street}<br>
    {$sellerInfo->zip} {$sellerInfo->town}<br>
    {$sellerInfo->country_name}
	{if $ask_for_vat}<br>NIP {$sellerInfo->vat_id}{/if}
    </td>
  </tr>
  <tr>
    <td colspan="2" style="height:0.5cm">&nbsp;
    
    </td>
  </tr>
</table>
<table style="width:170mm">
<tr><td width="50%">
<span style="font-size:5mm">{$english.63}</span>
</td>
<td>
<img src="barcode.php?number={$auction->auction_number}&txnid={$auction->txnid}&type=int25">
</td>
</tr>
</table>
<p>
<table style="width:170mm;">
  <tr>
    <td width="40%" style="font-size:3mm" valign="top">
      <b>{$english.64} </b> {$invoice->invoice_date_utf8}
    </td>
    <td width="40%" style="font-size:3mm" valign="top">
      <b>{$english.65} </b> {$auction->auction_number}
    </td>
    <td width="20%" style="font-size:3mm" valign="top">
      <b>{$english.66} </b> {$invoice->invoice_number}
    </td>
  </tr>
  <tr>
    <td width="40%" style="font-size:3mm" valign="top">
				{assign var="tel_country_code_invoice" value=$auction->tel_country_code_invoice}
      			<b>{$english.67}</b> {$countries.$tel_country_code_invoice.phone_prefix} {$auction->tel_invoice}
	  			{if $auction->cel_invoice}
					{assign var="cel_country_code_invoice" value=$auction->cel_country_code_invoice}
	  				<br/><b>{$english.168}</b> {$countries.$cel_country_code_invoice.phone_prefix}  {$auction->cel_invoice}
	  			{/if}
    </td>
    <td width="40%" style="font-size:3mm" valign="top">
      <b> {$english.113}</b> {$total_weight} kg / {$total_volume} m3
    </td>
    <td width="20%" style="font-size:3mm" valign="top">
      <b>{$english.69}</b> {$invoice->invoice_date_utf8}
    </td>
  </tr>
  <tr>
{if $resp}
    <td width="40%" style="font-size:3mm" valign="top">
      <!--<b>{$english.120}</b> {$resp}-->
    </td>
    <td width="40%" style="font-size:3mm" valign="top">
	{if $auction->customer_vat}
      <b>{$english.136}</b> {$auction->customer_vat}
	{/if} 
    </td>
    <td width="20%" style="font-size:3mm" valign="top">
	{if $auction->delivery_date_customer!='0000-00-00'}
      <b>{$english.162} <font style="font-size:6mm">{$auction->delivery_date_customer}</font></b>
	{else}
      <b>{$english.162}</b> {$english.182}
	{/if} 
    </td>
  </tr>
  <tr>
      <td width="20%" style="font-size:3mm" valign="top">
	{if $auction->customer_id}
      <b>{$english.163} </b>{$auction->customer_id}
	{/if} 
    </td>
  <td/>
  <td/>
  </tr>
{/if}
<tr><td colspan="3"><b>{$english.188}:</b> {$payment_method}</td></tr>
</table>

<form method="post" action="auction.php" id="invoice_form"/>
    <input type="hidden" name="number" value="{$auction->auction_number}">
    <input type="hidden" name="txnid" value="{$auction->txnid}">

<table style="width:170mm" border="0">
  <tr>
    <td colspan="7"><hr></td>
  </tr>
  <tr>
    <td>
    <b>{$english.70}</b>
    </td>
    <td>
    <b>{$english.71}</b>
    </td>
    <td colspan="2">
    <b>{$english.73}</b>
    </td>
    <td>
    <b>{$english.74}</b>
    </td>
    <td colspan="2">
    <b>{$english.75}</b>
    </td>
  </tr>

  <tr>
    <td colspan="8"><hr></td>
  </tr>

{literal}
<script language="JavaScript" src="JsHttpRequest/JsHttpRequest.js"></script>
<script type="text/javascript">
function fill_containers(id, op_order_id, article_id) {
		 JsHttpRequest.query(
		            'js_backend.php', // backend
		            {
		                // pass a text value 
		                'fn': 'fill_containers',
		                'id': id,
		                'op_order_id': op_order_id,
		                'article_id': article_id
		            },
		            // Function is called when an answer arrives. 
		            function(result, errors) {
		                // Write the answer.
		                document.getElementById('div_spec_order_container'+id).innerHTML = result["res"];
		            },
		            true  // do not disable caching
		        );
}
</script>
{/literal}

  {foreach from=$order key="k" item="item"}
  {if (1 || !$item->hidden) && $item->manual!=4}
	    {math equation="a-1" a=$k assign="k_1"}
		{assign var="order_k_1" value=$order.$k_1}
		{if $item->saved_id != $order_k_1->saved_id && !$item->manual}
		 <tr>
		    <td valign="top" style="text-align: right; width: 642px;" colspan="7" align="right">
	     	   <b>{if $auction->txnid==3 && $item->ShopDesription!=''}{$item->ShopDesription}{else}{$item->alias}{/if}</b>
			</td>
		</tr>
	    <tr>
		    <td class="available_text" valign="top" style="text-align: right; width: 642px;" colspan="7" align="right">
	        	{$english_shop.47} {$item->available_text}
			</td>
		</tr>
		{/if}
  <tr>
    <td valign="top">
    {if 1 || ($item->manual!=2 && $item->manual!=3)}{$item->quantity}{/if}
    </td>
    <td valign="top">
    {if $item->manual==2}{$item->custom_title|default:$item->name|escape}
			<br><font style="font-size:8px ">
		    {$item->description}<br>
			</font>
    {elseif $item->manual==3}<a target="_blank" href="shop_voucher.php?id={$auction->code_id}&shop_id={$shop_id}">{$item->name}</a>
	{else}
		{if $item->alias_id}
			{article_pic article_id=$item->article_id title="A"|cat:$item->alias_id|cat:": "|cat:$item->custom_title|default:$item->alias_name width=200}
			<br><font style="font-size:8px ">
		    {$item->custom_description|default:$item->alias_description}<br>
			RA{$item->article_id}<br>
			</font>
		{else}
			{article_pic article_id=$item->article_id title=$item->custom_title_id|default:$item->name width=200}
			<br><font style="font-size:8px ">
		    {$item->custom_description|default:$item->description}<br>
			</font>
		{/if}	
	<font style="font-size:6px ">
	{foreach from=$item->parcels item="unit"}
	    {math equation="quantity/items_per_shipping_unit" quantity=$item->quantity items_per_shipping_unit=$item->items_per_shipping_unit assign="quantity"}
    	{math equation="price*quantity" price=$unit->dimension quantity=$quantity assign="dimension"}
		<br>{$quantity|number_format:2} x {$unit->dimension_l} x {$unit->dimension_w} x {$unit->dimension_h} = {$dimension}m3 {$unit->weight_parcel|string_format:"%8.1f"}kg BM{$unit->bandmass}
	{/foreach}
	</font>{/if}
	<br>
	<font color="{if !$item->sent}red{else}green{/if}" style="font-size:6px">
	{$item->numbers}<br>
	{if $item->sent}
	{assign var="send_warehouse_id" value=$item->send_warehouse_id}
	{$item->state} from warehouse {$warehouses.$send_warehouse_id}
	{else}
	{$item->state}
	{/if}
	<br>{$item->state_wwo} {$item->state_ops}
	</font>
    </td>
    <td valign="top" align="right">
			{if $item->price<0}
			<font color="red">{$currency}</font>
			{elseif $item->oldprice>0 && ($item->price < $item->oldprice)}
			<font style="text-decoration:line-through;">{$currency}</font><br/>
			<font color="red">{$currency}</font>
			{else}
    		{$currency}
			{/if}
    </td>
    <td valign="top" align="right">
			{if $item->price<0}
			<font color="red">{$item->price}</font>
			{elseif $item->oldprice>0 && ($item->price < $item->oldprice)}
			<font style="text-decoration:line-through;">{$item->oldprice}</font><br/>
			<font color="red">{$item->price}</font>
			{else}
    		{$item->price|escape}
			{/if}
    </td>
    <td valign="top">
    {$vat|default:0|escape}%
    </td>
    {math equation="price*quantity" price=$item->price quantity=$item->quantity assign="price"}
    {math equation="price*quantity" price=$item->oldprice|default:0 quantity=$item->quantity assign="oldprice"}
    <td  valign="top" align="right">
			{if $price<0}
			<font color="red">{$currency}</font>
			{elseif $oldprice>0 && ($price < $oldprice)}
			<font style="text-decoration:line-through;">{$currency} </font><br/>
			<font color="red">{$currency} </font>
			{else}
    		{$currency} 
			{/if}
    </td>
    <td  valign="top" align="right">
			{if $price<0}
			<font color="red">{$price|string_format:"%8.2f"}</font>
			{elseif $oldprice>0 && ($price < $oldprice)}
			<font style="text-decoration:line-through;">{$oldprice|string_format:"%8.2f"}</font><br/>
			<font color="red">{$price|string_format:"%8.2f"}</font>
			{else}
    		{$price|string_format:"%8.2f"}
			{/if}
    </td>
  </tr>
  <tr><td colspan="8">
	<br/>
	<table width="100%" border="1">
	<tr>
		<td width="50%" nowrap valign="top">
			{if $item->wwa_taken && $item->real_wwo_id}
			<a target="_blank" href="/ware2ware_order.php?id={$item->real_wwo_id}">WWO: {$item->real_wwo_id}</a>
			{else}
			{if $item->real_wwo_id}
			<a target="_blank" href="/ware2ware_order.php?id={$item->real_wwo_id}" id="a_w[{$item->id}]">WWO: </a>
			{else}
			<a href="javascript:" id="a_w[{$item->id}]">WWO: </a>
			{/if}
			<select name="wwo_order_id[{$item->id}]" 
onChange="if (this.value=='') {ldelim} document.getElementById('a_w[{$item->id}]').target=''; document.getElementById('a_w[{$item->id}]').href='javascript:'; {rdelim} else {ldelim} document.getElementById('a_w[{$item->id}]').target='_blank'; document.getElementById('a_w[{$item->id}]').href='/ware2ware_order.php?article_id='+this.value {rdelim}">
				<option label="" value="">---</option>
				{html_options options=$item->available_wwo_list selected=$item->wwo_order_id}
			</select>
			<input type="submit" value="Save"/>{/if}
			<br/>{$item->wwo_story}
		</td>
	</tr>
	<tr>
	<td width="50%">
		{if 1 || !$item->new_article}
		<input type="button" value="{if !$item->spec_order}Special ordered{else}Undo special ordered{/if}" onClick="checkOrderWares({$item->id}, this);"/>
		<br>
		{$item->story}
		<br>
		{/if}
		{if (1 || !$item->new_article)}
		<br>
		<table><tr>
		<td>
			{if $item->spec_order_id}
			<a target="_blank" href="/op_order.php?id={$item->spec_order_id}" id="a_o[{$item->id}]">OPS:</a> 
			{else}
			<a href="javascript:" id="a_o[{$item->id}]">OPS: </a>
			{/if}
		</td>
		<td>
		<select name="spec_order_id[{$item->id}]"  
onChange="fill_containers({$item->id}, this.value, '{$item->article_id}'); if (this.value=='') {ldelim} document.getElementById('a_o[{$item->id}]').target=''; document.getElementById('a_o[{$item->id}]').href='javascript:'; {rdelim} else {ldelim} document.getElementById('a_o[{$item->id}]').target='_blank'; document.getElementById('a_o[{$item->id}]').href='/op_order.php?id='+this.value {rdelim}">
			<option label="" value="">---</option>
			{html_options options=$item->available_ops_list selected=$item->spec_order_id}
		</select>
		</td>
		<td>
		Container
		</td>
		<td>
		<div id="div_spec_order_container{$item->id}">
			<select name="spec_order_container_id[{$item->id}]"/>
			<option label="" value="">---</option>
			{html_options options=$item->available_ops_containers_list selected=$item->spec_order_container_id}
			</select>
		</div>
		</td>
		<tr><td colspan="2"><b>Unavailable OPS</b><br>
		{foreach from=$item->unavailable_ops_list item="ops"}
			<a href="op_order.php?id={$ops->id}" target="_blank">{$ops->title}</a><br/>
		{/foreach}
		</td></tr>
		</tr></table>
		{/if}
		<input type="hidden" id="spec_order[{$item->id}]" name="spec_order[{$item->id}]" value="{$item->spec_order|default:0}"/>
		{if $item->spec_order || $item->new_article}<br>Comment: <br>{/if}<textarea name="spec_order_comment[{$item->id}]" {if !$item->spec_order && !$item->new_article}style="display:none"{/if}>{$item->spec_order_comment}</textarea><br>
<hr>
		<input type="hidden" id="lost_new_article[{$item->id}]" name="lost_new_article[{$item->id}]" value="{$item->lost_new_article|default:0}"/>
		<input type="hidden" id="new_article[{$item->id}]" name="new_article[{$item->id}]" value="{$item->new_article|default:0}"/>
	{if $item->available_new_article_list}
		<input type="hidden" id="new_article_other_qty[{$item->id}]" value="{$item->new_article_other_qty}"/>
		<input type="button" value="{if !$item->new_article}Taken from new article{else}Undo taken from new article{/if}" onClick="new_article({$item->id}, this);"
			{if $item->new_article_completed || $item->lost_new_article}disabled{/if}/>
	{/if}
		{if $item->new_article && !$item->new_article_completed}
			<input type="button" value="{if !$item->lost_new_article}Carton reported lost{else}Undo reported lost{/if}" 
			onClick="lost_new_article({$item->id}, this);"/>
		{/if}
		<br>
		{$item->new_article_story}
	{if $item->new_article}
			{if $item->new_article_id}
			{article_pic article_id=$item->new_article_id title="Article: "|cat:$item->new_article_id width=200 id="a_a["|cat:$item->id|cat:']'}
			{else}
			<a href="javascript:" id="a_a[{$item->id}]">Article: {$item->new_article_id}</a>
			{/if}
			{if $item->new_wwo_id}was moved by WWO#<a target="_blank" href="ware2ware_order.php?id={$item->new_wwo_id}">{$item->new_wwo_id}</a>{/if}
			
{if $auction->delivery_date_real=='0000-00-00 00:00:00' }
			<select name="new_article_id[{$item->id}]"  id="new_article_id{$item->id}" 
onChange="document.getElementById('uncomplete_article_order_id{$item->id}').value = ''; if (this.value=='') {ldelim} document.getElementById('a_a[{$item->id}]').target=''; document.getElementById('a_a[{$item->id}]').href='javascript:'; {rdelim} else {ldelim} document.getElementById('a_a[{$item->id}]').target='_blank'; document.getElementById('a_a[{$item->id}]').href='/article.php?original_article_id='+this.value {rdelim}"
{if $item->new_article_completed}disabled{/if}>
			<option label="" value="">---</option>
			{html_options options=$item->available_new_article_list selected=$item->new_article_id_warehouse_id}
			</select>
			{if $item->available_uncomplete_article_list}<br>
				{if $item->uncomplete_article_order_id}
				{article_pic article_id=$item->uncomplete_article_id title="or uncomplete articles" width=200 id="a_ua["|cat:$item->id|cat:']'}
				{else}
				<a href="javascript:" id="a_ua[{$item->id}]">or uncomplete articles</a>
				{/if}
				<select name="uncomplete_article_order_id[{$item->id}]"  id="uncomplete_article_order_id{$item->id}" 
					onChange="document.getElementById('new_article_id{$item->id}').value = '';"
					{if $item->new_article_completed}disabled{/if}>
				<option label="" value="">---</option>
				{html_options options=$item->available_uncomplete_article_list selected=$item->uncomplete_article_order_id}
				</select>
				{if $item->uncomplete_article_order_id}<br><a target="_blank" href="auction.php?number={$item->unc_auction_number}&txnid={$item->unc_txnid}#invoice">{$item->unc_auction_number}/{$item->unc_txnid}</a>{/if}
				{if $item->unc_wwo_id}was moved by WWO#<a target="_blank" href="ware2ware_order.php?id={$item->unc_wwo_id}">{$item->unc_wwo_id}</a>{/if}
			{else}
				<input type="hidden" id="uncomplete_article_order_id{$item->id}" />
			{/if}
<br/>
{else}
	{assign var="new_warehouse_id" value=$item->new_article_warehouse_id}
	{$item->new_article_warehouse}
	<input type="hidden" name="uncomplete_article_order_id[{$item->id}]"  id="uncomplete_article_order_id{$item->id}" value="{$item->uncomplete_article_order_id}"/>
	<input type="hidden" name="new_article_id[{$item->id}]"  id="new_article_id{$item->id}" value="{$item->new_article_id_warehouse_id}"/>
<br/>
{/if}
			Quantity: <input type="number" name="new_article_qnt[{$item->id}]" id="new_article_qnt{$item->id}" class="new_article_qnt" data-aid="{$item->id}" size="10" min="1" value="{$item->new_article_qnt}"
				{if $item->new_article_completed}disabled{/if}/>
			<div id="barcodes_container">Decompleted barcode #1: <input type="text" name="decompleted_barcodes[{$item->id}][]" value=""/></div>
			<input type="hidden" name="new_article_completed[{$item->id}]" id="new_article_completed[{$item->id}]" value="{$item->new_article_completed}"/>
			<input type="button" value="{if !$item->new_article_completed}Article completed{else}Undo article completed{/if}" 
				{if $item->lost_new_article}disabled{/if}
				onClick="new_article_completed_checking({$item->id}, this);"/>
			<br>
			{$item->new_article_completed_story}
			<!--<br>
			<input type="checkbox" name="new_article_not_deduct[{$item->id}]" value="1"
				{if $item->new_article_not_deduct}checked{/if}/> do not reserve new article-->
	{/if}

		<br>
		{if $item->spec_order || $item->new_article}<br><input type="submit" value="Save"/>
		<input type="button" value="Missing parts on new article PDF" 
		onClick="location.href='/auction.php?number={$auction->auction_number}&txnid={$auction->txnid}&new_article_pdf=1'"/>{/if}
		{if $item->spec_order_id && $item->spec_order}
		<br>
	<table border="1" align="center">
	  <tr>
	    <th>Order ID</th>
	    <th>Invoice no.</th>
	    <th>Containers</th>
	    <th>Quantity</th>
	  </tr>
	  <tr>
	    <td><a href="op_order.php?id={$item->order_info->id}">{$item->order_info->id}</a></td>
	    <td>&nbsp;{$item->order_info->invoice_number}</td>
	    <td>&nbsp;{$item->order_info->containers}</td>
	    <td>{$item->order_info->sum_ordered}</td>
	  </tr>
	</table>
		{/if}
	</td>
	</tr>
	</table>
  </td></tr>
  {/if}
		    {math equation="a+1" a=$k assign="k1"}
			{assign var="order_k1" value=$order.$k1}
			{if (($item->saved_id != $order_k1->saved_id) || (!$item->manual && $order_k1->manual)) && $order_k1->saved_id}
			  <tr  >
			    <td colspan="7"><hr style="width: 100%"></td>
			  </tr>
			{/if}
  {/foreach}

  <tr>
    <td colspan="7"><hr></td>
  </tr>

  <tr>
    <td colspan="2">
    {$english.76}
    </td>
    <td align="right">
    {$currency}
    </td>
    <td align="right">
    {$invoice->total_shipping|string_format:"%8.2f"}
    </td>
    <td>
    {$vat|default:0|escape}%
    {*    {$currency}{$invoice->vat_shipping} *}
    </td>
    <td align="right">
    {$currency}
    </td>
    <td align="right">
    {$invoice->total_shipping|string_format:"%8.2f"}
    </td>
  </tr>
{if $invoice->total_cod>0}
  <tr>
    <td colspan="2">
	{$english.104}:
    </td>
    <td align="right">
    {$currency}
    </td>
    <td align="right">
    {$invoice->total_cod|string_format:"%8.2f"}
    </td>
    <td>
    {$vat|default:0|escape}%
    {*    {$currency}{$invoice->vat_shipping} *}
    </td>
    <td align="right">
    {$currency}
    </td>
    <td align="right">
    {$invoice->total_cod|string_format:"%8.2f"}
    </td>
  </tr>
{/if}
{if $invoice->total_cc_fee>0}
  <tr>
    <td colspan="2">
	{$english_shop.52}:
    </td>
    <td align="right">
    {$currency}
    </td>
    <td align="right">
    {$invoice->total_cc_fee|string_format:"%8.2f"}
    </td>
    <td>
    {$vat|default:0|escape}%
    {*    {$currency}{$invoice->vat_shipping} *}
    </td>
    <td align="right">
    {$currency}
    </td>
    <td align="right">
    {$invoice->total_cc_fee|string_format:"%8.2f"}
    </td>
  </tr>
{/if}
    {math equation="price  + shipping" price=$invoice->total_price shipping=$invoice->total_cc_fee assign="price"}
    {math equation="price  + shipping" price=$price shipping=$invoice->total_shipping assign="price"}
    {math equation="price  + shipping" price=$price shipping=$invoice->total_cod assign="price"}
  <tr>
    <td colspan="7">
    <hr>
    </td>
  </tr>
  <tr>
    <td colspan="4">
    {$english.77}
    </td>
    <td>{*{$vat|escape}%*}</td>
   {math equation="price / (100+vat) * vat" price=$price vat=$vat assign="vatvalue"}
    <td align="right">{$currency}</td>
    <td align="right">{if $invoice->total_vat>0}{$invoice->total_vat|string_format:"%8.2f"}{else}
	{$vatvalue|string_format:"%8.2f"}{/if}</td>
  </tr>
  <tr  >
    <td colspan="4">
    {$english.232}
    </td>
    <td>
    &nbsp;
    </td>
{if $_config_api_par.24 || ($ss->id && !$ss->vat_included)}
		   {assign var="priceEXCLvat" value=$price}
{else}
	{if $invoice->total_vat>0}
		   {math equation="price-vat" price=$price vat=$invoice->total_vat assign="priceEXCLvat"}
	{else}
		   {math equation="price-vat" price=$price vat=$vatvalue assign="priceEXCLvat"}
	{/if}
{/if}
    <td align="right">{$currency}</td>
    <td align="right">{$priceEXCLvat|string_format:"%8.2f"}</td>
  </tr>
  <tr>
    <td colspan="7">
    <hr>
    </td>
  </tr>
  <tr>
    <td colspan="5">
    <b>{$english.78}</b> :
    </td>
    <td align="right">
    {$currency}
    </td>
    <td align="right">
	{if $invoice->total_vat}
	   {math equation="a+b" a=$invoice->total_vat b=$price assign="total_w_vat"}
		{$total_w_vat|string_format:"%8.2f"}
	{else}
	{$price|string_format:"%8.2f"}{/if}
    </td>
  </tr>
  <tr>
    <td colspan="7"><hr></td>
  </tr>
  <tr>
    <td colspan="7">{$sellerInfo_invoice_footer|nl2br}</td>
  </tr>
  {if $payments}
  <tr><td colspan="4" style="height:1cm;">&nbsp;</td></tr>
  <tr>
    <td colspan="5">
    <b>{$english.90}</b>
    <TABLE>
    {foreach from=$payments item="payment"}
    <tr><td>{$payment->payment_date}</td>
    <td>{$currency} {$payment->amount}</td></tr>
    {/foreach}
    </TABLE>
    </td>
  </tr>
  {/if}
  
</table>
</form>
{$outside_addon}
{else}
<a name="invoice"></a>
{$static_html}
{/if}
{/if}
</p>
</td></tr></table>
{/if}
<br>
<table border="1" align="center">
<tr bgcolor="CCFFCC" style="font-weight:bold;">
    <td rowspan="2" align="center"><b>Supplier</b></td>
    <td rowspan="2" align="center"><b>Supplier's products</b></td>
    <td rowspan="2" align="center"><b>Supplier Article #</b></td>
    <td rowspan="2" align="center"><b>Article Volume</b></td>
  {foreach from=$warehouses_table item="warename" key="warekey"}
    <td colspan="4" align="center">{$warename}</td>
  {/foreach}
	<td rowspan="2" align="center"><b>Orders in prod.</b></td>
  	<td rowspan="2" align="center"><b>Orders on the way</b></td>
  	<td rowspan="2" align="center"><b>Next EDA</b></td>
  	<td rowspan="2" align="center"><b>Sales per day</b></td>
  	<td rowspan="2" align="center"><b>Sales in period</b></td>
    <td rowspan="2" align="center"><b>Quantity to order</b></td>
</tr>
<tr>
  {foreach from=$warehouses_table item="warename" key="warekey"}
    <td align="center"><b>Last Months volume</b></td>
    <td align="center"><b>In Stock</b></td>
    <td align="center"><b>Available</b></td>
    <td align="center"><b>Reserved</b></td>
  {/foreach}
</tr>
{foreach from=$autos item="aline"}
{if $aline->grouphead}
<tr>
  <td colspan=140><b>{$aline->title}</b>({$aline->description}){if $aline->main} - Main group{/if}{if $aline->additional} - Additional group{/if}</td>
</tr>
{else}
<tr {if $aline->bgcolor} bgcolor="{$aline->bgcolor}" {/if}>
  <td align="left"><a target="_blank" href="op_suppliers.php?company_id={$aline->company_id}" style="color:{if $aline->is_rp}{$config.article_replacement_parts}{else}#0000FF{/if}">{$aline->supplier_name}</a></td>
  <td align="left">
  {if $aline->is_rp}
  	{article_pic article_id=$aline->article_id title=$aline->article_name width=200 color=$config.article_replacement_parts add_url="order="|cat:$order}
  {else}
  	{article_pic article_id=$aline->article_id title=$aline->article_name width=200 color='#0000FF'}
  {/if}
  </td>
  <td align="right" style="color:{if $aline->is_rp}{$config.article_replacement_parts}{else}#000000{/if}">&nbsp;{$aline->supplier_article_id}</td>
  <td align="right" style="color:{if $aline->is_rp}{$config.article_replacement_parts}{else}#000000{/if}">{$aline->article_volume_per_single_unit}</td>
  {foreach from=$warehouses_table item="warename" key="warekey"}
  <td align="right" style="color:{if $aline->is_rp}{$config.article_replacement_parts}{else}#000000{/if}">{$aline->sold90.$warekey|default:0|string_format:"%8.0f"}</td>
  <td align="right" style="color:{if $aline->is_rp}{$config.article_replacement_parts}{else}#000000{/if}">{$aline->pieces.$warekey|string_format:"%8.0f"}</td>
  <td align="right" style="color:{if $aline->is_rp}{$config.article_replacement_parts}{else}#000000{/if}">{$aline->available.$warekey|string_format:"%8.0f"}</td>
  <td align="right" style="color:{if $aline->is_rp}{$config.article_replacement_parts}{else}#000000{/if}">{$aline->reserved.$warekey|string_format:"%8.0f"}</td>
  {/foreach}
  <td align="right" style="color:{if $aline->is_rp}{$config.article_replacement_parts}{else}#000000{/if}">{$aline->order_in_prod_qnt|string_format:"%8.0f"}</td>
  <td align="right" style="color:{if $aline->is_rp}{$config.article_replacement_parts}{else}#000000{/if}">{$aline->order_on_way_qnt|string_format:"%8.0f"}</td>
  <td align="right" style="color:{if $aline->is_rp}{$config.article_replacement_parts}{else}#000000{/if}">&nbsp;{$aline->mineda}</td>
  <td align="right" style="color:{if $aline->is_rp}{$config.article_replacement_parts}{else}#000000{/if}">{$aline->sales_per_day|string_format:"%8.0f"}</td>
  <td align="right" style="color:{if $aline->is_rp}{$config.article_replacement_parts}{else}#000000{/if}">{$aline->sales_in_period|string_format:"%8.0f"}</td>
  <td align="right" style="color:{if $aline->is_rp}{$config.article_replacement_parts}{else}#000000{/if}">{$aline->quantity_to_order|string_format:"%8.0f"}</td>
</tr>
{/if}
{foreachelse}
  <td colspan="140" align="center">No lines</td>
{/foreach}
</tr>
</table>

<table><tr>
<td valign="top">{include file="page_log.tpl"}</td>
<td valign="top">{include file="_alarm_table.tpl"}</td>
</tr></table>
{include file="footer.tpl"}

