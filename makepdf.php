<?php require_once __DIR__.'/data/data.example.php'; ?>
<?php require_once __DIR__.'/helpers/Curl.class.php'; ?>
<?php require_once __DIR__.'/helpers/MakeFile.class.php'; ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="http://github.com/malsup/media/raw/master/jquery.media.js?v0.92"></script>
    <script type="text/javascript" src="jquery.metadata.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('a.media').media({width:500, height:400});
        });
    </script>
</head>
<body>
    <div class="container">
        <?php
            try {
                $restApiGetCall = new CurlHelperService();
                $response = $restApiGetCall->getParsingData(json_encode($requestDataExample), false);
                $fileHelper = new MakeFile($response, 'test2.pdf');
//                $fileHelper->makeFile($fileHelper->getSpecificElementLabel($response));
            } catch (Exception $e) {
                echo '<div class="alert alert-danger">' . $e->getMessage() . '</div>';
                exit;
            }
        ?>

        <h2>Your PDF is printed.</h2>
        <a class="media" href="test.pdf">Show PDF File</a>
        <p>
            <a href="/">Go to labels list</a>
        </p>
    </div>
</body>
</html>
