<?php

error_reporting(E_ALL^E_NOTICE^E_WARNING^E_DEPRECATED^E_STRICT);
ini_set('display_errors',1);
/**
 * This script is the main point of working
 * with a completed auction
 * @package eBay_After_Sale
 */
/**
 * @ignore
 */

/**
 *  uses local api configs
 */
 require_once 'config_api.php';

$debug = 0;
//if ($_SERVER_REMOTE_ADDR == '46.229.141.180') $debug = 1;
if (isset($_GET['debug'])) $debug = $_GET['debug'];

require_once 'connect.php';
require_once 'util.php';
$loggedIn = false;
global $warehouses2shipNames;
include 'login.php';
if (!$loggedIn) {
   	exit;
}
if (!isset($_POST['herzog'])) {
	checkPermission($loggedUser, 'auction', ACCESS_FULL);
}
		$acls = $loggedUser->accessLevelsacl();
//		print_r($acls);
		$smarty->assign('acls', $acls);

require_once 'lib/Warehouse.php';
require_once 'lib/Vat.php';
require_once 'lib/Rating.php';
require_once 'lib/Insurance.php';
require_once 'lib/Zip.php';
require_once 'lib/Offer.php';
require_once 'lib/Category.php';
require_once 'lib/Auction.php';
require_once 'lib/Account.php';
require_once 'lib/Article.php';
require_once 'lib/Invoice.php';
require_once 'lib/Order.php';
require_once 'lib/Payment.php';
require_once 'lib/PrinterLog.php';
require_once 'lib/EmailLog.php';
require_once 'lib/AuctionLog.php';
require_once 'lib/ShippingMethod.php';
require_once 'lib/ShippingPlan.php';
require_once 'lib/ShippingCost.php';
require_once 'lib/Warehouse.php';
require_once 'lib/ArticleHistory.php';
require_once 'lib/Rma.php';
require_once 'lib/Config.php';
require_once 'config.php';
require_once 'PPHttpPost.php';
include 'english.php';

if ($debug) echo '0: '.round((getmicrotime()-$time),3).'<br>';if ($debug) $time = getmicrotime();

$countries = $dbr->getAssoc("select country.code, country.*
	from country 
	");
$smarty->assign("countries", $countries);

$action = "Delete";
if ($_POST['action']){
    $action = $_POST['action'];
}
$timediff = $loggedUser->get('timezone');
	$now = ServerTolocal(date("Y-m-d H:i:s"), $timediff);
	$username = $loggedUser->get('username');

	$uri = (string)$_SERVER['SCRIPT_NAME']."?".(string)$_SERVER['QUERY_STRING'];
	if ('POST' == $_SERVER['REQUEST_METHOD']) {
		$r = $db->query("REPLACE INTO user_sort_order SET page='orders'
				, vars='".mysql_escape_string(serialize($_POST))."'
				, sortorder='".$sort."', direction='".$direction."', username='".$loggedUser->get('username')."'");
		if (PEAR::isError($r)) print_r($r);
		$orders_vars = $_POST;
	} else {
		$r = $dbr->getRow("SELECT * FROM user_sort_order WHERE page='orders' AND username='".$loggedUser->get('username')."'");
		if (!strlen($r->username)) $default_orders=true;
		$orders_vars = unserialize($r->vars);
	}
	$pack = array();
	foreach($orders_vars['pack'] as $rec) $pack[$rec] = 1;
	$ready2pickup = array();
	foreach($orders_vars['ready2pickup'] as $rec) $ready2pickup[$rec] = 1;
	$send = array();
	foreach($orders_vars['send'] as $rec) $send[$rec] = 1;
		$smarty->assign('default_orders', $default_orders);
		$smarty->assign('pack', $pack);
		$smarty->assign('ready2pickup', $ready2pickup);
		$smarty->assign('send', $send);
	if ($_POST['mass_'.$action]) {
        if (isset($_POST['export_to_ricardo'])) {
		    header("Content-type: application/excel; name=import.xls");
		    header("Content-disposition: attachment; filename=import.xls");
			require_once 'Spreadsheet/Excel/Writer.php'; 
			$fn = 'tmppic/temp'.rand(100000, 999999).'.xls';
			$workbook = new Spreadsheet_Excel_Writer($fn);
			$workbook->setTempDir('tmppic/');
			$sheet = $workbook->addWorksheet();
			$sheet->setInputEncoding('UTF-8');

			$RicardoAvailabilityValue = $dbr->getAssoc("select `key`,`value` from list_value where par_name='AvailabilityValue'");
			$fields = array(
				"Channel","ChannelName","CategoryID","CategoryType","Category","Lng1"
				,"Lng2","Title1","Title2","Desc1","Desc2","Quantity","IsUserTheme"
				,"ThemeID","ThemeName","StateID","StateValue","WarrantyID","WarrantyValue"
				,"Warranty1","Warranty2","RefNo","Img1","Img2","Img3","Img4","Img5","PriceStart"
				,"PriceBuyNow","PriceIncrement","PaymentID","PaymentValue","PaymentFlags","Payment1"
				,"Payment2","AvailabilityID","AvailabilityValue","DeliveryID","DeliveryValue"
				,"DeliveryCost","Delivery1","Delivery2","PromoBold","PromoHomePage","PromoTopCat"
				,"PromoGallery","PromoHighlight","StartDate","DaysDuration","HoursDuration"
				,"ReactTimes","Reminder","ReminderMins","ConditionID","Condition","MakeID","Make"
				,"Model","TypeID","Type","DateRegistration","FuelID","Fuel","TransmissionID"
				,"Transmission","GearsID","Gears","CylindersID","Cylinders","PowerKW","PowerHP"
				,"EngineCapacity","Seats","Doors","RegNum","CatalogPrice","RepairCosts","MotorExtColor1"
				,"MotorExtColor2","IsMetallicColor","MotorIntColor1","MotorIntColor2","MotorUpholstery1"
				,"MotorUpholstery2","DateInspection","ODO","WarrantyMonths","StarterTypeID","StarterType"
				,"PowerSourceID","PowerSource","MotorAccessories1","MotorAccessories2"
			);
			foreach ($fields as $key=>$field) $sheet->writeString(0,$key, $field);
	        $arrname=$_POST['action'].'_group'; 
			$n=0;
			if (count($_POST[$arrname])) foreach ($_POST[$arrname] as $id) {
				$n++;
			  	list($auction_number, $txnid) = explode('_', $id);
			  	$auction = new Auction($db, $dbr, $auction_number, $txnid);
				$details = unserialize($auction->get('details'));
				foreach ($fields as $key=>$field) {
					switch ($field) {
				        case 'Channel':
					       $sheet->writeNumber($n,$key, 1);
			            break;
				        case 'ChannelName':
					       $sheet->writeString($n,$key, 'Auktion');
			            break;
				        case 'CategoryType':
					       $sheet->writeString($n,$key, 'Normal');
			            break;
				        case 'Lng1':
					       $sheet->writeString($n,$key, 'de');
			            break;
				        case 'Lng2':
					       $sheet->writeString($n,$key, 'fr');
			            break;
				        case 'Category':
				        case 'StateValue':
				        case 'WarrantyValue':
					       $sheet->writeString($n,$key, $details['Ricardo'][$field]);
			            break;
				        case 'PriceIncrement':
					       $sheet->writeNumber($n,$key, $details['Ricardo'][$field]);
			            break;
				        case 'CategoryID':
				        case 'AvailabilityID':
					       $sheet->writeNumber($n,$key, $details['Ricardo'][$field]);
			            break;
				        case 'Warranty1':
				        case 'Warranty2':
					       $sheet->writeString($n,$key, $details['Ricardo']['Warranty']);
			            break;
				        case 'Title1':
				        case 'Title2':
					       $sheet->writeString($n,$key, $details['name']);
			            break;
				        case 'Desc1':
				        case 'Desc2':
					       $sheet->writeString($n,$key, $details['descriptionRicardo'][$details['DescrNumRicardo']]);
			            break;
				        case 'Quantity':
					       $sheet->writeNumber($n,$key, 1);
			            break;
				        case 'IsUserTheme':
					       $sheet->writeNumber($n,$key, 0);
			            break;
				        case 'ThemeID':
					       $sheet->writeNumber($n,$key, -1);
			            break;
				        case 'StateID':
					       $sheet->writeNumber($n,$key, 1);
			            break;
				        case 'WarrantyID':
					       $sheet->writeNumber($n,$key, 0);
			            break;
				        case 'RefNo':
					       $sheet->writeNumber($n,$key, $auction->get('RefNo'));
			            break;
				        case 'Img1':
					       $sheet->writeString($n,$key, $details['gallery']);
			            break;
				        case 'PriceStart':
					       $sheet->writeNumber($n,$key, number_format($details['startprice'], 2));
			            break;
				        case 'PriceBuyNow':
					       $sheet->writeNumber($n,$key, number_format($details['BuyItNowPrice'], 2));
			            break;
				        case 'PaymentID':
					       $sheet->writeNumber($n,$key, 5);
			            break;
				        case 'PaymentValue':
					       $sheet->writeString($n,$key, 'Vorauszahlung');
			            break;
				        case 'PaymentFlags':
					       $sheet->writeNumber($n,$key, 8192);
			            break;
				        case 'AvailabilityValue': // !!!!
					       $sheet->writeString($n,$key, $RicardoAvailabilityValue[$details['Ricardo']['AvailabilityID']]);
			            break;
				        case 'DeliveryID':
					       $sheet->writeNumber($n,$key, 7);
			            break;
				        case 'DeliveryValue':
					       $sheet->writeString($n,$key, 'Spediteur oder Kurier');
			            break;
				        case 'DeliveryCost':
					       $sheet->writeNumber($n,$key, number_format($details['ShippingCost'], 2));
			            break;
				        case 'PromoBold':
					       $sheet->writeNumber($n,$key, 0);
			            break;
				        case 'PromoHomePage':
					       $sheet->writeNumber($n,$key, 0);
			            break;
				        case 'PromoTopCat':
					       $sheet->writeNumber($n,$key, 0);
			            break;
				        case 'PromoGallery':
					       $sheet->writeNumber($n,$key, 1);
			            break;
				        case 'PromoHighlight':
					       $sheet->writeNumber($n,$key, 0);
			            break;
				        case 'StartDate':
					       $sheet->writeString($n,$key, $auction->get('start_time'));
			            break;
				        case 'DaysDuration':
					       $sheet->writeNumber($n,$key, 3);
			            break;
				        case 'HoursDuration':
					       $sheet->writeNumber($n,$key, 0);
			            break;
				        case 'ReactTimes':
					       $sheet->writeNumber($n,$key, 0);
			            break;
				        case 'Reminder':
					       $sheet->writeNumber($n,$key, 0);
			            break;
				        case 'ReminderMins':
					       $sheet->writeNumber($n,$key, 0);
			            break;
				        case 'IsMetallicColor':
					       $sheet->writeNumber($n,$key, 0);
			            break;
						default:
					       $sheet->writeString($n,$key, '');
			            break;
					} // switch
				} // foreach field	
			};	// foreach auction
			$workbook->close();        
			$csv = file_get_contents($fn);
			unlink($fn);
		    echo $csv;
		    exit;
        } elseif (isset($_POST['export']) || $_POST['mass_action']=='Export selected') {
			$p_cur = 1;
			$items_per_page = 999999999;
			$status = $_POST['status'];
			$status_username = requestVar('status_username');
			$sort_target = requestVar('sort_target');
			$s = $status;
				$sort1 = 'auction_number';
				$direction1 = '';
	            $resultsCur = Auction::findStatus($db, $dbr, array($s), $status_username, 
					($p_cur-1)*$items_per_page, $items_per_page,
					' ORDER BY '.$sort1.($direction1<0 ? ' DESC': ' ASC'));
				$sizeofresults = $dbr->getOne("SELECT FOUND_ROWS()");
				switch ($s) {
					case 'ticket_opened':
						$fields = array(
							'Offer' => 'offer_name',
							'Ticket created' => 'end_time',
							'Responsible' => 'responsible_uname',
							'Last Comment Date' => 'last_comment_date',
							'# Days since last comment' => 'last_comment_date_due',
							'Last Comment' => 'last_comment',
							'# Days due' => 'days_due',
							);
						$target = 'rma';
					break;
					case 'uncompleted':
						$fields = array(
							'Offer' => 'offer_name',
							'End Date' => 'end_time',
							'Responsible' => 'responsible_uname',
							'Paid price' => 'winning_bid',
							'Last Comment Date' => 'last_comment_date',
							'Last Comment' => 'last_comment',
							'# Days due' => 'days_due',
							);
						$target = 'auction';
					break;
					case 'unpaid':
						$fields = array(
							'Offer' => 'offer_name',
							'End Date' => 'end_time',
							'Responsible' => 'responsible_uname',
							'Last Comment Date' => 'last_comment_date',
							'Last Comment' => 'last_comment',
							'Total amount' => 'total_amount',
							'Paid amount' => 'paid_amount',
							'Open amount' => 'open_amount',
							'# Days due' => 'days_due',
							);
						$target = 'auction';
					break;
					case 'ready_to_ship':
						$fields = array(
							'Offer' => 'offer_name',
							'Assigned' => 'end_time',
							'Shipping Company' => 'responsible_uname',
							'Last Comment Date' => 'last_comment_date',
							'Last Comment' => 'last_comment',
							'Customer' => 'customer',
							'Planned Shipping Date' => 'delivery_date_customer',
							'# Days due' => 'days_due',
							);
						$target = 'auction';
					break;
					case 'paidNOTassigned':
						$fields = array(
							'Offer' => 'offer_name',
							'Assigned' => 'end_time',
							'Shipping Company' => 'responsible_uname',
							'Last Comment Date' => 'last_comment_date',
							'Last Comment' => 'last_comment',
							'Customer' => 'customer',
							'Planned Shipping Date' => 'delivery_date_customer',
							'# Days due' => 'days_due',
							);
						$target = 'auction';
					break;
					case 'ins':
						$fields = array(
							'Offer' => 'offer_name',
							'Shipping Company' => 'shipping_username',
							'Responsible' => 'responsible_uname',
							'Last Comment Date' => 'last_comment_date',
							'Last Comment' => 'last_comment',
							'Open amount' => 'ins_open_amount',
							'Claim Date' => 'claim_date',
							'Responsible' => 'responsible_uname',
							'# Days due' => 'days_due',
							);
						$target = 'ins';
					break;
					case 'shipped':
						$fields = array(
							'Offer' => 'offer_name',
							'Assigned' => 'end_time',
							'Shipping Company' => 'responsible_uname',
							'Last Comment Date' => 'last_comment_date',
							'Last Comment' => 'last_comment',
							'Customer' => 'customer',
							'Shipping Date' => 'delivery_date_real',
							'Tracking numbers' => 'tns',
							'# Days due' => 'days_due',
							);
						$target = 'auction';
					break;
					case 'ticket_closed':
						$fields = array(
							'Offer' => 'offer_name',
							'Ticket created' => 'end_time',
							'Responsible' => 'responsible_uname',
							'Last Comment Date' => 'last_comment_date',
							'Last Comment' => 'last_comment',
							'Ticket closed' => 'close_date',
							'# Days due' => 'days_due',
							);
						$target = 'rma';
					break;
					case 'deleted':
						$fields = array(
							'Offer' => 'offer_name',
							'End Date' => 'end_time',
							'Responsible' => 'responsible_uname',
							'Last Comment Date' => 'last_comment_date',
							'Last Comment' => 'last_comment',
							'Cancelled Order Date' => 'deleted_date',
							'Total amount' => 'total_amount',
							);
						$target = 'auction';
					break;
				} // switch
	        $arrname=$_POST['action'].'_group'; 
		    $smarty->assign('selected', $_POST[$arrname]);
	    	$fields['Email stopped']='freeze_date';
		    $smarty->assign('target', $target);
		    $smarty->assign('fields', $fields);
			foreach($resultsCur as $key=>$var) {
				if (!in_array($var->auction_number.'_'.$var->txnid, $_POST['Delete_group'])) unset($resultsCur[$key]);
				else $resultsCur[$key]->last_comment = strip_tags($resultsCur[$key]->last_comment);
			}
		    $smarty->assign('results', $resultsCur);
		    header("Content-type: application/excel; name=search_result_$s.csv");
		    header("Content-disposition: attachment; filename=search_result_$s.csv");
			$smarty->display('results_csv.tpl');
		    exit;
        } elseif (isset($_POST['dhl'])) {
		    header("Content-type: text/plain");
#			header("Content-length: " . filesize($filename));
		    header("Content-disposition: attachment; filename=export.txt");
			$addresses = array();
			$pos = 0;
			if (count((array)$_POST[$action.'_group'])) foreach ((array)$_POST[$action.'_group'] as $id) {
				$pos++;
			  	list($auction_number, $txnid) = explode('_', $id);
			  	$auction = new Auction($db, $dbr, $auction_number, $txnid);
				$date = date("Ymd");
				$english = Auction::getTranslation($db, $dbr, $auction->get('siteid'), $auction->getMyLang());
				$auction->data->gender_shipping = $english[$auction->data->gender_shipping];
				$EPI_BPI = $dbr->getOne("select eu from country where code='"
					.countryToCountryCode($auction->data->country_shipping)."'")?'EPI':'BPI';
//				$EPI_BPI = countryToCountryCode($auction->data->country_shipping);
				$weight = $auction->getTotalWeight();
				if (preg_match_all( "/\d/", $auction->data->street_shipping, $matches  ) ) {
					$vars = explode($matches[0][0], $auction->data->street_shipping);
					$street_name = trim($vars[0]);
					$street_number = trim(str_replace($street_name, '', $auction->data->street_shipping));
				} else {
					$street_number = '';
					$street_name = $auction->data->street_shipping;
				}
				if (!strlen($street_number)) $street_number='-';
				$street_number = str_replace(' ','',$street_number);
				$country_shipping_code = countryToCountryCode($auction->data->country_shipping);
				if ($country_shipping_code=='UK') $country_shipping_code='GB';
				$phone = strlen($auction->data->cel_shipping)?$auction->data->cel_shipping:
					(strlen($auction->data->tel_shipping)?$auction->data->tel_shipping:'0');
				$company_shipping1 = substr($auction->data->company_shipping, 0, 30);
				$company_shipping2 = substr($auction->data->company_shipping, 30, 30);
				$company_shipping3 = substr($auction->data->company_shipping, 60, 30);
				switch ($auction->get('lang')) {
					case 'polish':
						setlocale(LC_ALL, 'pl_PL');
						break;
					case 'german':
						setlocale(LC_ALL, 'de_DE');
						break;
					case 'english':
						setlocale(LC_ALL, 'en_EN');
						break;
					case 'french':
						setlocale(LC_ALL, 'fr_FR');
						break;
					case 'spanish':
						setlocale(LC_ALL, 'es_ES');
						break;
				}
				if (strlen(iconv('UTF-8', 'ASCII//TRANSLIT', ($auction->data->firstname_shipping))))
					$auction->data->firstname_shipping = iconv('UTF-8', 'ASCII//TRANSLIT', ($auction->data->firstname_shipping));
				else $auction->data->firstname_shipping = utf8_decode($auction->data->firstname_shipping);
				if (strlen(iconv('UTF-8', 'ASCII//TRANSLIT', ($auction->data->name_shipping))))
					$auction->data->name_shipping = iconv('UTF-8', 'ASCII//TRANSLIT', ($auction->data->name_shipping));
				else $auction->data->name_shipping = utf8_decode($auction->data->name_shipping);
				$name = $auction->data->gender_shipping.' '.$auction->data->firstname_shipping.' '.$auction->data->name_shipping;
				if (strlen($name)>30) 
					$name = $auction->data->gender_shipping.' '.$auction->data->firstname_shipping[0].'.'.$auction->data->name_shipping;
				$zip = $auction->data->zip_shipping;
				if ($country_shipping_code=='GB' && substr($zip, strlen($zip)-4, 1)!=' ')
					$zip = substr($zip, 0, strlen($zip)-3).' '.substr($zip, strlen($zip)-3);

				if (strlen(iconv('UTF-8', 'ASCII//TRANSLIT', ($company_shipping1))))
					$company_shipping1 = iconv('UTF-8', 'ASCII//TRANSLIT', ($company_shipping1));
				else $company_shipping1 = utf8_decode($company_shipping1);
				if (strlen(iconv('UTF-8', 'ASCII//TRANSLIT', ($company_shipping2))))
					$company_shipping2 = iconv('UTF-8', 'ASCII//TRANSLIT', ($company_shipping2));
				else $company_shipping2 = utf8_decode($company_shipping2);
				if (strlen(iconv('UTF-8', 'ASCII//TRANSLIT', ($company_shipping3))))
					$company_shipping3 = iconv('UTF-8', 'ASCII//TRANSLIT', ($company_shipping3));
				else $company_shipping3 = utf8_decode($company_shipping3);
				if (strlen(iconv('UTF-8', 'ASCII//TRANSLIT', ($street_name))))
					$street_name = iconv('UTF-8', 'ASCII//TRANSLIT', ($street_name));
				else $street_name = utf8_decode($street_name);
				if (strlen(iconv('UTF-8', 'ASCII//TRANSLIT', ($auction->data->city_shipping))))
					$auction->data->city_shipping = iconv('UTF-8', 'ASCII//TRANSLIT', ($auction->data->city_shipping));
				else $auction->data->city_shipping = utf8_decode($auction->data->city_shipping);

				$str =  "$pos|DPEE-SHIPMENT|{$EPI_BPI}|$date|||||{$auction->data->invoice_number}|||||||||||||||||||||||||02|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
$pos|DPEE-SENDER|6079345304|Lagerhaus Prologistics|Qualitrade GmbH|Zentrallager|Bruessower Allee|91|Gewerbegebiet Ost Str A4|17291|Prenzlau|DE||mail@qualitradegmbh.com||||||||||||||||||||||
$pos|DPEE-RECEIVER|".(strlen($company_shipping1)?$company_shipping1:$name)."|$company_shipping2|$company_shipping3||$name|{$street_name}|{$street_number}||{$zip}|{$auction->data->city_shipping}|".$country_shipping_code."|||".$phone."||||||||||||||
$pos|DPEE-ITEM|{$weight}||||||
";
				echo $str;
				$auction->addShippingComment('exported to DHL'
				  , $loggedUser->get('username')
				  , serverToLocal(date('Y-m-d H:i:s'), $timediff));
			}
		    exit;
        } elseif (isset($_POST['tof'])) {
		    header("Content-type: application/excel; name=tof.xls");
		    header("Content-disposition: attachment; filename=tof.xls");
			require_once 'Spreadsheet/Excel/Writer.php'; 
			$fn = 'tmppic/temp'.rand(100000, 999999).'.xls';
			$workbook = new Spreadsheet_Excel_Writer($fn);
			$workbook->setTempDir('tmppic/');
			$sheet = $workbook->addWorksheet();
			$sheet->setInputEncoding('UTF-8');
			$fields = array(
				"Company Name","First Name","Last Name","Street","PLZ","City","Auftrag","Parcels","Weight"
			);
			foreach ($fields as $key=>$field) $sheet->writeString(0,$key, $field);
			$row = 1;
			if (count((array)$_POST[$action.'_group'])) {
				foreach ((array)$_POST[$action.'_group'] as $id) {
				  	list($auction_number, $txnid) = explode('_', $id);
				  	$auction = new Auction($db, $dbr, $auction_number, $txnid);
					$weight = $auction->getTotalWeight();
					$parcels = $dbr->getOne("select sum(quantity) from orders 
						where auction_number={$auction->data->auction_number} and txnid={$auction->data->txnid}");
					$sheet->writeString($row, 0, utf8_decode($auction->data->company_shipping));
					$sheet->writeString($row, 1, utf8_decode($auction->data->firstname_shipping));
					$sheet->writeString($row, 2, utf8_decode($auction->data->name_shipping.' '.$auction->data->tel_shipping));
					$sheet->writeString($row, 3, utf8_decode($auction->data->street_shipping));
					$sheet->writeString($row, 4, utf8_decode($auction->data->zip_shipping));
					$sheet->writeString($row, 5, utf8_decode($auction->data->city_shipping));
					$sheet->writeString($row, 6, ($auction->data->auction_number.'/'.$auction->data->txnid));
					$sheet->writeNumber($row, 7, number_format($parcels,2));
					$sheet->writeNumber($row, 8, number_format($weight,2));
					$auction->addShippingComment('exported to TOF'
					  , $loggedUser->get('username')
					  , serverToLocal(date('Y-m-d H:i:s'), $timediff));
					$row++;
				}
			}
			$workbook->close();        
			$csv = file_get_contents($fn);
			unlink($fn);
		    echo $csv;
		    exit;
        } elseif (isset($_POST['packingPDF'])) {
			if (count((array)$_POST[$action.'_group'])) {
				$res = Invoice::getShippingListPDFs($db, $dbr, (array)$_POST[$action.'_group'], 1);
				foreach ((array)$_POST[$action.'_group'] as $id) {
				  	list($auction_number, $txnid) = explode('_', $id);
				    PrinterLog::Log($db, $dbr, $auction_number,  $txnid, $loggedUser->get('username'), 'Print packing PDF');
				}
			}
			header ("Content-type: application/pdf; name=packingPDF.pdf");
			header("Content-disposition: inline; filename=packingPDF.pdf");
			echo $res;
		    exit;
        } elseif (isset($_POST['dhlol'])) {
			$addresses = array();
			$pos = 0;
			$csv = "SEND_NAME1;SEND_NAME2;SEND_STREET;SEND_HOUSENUMBER;SEND_PLZ;SEND_CITY;SEND_COUNTRY;RECV_NAME1;RECV_NAME2;RECV_STREET;RECV_HOUSENUMBER;RECV_PLZ;RECV_CITY;RECV_COUNTRY;PRODUCT;COUPON\n";
			if (count((array)$_POST[$action.'_group'])) foreach ((array)$_POST[$action.'_group'] as $id) {
				$pos++;
			  	list($auction_number, $txnid) = explode('_', $id);
			  	$auction = new Auction($db, $dbr, $auction_number, $txnid);
				if (preg_match_all( "/\d/", $auction->data->street_shipping, $matches  ) ) {
					$street_number_shipping = implode('',$matches[0]);
					$street_name_shipping = $auction->data->street_shipping;
					foreach($matches[0] as $number) {
						$street_name_shipping = str_replace($number, '', $street_name_shipping);
					}
				} else {
					$street_number_shipping = '';
					$street_name_shipping = $auction->data->street_shipping;
				}
				if (!strlen($street_number_shipping)) $street_number_shipping='-';

					$csv .= utf8_decode($auction->get('firstname_shipping').';');
					$csv .= utf8_decode($auction->get('name_shipping').';');
					$csv .= utf8_decode($street_name_shipping.';');
					$csv .= $street_number_shipping.';';
					$csv .= utf8_decode($auction->get('zip_shipping').';');
					$csv .= utf8_decode($auction->get('city_shipping').';');
					$csv .= utf8_decode(countryToCountryCode($auction->get('country_shipping')).';');
					$csv .= "Lagerhaus;Prologistics;Seeweg;1;17291;Grunow;DE;";
					$csv .= $_POST['dhlol_PRODUCT'].';';
					$csv .= "\n";
			}
		    header("Content-type: application/excel; name=dhl_online.csv");
		    header("Content-disposition: attachment; filename=dhl_online.csv");
			echo $csv;
		    exit;
        } elseif (isset($_POST['herzog'])) {
			$addresses = array();
			$pos = 0;
			$csv = "Empfänger Name;Empfänger Land;Empfänger Ort;Empfänger PLZ;Empfänger Strasse;Empfänger HausNr.;Entladestelle Name;Entladestelle Land;Entladestelle Ort;Entladestelle PLZ;Entladestelle Strasse;Entladestelle HausNr.;Entladedatum;Entladezeit;Ladedatum;Ladezeit;Auftragsnr-Kunde;Lieferscheinnr-Kunde;Gesamt Gewicht;ArtikelNr-Kunde;Aritkelname;Menge1/Anzahl;Gewicht\n";
			if (count((array)$_POST[$action.'_group'])) foreach ((array)$_POST[$action.'_group'] as $id) {
				$pos++;
			  	list($auction_number, $txnid) = explode('_', $id);
			  	$auction = new Auction($db, $dbr, $auction_number, $txnid);
				$date = date("Ymd");
				$english = Auction::getTranslation($db, $dbr, $auction->get('siteid'), $auction->getMyLang());
				$auction->data->gender_shipping = $english[$auction->data->gender_shipping];
				$auction->data->gender_invoice = $english[$auction->data->gender_invoice];
				$EPI_BPI = $dbr->getOne("select eu from country where code='"
					.countryToCountryCode($auction->data->country_shipping)."'")?'EPI':'BPI';
//				$EPI_BPI = countryToCountryCode($auction->data->country_shipping);
				$weight = $auction->getTotalWeight();
				if (preg_match_all( "/\d/", $auction->data->street_invoice, $matches  ) ) {
					$street_number_invoice = implode('',$matches[0]);
					$street_name_invoice = $auction->data->street_invoice;
					foreach($matches[0] as $number) {
						$street_name_invoice = str_replace($number, '', $street_name_invoice);
					}
				} else {
					$street_number_invoice = '';
					$street_name_invoice = $auction->data->street_invoice;
				}
				if (!strlen($street_number_invoice)) $street_number_invoice='-';
				if (preg_match_all( "/\d/", $auction->data->street_shipping, $matches  ) ) {
					$street_number_shipping = implode('',$matches[0]);
					$street_name_shipping = $auction->data->street_shipping;
					foreach($matches[0] as $number) {
						$street_name_shipping = str_replace($number, '', $street_name_shipping);
					}
				} else {
					$street_number_shipping = '';
					$street_name_shipping = $auction->data->street_shipping;
				}
				if (!strlen($street_number_shipping)) $street_number_shipping='-';
				$country_shipping_code = countryToCountryCode($auction->data->country_shipping);
				if ($country_shipping_code=='UK') $country_shipping_code='GB';
				$phone = strlen($auction->data->cel_shipping)?$auction->data->cel_shipping:
					(strlen($auction->data->tel_shipping)?$auction->data->tel_shipping:'0');
				$company_shipping1 = substr($auction->data->company_shipping, 0, 30);
				$company_shipping2 = substr($auction->data->company_shipping, 30, 30);
				$company_shipping3 = substr($auction->data->company_shipping, 60, 30);
				$name = $auction->data->gender_shipping.' '.$auction->data->firstname_shipping.' '.$auction->data->name_shipping;
				if (strlen($name)>30) 
					$name = $auction->data->gender_shipping.' '.$auction->data->firstname_shipping[0].'.'.$auction->data->name_shipping;
				$zip = $auction->data->zip_shipping;
				if ($country_shipping_code=='GB' && substr($zip, strlen($zip)-4, 1)!=' ')
					$zip = substr($zip, 0, strlen($zip)-3).' '.substr($zip, strlen($zip)-3);
    			$allorder = Order::listAll($db, $dbr, $auction->get('auction_number'), $auction->get('txnid'), 1
					, $auction->getMyLang(), '0,1',1);
				foreach($allorder as $item) {
					$csv .= utf8_decode($auction->data->gender_invoice.' '.$auction->get('firstname_invoice').' '.$auction->get('name_invoice').';');
					$csv .= utf8_decode(countryToCountryCode($auction->get('country_invoice')).';');
					$csv .= utf8_decode($auction->get('city_invoice').';');
					$csv .= utf8_decode($auction->get('zip_invoice').';');
					$csv .= utf8_decode($street_name_invoice.';');
					$csv .= $street_number_invoice.';';
					$csv .= utf8_decode($auction->data->gender_shipping.' '.$auction->get('firstname_shipping').' '.$auction->get('name_shipping').';');
					$csv .= utf8_decode(countryToCountryCode($auction->get('country_shipping')).';');
					$csv .= utf8_decode($auction->get('city_shipping').';');
					$csv .= utf8_decode($auction->get('zip_shipping').';');
					$csv .= utf8_decode($street_name_shipping.';');
					$csv .= $street_number_shipping.';';
					$csv .= ($auction->data->delivery_date_customer!='0000-00-00'?$auction->data->delivery_date_customer:date('Y-m-d')).';';
					$csv .= "17.00;";
					$csv .= ($auction->data->delivery_date_customer!='0000-00-00'?$auction->data->delivery_date_customer:date('Y-m-d')).';';
					$csv .= "07.00;";
					$csv .= $auction->get('auction_number').'/'.$auction->get('txnid').';';
					$csv .= ';';
					$csv .= $weight.';';
					$csv .= $item->article_id.';';
					$csv .= utf8_decode("".(strlen($item->custom_title)?$item->custom_title:$item->name).';');
					$csv .= $item->quantity.';';
					$csv .= ($item->quantity*$item->weight).';';
					$csv .= "\n";
				}
				$auction->addShippingComment('exported to Herzog'
				  , $loggedUser->get('username')
				  , serverToLocal(date('Y-m-d H:i:s'), $timediff));
			}
		    header("Content-type: application/excel; name=herzog.csv");
		    header("Content-disposition: attachment; filename=herzog.csv");
			echo $csv;
		    exit;
        } elseif (isset($_POST['map'])) {
			$addresses = array();
			if (count((array)$_POST[$action.'_group'])) foreach ((array)$_POST[$action.'_group'] as $id) {
			  	list($auction_number, $txnid) = explode('_', $id);
			  	$auction = new Auction($db, $dbr, $auction_number, $txnid);
				$english = Auction::getTranslation($db, $dbr, $auction->get('siteid'), $auction->getMyLang());
//				print_r($auction); 
				$address['address'] = $auction->get('street_shipping').', '.$auction->get('zip_shipping').' '.$auction->get('city_shipping').', '.$auction->get('country_invoice');
				$address['title'] = '<a target="_blank" href="auction.php?number='.$auction_number.'&txnid='.$txnid.'">Sales#'.$auction_number.'/'.$txnid.'</a><br>'
					.'<a target="_blank" href="shipping_auction.php?number='.$auction_number.'&txnid='.$txnid.'">Shipping Order#'.$auction_number.'/'.$txnid.'</a><br>'
					.($auction->get('delivery_date_customer')!='0000-00-00'
					?($auction->get('delivery_date_customer')<date("Y-m-d")
						?'<font color="red">Shipping date: '.$auction->get('delivery_date_customer')."</font>"
						:'Shipping date: '.$auction->get('delivery_date_customer')).'<br>'
					:'')
					.(strlen($auction->get('company_shipping'))?$auction->get('company_shipping').'<br>':'')
					.$english[$auction->get('gender_shipping')].' '.$auction->get('firstname_shipping').' '.$auction->get('name_shipping').'<br>'
					.$address['address'].'<br>'
					.'Tel fix '.$auction->get('tel_shipping').'<br>'
					.'Tel mobile '.$auction->get('cel_shipping').'<br>'
					;
				$address['title'] = str_replace('"','\"',$address['title']);	
//				print_r($address); 
				$addresses[] = $address;
			}	
		    $smarty->assign('addresses', $addresses);
			$smarty->display('gmap.tpl');
		    exit;
        } elseif ($_POST['what']=='alarm') {
//			print_r($_POST[$action.'_group']); die($action.'_group');
		    foreach((array)$_POST[$action.'_group'] as $number) {
				$qrystr = "update alarms set `status`='Closed' where `id`=$number";
				$res = $db->query($qrystr);
			}
		    header('Location: ' . $_POST['return']);
		    exit;
		} else {
		    foreach((array)$_POST[$action.'_group'] as $number => $dummy) {
				list($number, $txnid) = explode('_', $dummy);
				if (strlen($txnid)) {
			        $a = new Auction($db, $dbr, $number, $txnid);
					if (isset($_POST['email'])) {
						if (!isset($_POST['address']['invoice'])) {
							$a->data->email_invoice = '';
						}
						if (!isset($_POST['address']['shipping'])) {
							$a->data->email_shipping = '';
						}
						$a->data->text = (isset($_POST['emailsubject'])?$_POST['emailsubject']."\n":'').$_POST['emailtext'];
						if ($_FILES['emailattachment']['name']) {
							$a->attachments = array();
							$rec = new stdClass;
							$rec->data = (file_get_contents($_FILES['emailattachment']['tmp_name']));
							$rec->name = $_FILES['emailattachment']['name'];
							$a->attachments[] = $rec;
						}
					    $res = standardEmail($db, $dbr, $a, 'mass_email');
					} elseif ($action=='Solve') {
						if (isset($_POST['setactive'])) {
							$a->set('solved', 0); 
							$a->set('relist_failed', 0); 
						}	
						if (isset($_POST['setinactive'])) {
							$a->set('solved', 1); 
						}	
						$a->update();
					} else {
					    $a->setDeleted();
					}	
				} else {
					$a = new Listing($db, $dbr, $number);
					if ($action=='Solve') $a->set('solved', 1);
						else $a->set('finished', 1);
					$a->update();
				} // delete listing
		    } 
		    header('Location: ' . $_POST['return']);
		    exit;
	    } // foreach record
	} // mass actions


		if ($_GET['checkout']==3) {
								$data = $_GET['DATA'];
								$SIGNATURE = $_GET['SIGNATURE'];
								$url = "https://www.saferpay.com/hosting/VerifyPayConfirm.asp?DATA=".urlencode($data)."&SIGNATURE=".urlencode($SIGNATURE);
								$saferpay_result = file_get_contents($url);
		  						if (substr($saferpay_result, 0, 3) == "OK:") {
									require_once 'XML/Unserializer.php';
			    					$options = array('parseAttributes'  => true);
									$_us = new XML_Unserializer( $options );
									$r = $_us->unserialize($data);
									$data = $_us->getUnserializedData();
//									$orderid = unserialize(base64_decode($data['ORDERID']));
									$token = $data['ORDERID'];
									$orderid = $dbr->getOne("select orderid from _tmp_pp_token where token='$token'");
									$orderid = unserialize(base64_decode($orderid));
									$number = $orderid['number'];
									$txnid = $orderid['txnid'];
									// add_payment
									$q = "insert into payment_saferpay set
									       `auction_number`='".$number."',             
									       `txnid`='".$txnid."',             
									       `txn_id`='".$data['ID']."',                      
		                  					`authorisation_date`=NOW(),                           
		                  					`payment_status`='reserved',                           
		                  					`amount`=".($data['AMOUNT']/100).",                           
		                  					`currency`='".$data['CURRENCY']."'
										";
									$r = $db->query($q);
									if (PEAR::isError($r)) {
										$str = print_r($r, true);
										file_put_contents('dberror', $str);
									}
								}	
	   	    header('Location: auction.php?number='.$number.'&txnid='.$txnid);
    	    exit;
		}

if ($debug) echo '1: '.round((getmicrotime()-$time),3).'<br>';if ($debug) $time = getmicrotime();

	$number = requestVar('number', '');
	$txnid = requestVar('txnid', '');
if ($debug) echo "get $number/$txnid<br>";
$auction = new Auction($db, $dbr, $number, $txnid); //////////// 0.5
if ($debug) echo "get {$auction->data->number}/{$auction->data->txnid}<br>";
if (is_a($auction->_error,'PEAR_Error') || strlen($auction->_error)) {
    $smarty->assign('errortext', $auction->_error->getMessage());
	if ($debug) print_r($auction);
    $smarty->assign('nodisplay', 1);
} else {
	$get_sql = 'SELECT id, auction_number, exchange_auction_id, route_id FROM auction WHERE exchange_auction_id=' . $auction->get('id');
	$driver_tasks = $dbr->getAll($get_sql);
	$smarty->assign('driver_tasks', $driver_tasks);

	$invoice = new Invoice($db, $dbr, $auction->get('invoice_number'));
    $offer = new Offer($db, $dbr, $auction->get('offer_id'));
	$offers = $dbr->getAll("select offer.*, auction.saved_id
		from offer
		join auction on auction.offer_id=offer.offer_id
		where (auction_number=".$auction->get('auction_number')." and txnid=".$auction->get('txnid').")
		or (main_auction_number=".$auction->get('auction_number')." and main_txnid=".$auction->get('txnid').")");
    $smarty->assign('offers', $offers);
}
	$comment_notif = $dbr->getOne("select count(*) from comment_notif where obj='auction' and obj_id=".$auction->get('id')." and username='".$loggedUser->get('username')."'");
	$smarty->assign('comment_notif', $comment_notif);

	if ($auction->get('lang')) {
		switch ($auction->get('lang')) {
			case 'polish':
				setlocale(LC_TIME, 'pl_PL');
				break;
			case 'german':
				setlocale(LC_TIME, 'de_DE');
				break;
			case 'english':
				setlocale(LC_TIME, 'en_EN');
				break;
			case 'french':
				setlocale(LC_TIME, 'fr_FR');
				break;
			case 'spanish':
				setlocale(LC_TIME, 'es_ES');
				break;
			case 'Hungarian':
				setlocale(LC_TIME, 'hu_HU');
				break;
		}
	} else {
		if ($auction->get('siteid')==3)
			setlocale(LC_TIME, 'en_EN');
		elseif ($auction->get('siteid')==77)
			setlocale(LC_TIME, 'de_DE');
	}

    if ($auction->get('main_auction_number')) {
		$main_auction = new Auction($db, $dbr, $auction->get('main_auction_number'), $auction->get('main_txnid'));
		$smarty->assign('main_auction', $main_auction);
	} else {
if ($debug) echo "2 get ".$auction->get('auction_number')."/".$auction->get('txnid')."<br>";
		$main_auction = new Auction($db, $dbr, $auction->get('auction_number'), $auction->get('txnid'));
		$smarty->assign('main_auction', $auction);
	}

	if (isset($_GET['new_article_pdf'])) {
		header ("Content-type: application/pdf; name=newarticles.pdf");
		header("Content-disposition: inline; filename=newarticles.pdf");
    	$allorder = Order::listAll($db, $dbr, $auction->get('auction_number'), $auction->get('txnid'), 1
			, $auction->getMyLang(), '0,1',1); 
			
        $pdf = &File_PDF::factory('L', 'mm', 'A4');
		$y_max = 200;
        $pdf->open();
        $pdf->setAutoPageBreak(false);
	    $pdf->addPage();
        $pdf->setFillColor('rgb', 0, 0, 0);
        $pdf->setDrawColor('rgb', 0, 0, 0);
        $pdf->setFont('arial','B', 14);
		$pdf->setLeftMargin(200);
		$pdf->setTopMargin(200);
        $pdf->setFont('arial','B', 10);
		$y = 15; $x = 5;
		$dx = 70; $pdf->rect($x, $y-3, $dx, 4); $pdf->text($x+2, $y, 'Article # that was taken'); $x+=$dx;
		$dx = 20; $pdf->rect($x, $y-3, $dx, 4); $pdf->text($x+2, $y, 'Quantity'); $x+=$dx;
		$dx = 70; $pdf->rect($x, $y-3, $dx, 4); $pdf->text($x+2, $y, 'From what article# it was taken'); $x+=$dx;
		$dx = 20; $pdf->rect($x, $y-3, $dx, 4) ;$pdf->text($x+2, $y, 'Reserved'); $x+=$dx;
		$dx = 40; $pdf->rect($x, $y-3, $dx, 4); $pdf->text($x+2, $y, 'For what Auftrag'); $x+=$dx;
		$dx = 70; $pdf->rect($x, $y-3, $dx, 4); $pdf->text($x+2, $y, 'Responsbible user'); $x+=$dx;
		$y+=3;
		foreach ($allorder as $order){
			if (!$order->new_article) continue;
			$x = 5; $new_y=0;
			$dx = 70; $pdf->setXY($x, $y); $pdf->multiCell($dx, 3, $order->custom_title_id, 0, 'L');  $x+=$dx; $new_y = max($new_y, $pdf->getY());
			$dx = 20; $pdf->setXY($x, $y); $pdf->multiCell($dx, 3, $order->new_article_qnt, 0, 'L');  $x+=$dx; $new_y = max($new_y, $pdf->getY());
			$dx = 70; $pdf->setXY($x, $y); $pdf->multiCell($dx, 3, $order->new_article_id_name, 0, 'L');  $x+=$dx; $new_y = max($new_y, $pdf->getY());
			$dx = 20; $pdf->setXY($x, $y); $pdf->multiCell($dx, 3, $order->new_article_not_deduct?'No':'Yes', 0, 'L');  $x+=$dx; $new_y = max($new_y, $pdf->getY());
			$dx = 40; $pdf->setXY($x, $y); $pdf->multiCell($dx, 3, $order->auction_number.'/'.$order->txnid, 0, 'L');  $x+=$dx; $new_y = max($new_y, $pdf->getY());
			$dx = 70; $pdf->setXY($x, $y); $pdf->multiCell($dx, 3, $order->new_article_story, 0, 'L');  $new_y = max($new_y, $pdf->getY());
			$y = $new_y;
		}
	    $pdf->close();
	    echo $pdf->getOutput();
	}

if ($debug) echo '2: '.round((getmicrotime()-$time),3).'<br>';if ($debug) $time = getmicrotime();

$error = requestVar('error', '');
if (strlen($error))
	$smarty->assign('errortext', $error);

$au_type = $dbr->getOne("select fget_AType($number, $txnid)");
$info = SellerInfo::Singleton($db, $dbr, $auction->get('username'));

		if ($_GET['checkout']==4) {
							$info->getPayPal($API_UserName, $API_Password, $API_Signature);
								// Set request-specific fields.
								$token = urlencode(htmlspecialchars($_REQUEST['token']));
								// Add request-specific fields to the request string.
								$nvpStr = "&TOKEN=$token";
								
								// Execute the API operation; see the PPHttpPost function above.
								$httpParsedResponseAr = PPHttpPost('GetExpressCheckoutDetails', $nvpStr);
								
								if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"])) {
									$orderid = $dbr->getOne("select orderid from _tmp_pp_token where token='$token'");
									$orderid = unserialize(base64_decode($orderid));
									$_COOKIE['shop_cart'] = $orderid['shop_cart'];
									$personal = $orderid['personal'];
									$payment = $orderid['payment'];
									$id = $orderid['id'];
									$when = $orderid['when'];
									$shipon_date = $orderid['shipon_date'];
									$bonus = $orderid['bonus'];
									$promo_code = $orderid['promo_code'];
									$AMOUNT = $orderid['AMOUNT'];
									// Extract the response details.
									$payerID = $httpParsedResponseAr['PAYERID'];
									$payerEMAIL = $httpParsedResponseAr['EMAIL'];;
									$street1 = $httpParsedResponseAr["SHIPTOSTREET"];
									if(array_key_exists("SHIPTOSTREET2", $httpParsedResponseAr)) {
										$street2 = $httpParsedResponseAr["SHIPTOSTREET2"];
									}
									$city_name = $httpParsedResponseAr["SHIPTOCITY"];
									$state_province = $httpParsedResponseAr["SHIPTOSTATE"];
									$postal_code = $httpParsedResponseAr["SHIPTOZIP"];
									$country_code = $httpParsedResponseAr["SHIPTOCOUNTRYCODE"];
								} else  {
									exit('GetExpressCheckoutDetails failed: ' . print_r($httpParsedResponseAr, true));
								}

								$paymentType = urlencode("Sale");			// or 'Sale' or 'Order'
								$paymentAmount = urlencode($AMOUNT);
								$currencyID = urlencode(siteToSymbol($auction->data->siteid));	
								
								// Add request-specific fields to the request string.
								$nvpStr = "&TOKEN=$token&PAYERID=$payerID&PAYMENTACTION=$paymentType&AMT=$paymentAmount&CURRENCYCODE=$currencyID";
								
								// Execute the API operation; see the PPHttpPost function above.
								$httpParsedResponseAr = PPHttpPost('DoExpressCheckoutPayment', $nvpStr);
								
								if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"])) {
									$q = "insert into payment_paypal set
									       `item_number`='".$auction->data->auction_number."',             
									       `txn_id`='".$httpParsedResponseAr["TRANSACTIONID"]."',                      
										   `txn_type`='".$httpParsedResponseAr["TRANSACTIONTYPE"]."',
										   `payment_status`='".$httpParsedResponseAr["PAYMENTSTATUS"]."',
										   `payment_date`='".urldecode($httpParsedResponseAr["ORDERTIME"])."',
										   `mc_gross`='".urldecode($httpParsedResponseAr["AMT"])."',
										   `mc_fee`='".urldecode($httpParsedResponseAr["FEEAMT"])."',
										   `mc_currency`='".$httpParsedResponseAr["CURRENCYCODE"]."',
										   `auction_closing_date`='".urldecode($httpParsedResponseAr["ORDERTIME"])."',
										   `auction_buyer_id`='$payerID',
										   `auction_txnid`='".$auction->data->txnid."',
										   `receiver_email`='".$info->get('paypal_email')."',
										   `payer_email`='$payerEMAIL', 
										   `verification_status`='VERIFIED', 
										   `comment`='verification ok'";
									$r = $db->query($q);
								} else  {
									exit('DoExpressCheckoutPayment failed: ' . print_r($httpParsedResponseAr, true));
								}
	   	    header('Location: auction.php?number='.$number.'&txnid='.$txnid);
    	    exit;
		}

	if ($auction->get('lang'))
		$deflang = $auction->get('lang');
	elseif ($auction->get('customer_id'))
		$deflang = $dbr->getOne("select lang from customer".$au_type." where id='".$auction->get('customer_id')."'");
	else	
		$deflang = $dbr->getOne("select lang from customer".$au_type." where email='".$auction->get('email')."'");
if (!strlen($deflang)) $deflang = $info->data->default_lang;
//$lang = $deflang;
$info = new SellerInfo($db, $dbr, $auction->get('username'), $deflang);

$english = Auction::getTranslation($db, $dbr, $auction->get('siteid'), $deflang);
$smarty->assign('english', $english);
$english_shop = Auction::getTranslationShop($db, $dbr, $auction->get('siteid'), $deflang);
$smarty->assign('english_shop', $english_shop);

    $orderBonus = Order::listBonus($db, $dbr, $number, $txnid);
	$smarty->assign('orderBonus',$orderBonus);

if (isset($_POST['spec_order'])) {
	foreach($_POST['spec_order'] as $id=>$spec_order) {
			$spec_order_id = (int)$_POST['spec_order_id'][$id];
			$spec_order_container_id = (int)$_POST['spec_order_container_id'][$id];
			$spec_order_id = (int)$spec_order_id;
			if ($spec_order && $spec_order_id && !$dbr->getOne("select count(*)
				from op_article 
				where op_order_id=$spec_order_id
				and article_id=(select article_id from orders where id=$id)")
				) {
//				$error = "Article must be in OPS";
			} 
			$new_article = (int)$_POST['new_article'][$id];
			$lost_new_article = (int)$_POST['lost_new_article'][$id];
			$new_article_not_deduct = (int)$_POST['new_article_not_deduct'][$id];
			$new_article_completed = (int)$_POST['new_article_completed'][$id];
			if (isset($_POST['new_article_id'][$id])) {
				list($new_article_id, $new_article_warehouse_id) = explode(':', $_POST['new_article_id'][$id]);
				$new_article_warehouse_id = (int)$new_article_warehouse_id;
				$new_article_qnt = (int)$_POST['new_article_qnt'][$id];
				if (!$new_article_qnt) $new_article_qnt=1;
				$new_article_id = "'".mysql_escape_string($new_article_id)."'";
			} else {
				$new_article_warehouse_id = 'new_article_warehouse_id';
				$new_article_qnt = 'new_article_qnt';
				$new_article_id = 'new_article_id';
			}
			$spec_order_comment = mysql_escape_string($_POST['spec_order_comment'][$id]);
			if (isset($_POST['uncomplete_article_order_id'][$id])) {
				$uncomplete_article_order_id = (int)($_POST['uncomplete_article_order_id'][$id]);
				if ((int)$dbr->getOne("select wwa.wwo_id from wwo_article wwa 
					where wwa.uncomplete_article_order_id=$uncomplete_article_order_id and not wwa.delivered"))
					$uncomplete_article_order_id = 0;
			} else $uncomplete_article_order_id = 'uncomplete_article_order_id';
			$q="update orders set spec_order_id=$spec_order_id 
				, spec_order=$spec_order
				, spec_order_container_id=$spec_order_container_id
				, spec_order_comment='$spec_order_comment'
				, new_article=$new_article
				, lost_new_article=$lost_new_article
				, new_article_not_deduct=$new_article_not_deduct
				, new_article_qnt=$new_article_qnt
				, new_article_warehouse_id=$new_article_warehouse_id
				, new_article_completed=$new_article_completed
				, new_article_id=$new_article_id
				, uncomplete_article_order_id = $uncomplete_article_order_id
				where id=$id";
			$r = $db->query($q);
   			if (PEAR::isError($r)) { aprint_r($r); die();}
	}
	foreach($_POST['wwo_order_id'] as $id=>$wwo_order_id) {
			$wwo_order_id = (int)$wwo_order_id;
/*			$q = "select wwa.id
				from ww_order wwo
				join wwo_article wwa on wwa.wwo_id=wwo.id
				where wwa.id=$wwo_order_id
				and wwo.closed=0 
				and wwa.qnt>=(select sum(quantity) from orders
					where orders.id<>$id and orders.wwo_order_id=wwa.id 
					and orders.article_id=wwa.article_id)
					+(select quantity from orders where orders.id=$id)
				";
			$checked_id = $dbr->getOne($q);
			if ($checked_id==$wwo_order_id)*/ {
				$db->query("update orders set wwo_order_id=$wwo_order_id 
					where id=$id");
			}
	}
    header('Location: auction.php?number='.$auction->get('auction_number') . '&txnid=' . $auction->get('txnid')
		.(strlen($error)?"&error=$error":"")
		."#invoice");
    exit;
}

if ($debug) echo '3: '.round((getmicrotime()-$time),3).'<br>';if ($debug) $time = getmicrotime();

if (isset($_GET['freeze'])) {
	$q = "update auction set no_emails=".(int)$_GET['freeze']."
		, no_emails_date='".serverToLocal(date('Y-m-d H:i:s'), $timediff)."'
		";
    if ((int)$_GET['freeze']) {
        $q .= ", freeze_date='".serverToLocal(date('Y-m-d H:i:s'), $timediff)."'
		";
    }
	if ($auction->get('main_auction_number')) {
		$db->query($q." where main_auction_number=".$auction->get('main_auction_number')." and main_txnid=".$auction->get('main_txnid'));
		$db->query($q." where auction_number=".$auction->get('main_auction_number')." and txnid=".$auction->get('main_txnid'));
	}
	if ($auction->get('auction_number')) {
		$db->query($q." where main_auction_number=".$auction->get('auction_number')." and main_txnid=".$auction->get('txnid'));
	}
    $auction->set('no_emails', (int)$_GET['freeze']);
    $auction->data->no_emails_by = $loggedUser->get('username');
//    echo $loggedUser->get('username');
    $auction->set('no_emails_date', serverToLocal(date('Y-m-d H:i:s'), $timediff));
    if ((int)$_GET['freeze']) {
        $auction->set('freeze_date', serverToLocal(date('Y-m-d H:i:s'), $timediff));
    }
    $auction->update();
    header('Location: ' . $_GET['return']);
    exit;
}

if ('POST' == $_SERVER['REQUEST_METHOD']) {
	if ($_POST['delete']) {
	    if (!$auction->get('deleted') && strlen(mysql_escape_string($_POST['newcomment_del']))) 
			$auction->addComment(($_POST['newcomment_del'])
		  , $loggedUser->get('username')
		  , serverToLocal(date('Y-m-d H:i:s'), $timediff), $_POST['newcomment_src']);
	    $auction->setDeleted();
	}
	if (isset($_POST['update']) && $_POST['update']=='deldoc') {
	    $auction->delDoc($_POST['deleted_doc']);
	}
	if (isset($_POST['update']) && $_POST['update']=='delcomm') {
	    $auction->delComment($_POST['deleted_obj']);
	}
	if (isset($_POST['email'])) {
	    $auction->set('email', mysql_escape_string($_POST['email']));
	}
	if (isset($_POST['winning_bid'])) {
	    $auction->set('winning_bid', mysql_escape_string($_POST['winning_bid']));
	    $auction->set('siteid', mysql_escape_string($_POST['siteid']));
	    $auction->set('quantity', mysql_escape_string($_POST['quantity']));
	}
	
	if ($_POST['emp_invoice']){
	    $auction->set('emp_id', (int)($_POST['emp_id']));
	}
	if (isset($_POST['scan'])) {
		if ($txnid>=1) scan_auction($db, $dbr, $number, $txnid, $info);
	  	$auction = new Auction($db, $dbr, $number, $txnid);
	}
	if (isset($_POST['change_offer']) && (int)$_POST['newoffer_id']) {
	    $auction->set('offer_id', (int)$_POST['newoffer_id']);
	}
	if (isset($_POST['change_source'])) {
	    $auction->set('source_seller_id', (int)$_POST['source_seller_id']);
		$db->query("update auction set 
			dontsend_marked_as_Shipped=
			(select dontsend_marked_as_Shipped from payment_method where code=auction.payment_method)
			*
			(select dontsend_marked_as_Shipped from source_seller where id=auction.source_seller_id)
			where auction_number=".$auction->get('auction_number')." and txnid=".$auction->get('txnid'));
	}
	if (isset($_POST['change_number'])) {
	    $auction->set('ff_number', $_POST['ff_number']);
	}
	if (isset($_POST['lang'])) {
	    $auction->set('lang', $_POST['lang']);
	}
	if (isset($_POST['set_rating'])) {
		$result = set_rating($db, $dbr, $number, $txnid, $info);
	}
	if (isset($_POST['get_rating'])) {
		$result = get_rating($db, $dbr, $number, $txnid, $info);
	}
	if (isset($_POST['shipping_username'])) {
		$newuser = $_POST['shipping_username'];
		$auction->set('shipping_username', $newuser);
		if ($newuser!=$main_auction->get('shipping_username')) {
			$db->query("update auction set route_id=null where auction_number="
					.$main_auction->get('main_auction_number').' and txnid='.$main_auction->get('main_txnid'));
			$db->query("update auction set route_id=null where auction_number="
					.$main_auction->get('auction_number').' and txnid='.$main_auction->get('txnid'));
		}
		if (strlen($newuser)) {
			$db->query("update auction set shipping_username='$newuser' where auction_number="
					.$main_auction->get('main_auction_number').' and txnid='.$main_auction->get('main_txnid'));
			$db->query("update auction set shipping_username='$newuser' where auction_number="
					.$main_auction->get('auction_number').' and txnid='.$main_auction->get('txnid'));
		    $ret = new stdClass;
			$ret->from = $loggedUser->get('email');
			$ret->from_name = $loggedUser->get('name');
			$user = new User($db, $dbr, $newuser);
			if ($user->get('get_shipping_orders') && ($_POST['reassign']=='Reassign')) {
				$ret->email_invoice = $user->get('email');
				$ret->auction_number = $main_auction->get('auction_number');
				$ret->txnid = $main_auction->get('txnid');
				if (strlen($ret->email_invoice)) {
					if (!standardEmail($db, $dbr, $ret, 'new_shipping_order'))
		      		      echo 'Error mail sending!'; 
				}
			}
			$reserve_warehouse_id = $dbr->getRow("SELECT default_reserve_warehouse_id, default_available_reserve_warehouse_id 
					FROM users	WHERE username = '".$user->get('username')."'");
			if (!(int)$reserve_warehouse_id->default_available_reserve_warehouse_id) 
				$reserve_warehouse_id->default_available_reserve_warehouse_id = $reserve_warehouse_id->default_reserve_warehouse_id;
			if ((int)$reserve_warehouse_id->default_reserve_warehouse_id) {
				$aus = $dbr->getAll("select o.id, IF(fget_Article_stock(o.article_id, {$reserve_warehouse_id->default_available_reserve_warehouse_id})>=o.quantity,{$reserve_warehouse_id->default_available_reserve_warehouse_id},{$reserve_warehouse_id->default_reserve_warehouse_id}) reserve_warehouse_id
					from orders o
					left join auction au on au.auction_number=o.auction_number and au.txnid=o.txnid
					left join auction mau on mau.auction_number=au.main_auction_number and mau.txnid=au.main_txnid
					where (au.auction_number=".$auction->get('auction_number')." and au.txnid=".$auction->get('txnid').") and o.manual=0 and not o.sent
					union
					select o.id, IF(fget_Article_stock(o.article_id, {$reserve_warehouse_id->default_available_reserve_warehouse_id})>=o.quantity,{$reserve_warehouse_id->default_available_reserve_warehouse_id},{$reserve_warehouse_id->default_reserve_warehouse_id}) reserve_warehouse_id
					from orders o
					left join auction au on au.auction_number=o.auction_number and au.txnid=o.txnid
					left join auction mau on mau.auction_number=au.main_auction_number and mau.txnid=au.main_txnid
					where (mau.auction_number=".$auction->get('auction_number')." and mau.txnid=".$auction->get('txnid').") and o.manual=0 and not o.sent
					");
		    	if (PEAR::isError($aus)) { aprint_r($aus); die();}
				foreach($aus as $au) {
					$r = $db->query("update orders set reserve_warehouse_id=$au->reserve_warehouse_id 
						where id=$au->id");
		    		if (PEAR::isError($r)) { aprint_r($r); die();}
				}
			} // if def reserved ware is set
		} else {
			$db->query("update auction set shipping_username=NULL where auction_number="
					.$main_auction->get('main_auction_number').' and txnid='.$main_auction->get('main_txnid'));
			$db->query("update auction set shipping_username=NULL where auction_number="
					.$main_auction->get('auction_number').' and txnid='.$main_auction->get('txnid'));
		}		  
	}
	if (isset($_POST['shipping_resp_username'])) {
		$newuser = $_POST['shipping_resp_username'];
		if (strlen($newuser)) {
			$db->query("update auction set shipping_resp_username='$newuser' where auction_number="
				.$auction->get('auction_number').' and txnid='.$auction->get('txnid'));
			$db->query("update auction set shipping_resp_username='$newuser' where auction_number="
				.$main_auction->get('auction_number').' and txnid='.$main_auction->get('txnid'));
		    $ret = new stdClass;
			$ret->from = $loggedUser->get('email');
			$ret->from_name = $loggedUser->get('name');
			$user = new User($db, $dbr, $newuser);
			$ret->email_invoice = $user->get('email');
			$ret->auction_number = $main_auction->get('auction_number');
			$ret->txnid = $main_auction->get('txnid');
			if (!standardEmail($db, $dbr, $ret, 'new_responsible_shipping_order'))
	      	      echo 'Error mail sending!'; 
		}		  
	}
	if (isset($_POST['complain_submit'])) {
		if ($_POST['complain_submit']=='Clear complain') {
			$auction->set('complained', 0);
		} else {	
			$auction->set('complained', 1);
		};	
		$auction->set('complain_set_date', ServerTolocal(date("Y-m-d H:i:s"), $timediff));
	    $auction->set('complain_set_by', $loggedUser->get('username'));
	    $auction->update();
	}				
	if (isset($_POST['ticket_voucher_email_send']) 
				&& ($template_id = (int)$info->get('rma_shop_promo_template_id'))
				&& ($id = (int)$info->get('rma_shop_promo_code_id'))
	//					&& ($days_valid = (int)$info->get('rma_shop_promo_days_valid'))
				&& ($days_valid = (int)$_POST['voucher_days'])
				&& ($voucher_amount = (1*$_POST['voucher_amount']))
		) {
		$code = generate_password();
		while ($dbr->getOne("select count(*) from shop_promo_codes where code='$code'")) $code = generate_password();
		$codes[] = $code;
		$result = Shop_Catalogue::duplicateVoucher($db, $dbr, $id, $codes, array(
					'date_from'=>'NOW()',
					'date_to'=>"date_add(now(),INTERVAL $days_valid day)",
				)
				, $voucher_amount);
		extract($result);
/*		$name = $dbr->getOne("select name from shop_promo_codes where id=$id");
			$article_id = $dbr->getOne('SELECT MAX( CAST( article_id AS SIGNED ) )+1 FROM article where admin_id=3');
			$q = "insert into article set name='$name'
				,article_id='$article_id', admin_id=3
				";
			$r = $db->query($q);
			if (PEAR::isError($r)) { aprint_r($r); $codes_failed[] = $code; continue;}
			$q = "insert into shop_promo_codes (`name`,date_from,date_to,`usage`,shop_id,`code`,min_amount,days_available,`type`,days_send,days_remind,article_id,usage_customer,inactive,notforbonus,notforshipping,sold_for_amount,copied_from_id,template_id,security_code,security
					,is_affili,is_requested,marked_for_payment,security_code_min,security_code_max,basegroup,descr_is_name,landing_page,dont_take_sold_for_amount,free_shipping) 
				select `name`, NOW(), date_add(now(),INTERVAL $days_valid day),`usage`,shop_id,'$code',min_amount,days_available,`type`,days_send,days_remind,'$article_id' as article_id,usage_customer,inactive,notforbonus,notforshipping,sold_for_amount, $id,template_id,security_code,security
					,is_affili,is_requested,marked_for_payment,security_code_min,security_code_max,basegroup,descr_is_name,landing_page,dont_take_sold_for_amount,free_shipping
				from shop_promo_codes where id=$id";
			$r = $db->query($q);
			if (PEAR::isError($r)) { aprint_r($r); $codes_failed[] = $code; continue;}
			$new_id = mysql_insert_id();
			$q = "insert into shop_promo_values (code_id,percent,amount,`type`,`usage`) 
				select $new_id,percent,$voucher_amount,`type`,`usage` from shop_promo_values where code_id=$id";
			$r = $db->query($q);
			if (PEAR::isError($r)) { aprint_r($r); $codes_failed[] = $code; continue;}
			$q = "insert into translation (`language`,table_name,field_name,id,`value`,unchecked,updated) 
				select `language`,table_name,field_name,$new_id,`value`,unchecked,updated from translation
					where table_name='shop_promo_codes' and id=$id";
			$r = $db->query($q);
			if (PEAR::isError($r)) { aprint_r($r); $codes_failed[] = $code; continue;}
			$q = "insert into shop_promo_condition_promo (code_id,condition_id) 
				select $new_id,condition_id from shop_promo_condition_promo where code_id=$id";
			$r = $db->query($q);
			if (PEAR::isError($r)) { aprint_r($r); $codes_failed[] = $code; continue;}
			$q = "insert into shop_promo_sa (code_id,saved_id,quantity) 
				select $new_id,saved_id,quantity from shop_promo_sa where code_id=$id";
			$r = $db->query($q);
			if (PEAR::isError($r)) { aprint_r($r); $codes_failed[] = $code; continue;}
			$q = "insert into shop_promo_articles (code_id,article_id,quantity,`type`,`usage`,old_price,new_price) 
				select $new_id,article_id,quantity,`type`,`usage`,old_price,new_price from shop_promo_articles where code_id=$id";
			$r = $db->query($q);
			if (PEAR::isError($r)) { aprint_r($r); $codes_failed[] = $code; continue;}
			$codes_ok[] = $code;*/
		$q = "select shop_promo_codes.*, shop_promo_values.percent, shop_promo_values.amount
			, (select count(*) from auction where deleted=0 and code_id and code_id=shop_promo_codes.id) used
			, shop.name shop
					, (select CONCAT('Was ',IF(tl.new_value=1, 'added', 'removed'),' by ', IFNULL(u.name, tl.username)
						, ' on ', tl.updated)
						from total_log tl
						left join users u on u.system_username=tl.username
						where table_name='shop_promo_codes' and field_name='marked_for_payment' and tableid=shop_promo_codes.id
						order by tl.updated desc limit 1
						) marked_for_payment_log
			from shop_promo_codes
			join shop_promo_values on shop_promo_codes.id = shop_promo_values.code_id
			join shop on shop.id = shop_promo_codes.shop_id
			where shop_promo_codes.id=$new_id";
		$cat = $dbr->getRow($q);
		$template = $dbr->getOne("select t.value from translation t
			where t.language='".$auction->getMyLang()."' and t.id={$template_id}
			and t.table_name='shop_promo_template' and field_name='html'");
		$cat->voucher_code = $cat->code;
		$cat->voucher_id = $cat->id;
		$cat->voucher_amount = $cat->amount;
		$cat->currency = siteToSymbol($auction->data->siteid);
		$conditions = $dbr->getAssoc("select shop_promo_condition.id, 
			(select value from translation where table_name='shop_promo_condition' and field_name='cond_text' and id=shop_promo_condition.id and language='".$auction->getMyLang()."') translated_text
			from shop_promo_condition
			join shop_promo_condition_promo on condition_id=shop_promo_condition.id and code_id=$new_id
			order by shop_promo_condition.ordering
				");
		$cat->conditions = implode(' - ',$conditions);
		$sas = $dbr->getAssoc("select saved_id, 
			IFNULL(translated_text, 
				(select value from translation where table_name='translate' and field_name='translate' 
				and id=22 and language='".$auction->getMyLang()."')
			) translated_text
			from (
			select shop_promo_sa.saved_id, 
			(select offer_name.name
			from translation 
			join offer_name on translation.value=offer_name.id
			where translation.table_name='sa' 
			and translation.field_name='ShopDesription' 
			and translation.id=shop_promo_sa.saved_id and translation.language='".$auction->getMyLang()."') translated_text
			from shop_promo_sa
			where code_id=$new_id
			) t
				");
		$cat->sas = implode(', <br/>',$sas);
		$template = substitute($template,$cat);
		if ($auction->getMyLang()!='polish') {
			$template = utf8_decode($template);
		}
		require_once("dompdf/dompdf_config.inc.php");
		$dompdf = new DOMPDF();
		$dompdf->set_paper('A4','landscape');
		$dompdf->load_html(($template));
		$dompdf->render();
		$content = $dompdf->output();
		$rec = new stdClass;
		$rec->data = $content;
		$rec->name = 'voucher.pdf';
		$auction->attachments = array($rec);
		$auction->notes = $cat->id.'_'.$template_id;
		$auction->data->amount = $cat->amount;
		$auction->data->date_to = $cat->date_to;
		global $lang;
		$lang = $auction->getMyLang();
	      	$r = standardEmail($db, $dbr, $auction, 'ticket_voucher_email');
	}
	
	if (isset($_POST['rating_case'])) {
		if (isset($_POST['manualRating']) && strlen(trim($_POST['manualRating']))) {
			$text = mysql_escape_string(trim($_POST['manualRating']));
			if ($auction->get('rma_id')) {
			   $source = $dbr->getRow('select * 
			   	from auction join rma on auction.auction_number=rma.auction_number and auction.txnid=rma.txnid 
				   where rma.rma_id='.$auction->get('rma_id'));
				if (PEAR::isError($source)) aprint_r($source);
			   $number = $source->auction_number;
			   $txnid = $source->txnid;
			} else {
			   $number = $auction->data->auction_number;
			   $txnid = $auction->data->txnid;
			}   
			$code = (int)$_POST['rating'];
			$q = "insert into auction_feedback (
				`auction_number`,`txnid`,`type`,`id`,`datetime`,`code`,`text`)
				values ($number, $txnid, 'received', 0, 
					'".ServerTolocal(date("Y-m-d H:i:s"), $timediff)."',$code,'$text'
				)";
			$r = $db->query($q);
			if (PEAR::isError($r)) { /*aprint_r($r); die(); */}
			if (($code<=3 && $txnid==3) || (($code==3 || $code==2) && $txnid!=3)) {
				$rating = new Rating($db, $dbr, 0, $number, $txnid, $timediff);
				$rating->update();
							$ret = new stdClass;
							$ret->original_username = $auction->data->username;
							$ret->username = Config::get($db, $dbr, 'aatokenSeller');
							$ret->auction_number = $number;
							$ret->txnid = $txnid;
						    global $siteURL;
							$rating_id = $db->getOne("select max(id) from rating where auction_number=$number and txnid=$txnid");
							$ret->rating_case_url = $siteURL . "rating_case.php?id=" . $rating_id;
							$r = standardEmail($db, $dbr, $ret, 'rating_case');
			} // if we have bad rating
		} // if we have a manual rating
	}
    $auction->update();
}


if ($debug) echo '4: '.round((getmicrotime()-$time),3).'<br>';if ($debug) $time = getmicrotime();


$info = new SellerInfo($db, $dbr, $auction->get('username'), $deflang);

////$shipping_comments = substitute($info->get('shipping_comments'), $auction->data);
//$shipping_comments = substitute($info->getTemplate('shipping_comments', ''), $auction->data);
//$smarty->assign('shipping_comments', $shipping_comments);

if ($debug) echo '5: '.round((getmicrotime()-$time),3).'<br>';if ($debug) $time = getmicrotime();

if (isset($_GET['book'])) {
	if (strlen($_GET['ppp'])) { $tblname = 'payment_paypal'; $ppid = $_GET['ppp']; $acct = $info->get('paypal_account'); $date_fldname = 'payment_date'; $date_fldname1 = 'payment_date_MEZ';}
	if (strlen($_GET['psp'])) { 
			$tblname = 'payment_saferpay'; $ppid = $_GET['psp']; 
			$acct = $info->get($auction->data->payment_method=='ideal_shp'?'ideal_account':'cc_account'); 
			$date_fldname = 'authorisation_date'; $date_fldname1 = 'authorisation_date';
	}
	$amt_fldname = 'amount';
	if (strlen($_GET['bill'])) { $tblname = 'payment_saferpay'; $ppid = $_GET['bill']; $acct = $info->get('bill_account'); $date_fldname = 'authorisation_date'; $date_fldname1 = 'authorisation_date';}
	if (strlen($_GET['gcp'])) { $tblname = 'payment_google'; $ppid = $_GET['gcp']; $acct = $info->get('cc_account'); $date_fldname = 'payment_date'; $date_fldname1 = 'payment_date';}
	if (strlen($_GET['bsp'])) { $tblname = 'payment_bean'; $ppid = $_GET['bsp']; $acct = $info->get('cc_account'); $date_fldname = 'payment_date'; $date_fldname1 = 'payment_date';}
	if (strlen($_GET['p24'])) { $tblname = 'payment_p24'; $ppid = $_GET['p24']; $acct = $info->get('p24_account'); $date_fldname = 'payment_date'; $date_fldname1 = 'payment_date';}
	if (strlen($_GET['sfp'])) { $tblname = 'payment_sofort'; $ppid = $_GET['sfp']; $acct = $info->get('sofort_account'); $date_fldname = 'getTime'; $date_fldname1 = 'getTime'; $amt_fldname = 'getAmount';}
	if ((int)$_GET['book']) {
	    $res = $dbr->getRow("select *, DATE_ADD($date_fldname, INTERVAL 9 HOUR) payment_date_MEZ 
			from $tblname WHERE id='$ppid'");
		$date = $res->$date_fldname1;
	    $total = Payment::total($db, $dbr, $auction->get('auction_number'), $auction->get('txnid'));
	    $amount = isset($res->mc_gross)?$res->mc_gross:$res->$amt_fldname;
	    $due = $invoice->get('total_price') + $invoice->get('total_shipping') + $invoice->get('total_cod');
	    if ((($total + $amount) >= $due) 
					&& (int)$dbr->getOne("select send_payment_confirmation from accounts where number='$acct'")) {
	            $auction->set('paid', 1);
	            if ($auction->get('process_stage') < STAGE_PAID) {
	                $auction->set('process_stage', STAGE_PAID);
	            }
	            $auction->update();
				$template = $dbr->getOne("select email_template_thanks from payment_method where `code`='".$auction->get('payment_method')."'");
	            standardEmail($db, $dbr, $auction, $template);
	    }
	    $payment = new Payment($db, $dbr);
		$r = VAT::get_vat_attribs($db, $dbr, $auction);
	    if (!PEAR::isError($r)) {
	           $payment->set('vat_percent', (strlen($auction->get('customer_vat')) ? 0 : $r->vat_percent));
	           $payment->set('vat_account_number', (strlen($auction->get('customer_vat')) ? $r->out_vat : $r->vat_account_number));
	           $payment->set('selling_account_number', (strlen($auction->get('customer_vat')) ? $r->out_selling : $r->selling_account_number));
		};	
	    $payment->set('auction_number', $auction->get('auction_number'));
	    $payment->set('txnid', $auction->get('txnid'));
	    $payment->set('amount', $amount);
	    $payment->set('account', $acct);
	    $payment->set('payment_date', $date.' '.date("H:i:s"));
		$payment->set('listingfee', 0);
	    $payment->set('username', $loggedUser->get('username'));
	    $payment->set('comment', '');
	    $payment->update();
		$payment_id = mysql_insert_id();
		$invoice->recalcOpenAmount();
		$invoice = new Invoice($db, $dbr, $auction->get('invoice_number'));
		if (strlen($info->get('send_invoice_to_seller_email')) && $invoice->get('open_amount')<=0) {
			$auction->data->email_invoice = $info->get('send_invoice_to_seller_email');
			standardEmail($db, $dbr, $auction, 'send_invoice_to_seller_email');
		}
	    if ( $tblname == 'payment_sofort') {
			$r = $db->query("UPDATE $tblname SET booked_payment_id=$payment_id WHERE id='$ppid'");
		} else {
		    $r = $db->query("UPDATE $tblname SET booked_payment_id=$payment_id, booked=".$_GET['book']."
				, booked_date='".serverToLocal(date('Y-m-d H:i:s'), $timediff)."', booked_by='$username' WHERE id='$ppid'");
		}
		if (PEAR::isError($r)) aprint_r($r);
	    header('Location: auction.php?number='.$auction->get('auction_number') . '&txnid=' . $auction->get('txnid'));
	    exit;
	} // if book=1	
	else {
	    $r = $db->query("delete from payment WHERE payment_id=(
			select booked_payment_id from $tblname where id='$ppid')");
		if (PEAR::isError($r)) aprint_r($r);
	    if ( $tblname == 'payment_sofort') {
			$r = $db->query("UPDATE $tblname SET booked_payment_id=NULL WHERE id='$ppid'");
		} else {
		    $r = $db->query("UPDATE $tblname SET booked=".$_GET['book']."
				, booked_date='".serverToLocal(date('Y-m-d H:i:s'), $timediff)."', booked_by='$username' WHERE id='$ppid'");
		}
		if (PEAR::isError($r)) aprint_r($r);
	    header('Location: auction.php?number='.$auction->get('auction_number') . '&txnid=' . $auction->get('txnid'));
	    exit;
	}
}

if (isset($_GET['monitor'])) {
    $r = $db->query("UPDATE tracking_numbers SET monitored=".$_GET['monitor']."
		, monitored_date='".serverToLocal(date('Y-m-d H:i:s'), $timediff)."', monitored_by='$username' WHERE id=".$_GET['tnid']);
	if (PEAR::isError($r)) aprint_r($r);
    header('Location: auction.php?number='.$auction->get('auction_number') . '&txnid=' . $auction->get('txnid'));
    exit;
}

if ($delnum = requestVar('delnum','','GET')) {
	$tn_id = $dbr->getOne("select id 
		FROM tracking_numbers 
		WHERE auction_number=$number AND txnid =$txnid 
		AND date_time='".$_GET['d']."' AND number='".mysql_escape_string(urlencode($delnum))."'");
	if (PEAR::isError($tn_id)) { aprint_r($tn_id); die(); }
	if (!(int)$tn_id) {
		$tn_id = $dbr->getOne("select id 
			FROM tracking_numbers 
			WHERE auction_number=$number AND txnid =$txnid 
			AND date_time='".$_GET['d']."' AND number='".mysql_escape_string($delnum)."'");
		if (PEAR::isError($tn_id)) { aprint_r($tn_id); die(); }
	}
    $r = $db->query("DELETE FROM tn_orders WHERE tn_id=$tn_id");
	if (PEAR::isError($r)) { aprint_r($r); die(); }
    $r = $db->query("DELETE FROM tracking_numbers WHERE id=$tn_id");
	if (PEAR::isError($r)) { aprint_r($r); die(); }
    header('Location: auction.php?number='.$auction->get('auction_number') . '&txnid=' . $auction->get('txnid'));
    exit;
}

if ($labelnum = requestVar('labelnum','','GET')) {
	$auction->data->country_invoice = translate($db, $dbr, $auction->data->country_invoice, $auction->getMyLang());
	$auction->data->country_shipping = translate($db, $dbr, $auction->data->country_shipping, $auction->getMyLang());
	$smarty->assign('auction', $auction->data);
	$mid = $dbr->getOne('select shipping_method from tracking_numbers where number='.$labelnum.' LIMIT 0,1');
//	echo $mid;
    $method = new ShippingMethod($db, $dbr, $mid);
/*	echo $method->return_name.<br>
	echo $method->return_address1|escape}<br>
	echo $method->return_address2|escape}<br>
	echo $method->return_address3|escape}
*/
	$smarty->assign('method', $method->data);
    $smarty->assign('nomenu', true);
    $smarty->display('schenker.tpl');
    exit;
}

if ($delpay = requestVar('delpay','','GET')) {
    $db->execParam(
        "DELETE FROM payment WHERE auction_number=?  AND txnid=? AND payment_id=?",
        array($number, $txnid, $delpay)
    );
	$invoice = new Invoice($db, $dbr, $auction->get('invoice_number'));
	$invoice->recalcOpenAmount();
    header('Location: auction.php?number='.$auction->get('auction_number') . '&txnid=' . $auction->get('txnid'));
    exit;
}

if ($dellistingfee = requestVar('dellistingfee','','GET')) {
	$auction->DeleteListingFee($db, $dbr, $dellistingfee);
    header('Location: auction.php?number='.$auction->get('auction_number') . '&txnid=' . $auction->get('txnid'));
    exit;
}

if ($closelistingfee = requestVar('closelistingfee','','GET')) {
	$date = localToServer(date(date('Y-m-d'), time()), $timediff);
	$auction->CloseListingFee($db, $dbr, $closelistingfee, $date, $loggedUser->get('username'));
    header('Location: auction.php?number='.$auction->get('auction_number') . '&txnid=' . $auction->get('txnid'));
    exit;
}

if ($delrma = requestVar('delrma','','GET')) {
//	$auction = new Auction ($db, $dbr, $auction_number, $txnid);
    $rma = new Rma($db, $dbr, $auction, $delrma);
	if (!$rma->delete())
		$errmsg =  $rma->_error->getMessage(); 
    header('Location: auction.php?number='.$auction->get('auction_number') . '&txnid=' . $auction->get('txnid')
		.'&error='.(strlen($errmsg)?$errmsg:''));
    exit;
}

if (isset($_GET['delins'])) {
	$ins = new Insurance ($db, $dbr, $_GET['delins']);
	$ins->delete();
    header('Location: auction.php?number='.$auction->get('auction_number') . '&txnid=' . $auction->get('txnid'));
    exit;
}

if (isset($_GET['release'])) {
	Auction::releaseSubinvoice($db, $dbr, $_GET['rnumber'], $_GET['rtxnid']);
    header('Location: auction.php?number='.$auction->get('auction_number') . '&txnid=' . $auction->get('txnid'));
    exit;
}

if (isset($_REQUEST['restatic'])) {
    $invoice = new Invoice($db, $dbr, $auction->get('invoice_number'));
#			$invoice->make_static($db, $dbr, $auction->get('auction_number'), $auction->get('txnid'), $english);
			$invoice->make_static_HTML($db, $dbr, $auction->get('auction_number'), $auction->get('txnid'));
			$invoice->make_static_shipping_list_HTML($db, $dbr, $auction->get('auction_number'), $auction->get('txnid'));
#			$invoice->make_static_shipping_list($db, $dbr, $auction->get('auction_number'), $auction->get('txnid'), $english);
    header('Location: auction.php?number='.$auction->get('auction_number') . '&txnid=' . $auction->get('txnid'));
    exit;
}

if ($debug) echo '6: '.round((getmicrotime()-$time),3).'<br>';if ($debug) $time = getmicrotime();
$shop_id=(int)$dbr->getOne("select id from shop where '".$auction->get('server')."' in (url, concat('www.',url))");

if (requestVar('print_invoice','','POST') 
	|| requestVar('print_shipping_list','','POST') 
	|| requestVar('show_invoice','','POST') 
	|| requestVar('outside_invoice','','POST') 
	|| requestVar('produce_invoice','','POST') 
	|| requestVar('produce_outside_invoice','','POST') 
	|| requestVar('static_invoice','','POST')
	|| requestVar('static_shipping_list','','POST')
	) {
    $invoice = new Invoice($db, $dbr, $auction->get('invoice_number'));
	if (requestVar('static_invoice','','POST')) {
		if (strlen($invoice->get('static'))) {
			header ("Content-type: application/pdf");
			echo base64_decode($invoice->get('static'));
			exit;
		} else {
			echo 'No static invoice';
			exit;
		};	
	};
	if (requestVar('static_shipping_list','','POST')) {
		if (strlen($invoice->get('static_shipping_list'))) {
			header ("Content-type: application/pdf");
			echo base64_decode($invoice->get('static_shipping_list'));
			exit;
		} else {
			echo 'No static shipping list';
			exit;
		};	
	};
	if (requestVar('print_invoice','','POST') 
		|| requestVar('outside_invoice','','POST')
		|| requestVar('print_shipping_list','','POST')
		) {
	    if ($invoice->get('print_date') == '0000-00-00') {
	        $invoice->set('print_date', date('Y-m-d'));
	        $invoice->update();
	    }
        $auction->set('printed', 1);
        $auction->update();
		if (requestVar('print_shipping_list','','POST')) {
		    PrinterLog::Log($db, $dbr, $auction->get('auction_number'),  $auction->get('txnid'), $loggedUser->get('username'), 
				'Print shipping list');
		    $smarty->assign('print_shipping_list', 1);
		} else {
		    PrinterLog::Log($db, $dbr, $auction->get('auction_number'),  $auction->get('txnid'), $loggedUser->get('username'), 
				requestVar('outside_invoice','','POST') ? 'Print outside EU invoice' : 'Printed invoice');
		}		
	    $smarty->assign('print', 1);
    }
/*    $r = $dbr->getAssoc("SELECT CONCAT( auction_number, '/', txnid ), invoice_number
				FROM auction
				WHERE main_auction_number =".$auction->get('auction_number')."
				AND main_txnid =".$auction->get('txnid'));
	if (PEAR::isError($r)) aprint_r($r);
    $subinvoices = array_map('intval', (array)$r);
	if (count($subinvoices)) {
    	$subinvoices = implode(',', $subinvoices);
	    $r = $db->query("SELECT SUM(total_price) total_price,
			SUM(total_shipping) total_shipping,
			SUM(total_cod) total_cod
			FROM invoice WHERE invoice_number IN (".$subinvoices.")");
		$sub = $r->fetchRow();	
		$invoice->data->total_price += $sub->total_price;
		$invoice->data->total_shipping += $sub->total_shipping;
		$invoice->data->total_cod += $sub->total_cod;
	};*/

	if (requestVar('outside_invoice','','POST') || requestVar('produce_outside_invoice','','POST')) {
	    $out = nl2br(trim(htmlspecialchars(substitute(substitute($info->getTemplate('outside_invoice_addon'
			, $auction->getMyLang()/*SiteToCountryCode($auction->get('siteid'))*/), $auction->data), $info->data))));
		$smarty->assign('outside_addon', $out);
//		die($out);
    }
	$vat = VAT::get_vat_percent($db, $dbr, $auction);
    $order = Order::listAll($db, $dbr, $auction->get('auction_number'), $auction->get('txnid'), 1
		, $auction->getMyLang(), '0,1',1);
    $allorder = Order::listAll($db, $dbr, $auction->get('auction_number'), $auction->get('txnid'), 1
		, $auction->getMyLang(), '0,1',1);
		
	$info = new SellerInfo($db, $dbr, $auction->get('username'), $deflang);
	if ((requestVar('produce_invoice','','POST') || requestVar('produce_outside_invoice','','POST')) && strlen($_POST['customseller']))
		$info = new SellerInfo($db, $dbr, $_POST['customseller'], $deflang);
	$info->data->country_name = translate($db, $dbr, $info->data->country_name, $auction->getMyLang(), 'country', 'name');
    $smarty->assign('sellerInfo', $info->data);
    $invoice->data->vat_shipping = round($info->get('vat') * $invoice->data->total_shipping / (100 + $info->get('vat')),2);
    $invoice->data->shipping_no_vat = $invoice->data->total_shipping - $invoice->data->vat_shipping;
	if (strlen($auction->get('customer_vat'))) $vat = 0;
    $smarty->assign('vat', $vat);
	$invoice->data->invoice_date_utf8 = utf8_encode(strftime($info->data->date_format_invoice, strtotime($invoice->data->invoice_date)));
    $smarty->assign('invoice', $invoice->data);
    $auctiondata = $auction->data; 
	$auctiondata->vat_id = $info->get('vat_id');
    $auctiondata->seller_email = $info->get('email');
	$sellerInfo_invoice_footer = substitute($info->getTemplate('invoice_footer'
		, $auction->getMyLang()/*SiteToCountryCode($auction->get('siteid'))*/), $auctiondata);
	$sellerInfo_invoice_footer = substitute($sellerInfo_invoice_footer, $info->data);
    $smarty->assign('sellerInfo_invoice_footer', $sellerInfo_invoice_footer);
	$sellerInfo_packing_list_footer = substitute($info->getTemplate('packing_list_footer'
		, $auction->getMyLang()/*SiteToCountryCode($auction->get('siteid'))*/), $auctiondata);
	$sellerInfo_packing_list_footer = substitute($sellerInfo_packing_list_footer, $info->data);
	$sellerInfo_packing_list_footer = substitute($sellerInfo_packing_list_footer, $info->data);
    $smarty->assign('sellerInfo_packing_list_footer', $sellerInfo_packing_list_footer);
	foreach ($order as $key=>$item) {
        $article = new Article($db, $dbr, $item->article_id);
		$order[$key]->parcels = $article->parcels;
	}
	
    $smarty->assign('order', $order);
    $smarty->assign('allorder', $allorder);
	$auction->data->country_invoice = translate($db, $dbr, $auction->data->country_invoice, $auction->getMyLang());
	$auction->data->country_shipping = translate($db, $dbr, $auction->data->country_shipping, $auction->getMyLang());
    $smarty->assign('auction', $auction->data);
    $smarty->assign("currency",siteToSymbol($auction->data->siteid));
    $smarty->assign("payments", Payment::listAll($db, $dbr, $auction->get('auction_number'), $auction->get('txnid')));
    $smarty->assign("feepayments", Payment::listAll($db, $dbr, $auction->get('auction_number'), $auction->get('txnid'), 1));
    $smarty->assign("rmas", Rma::listAll($db, $dbr, $auction));
	$smarty->assign('total_weight', $auction->getTotalWeight());
	$smarty->assign('total_volume', $auction->getTotalVolume());
	$resp = $dbr->getOne("SELECT username
		FROM (
		SELECT NULL rma_id, username, time
		FROM prologis_log.auction_log
		WHERE auction_number = $number
		AND txnid = $txnid
		UNION ALL 
		SELECT rma_id, responsible_uname username, create_date time
		FROM rma
		WHERE auction_number = $number
		AND txnid = $txnid
		)t
		ORDER BY rma_id DESC , time DESC limit 1");
	if (PEAR::isError($resp)) aprint_r($resp);
	$resp = translate($db, $dbr, $resp, $deflang);
	$smarty->assign('resp', $resp);
	$seller_def_warehouse = new Warehouse($db, $dbr, $info->get('default_warehouse_id'));
	$smarty->assign('warehouse', $seller_def_warehouse->data);
	$customer = $dbr->getRow("select * from customer".$au_type." where id='".$auction->get('customer_id')."'");
	$customer->reseller_logo_url = $siteURL."images/cache/undef_src_customer".$au_type."_picid_".$customer->id.'_image.jpg';
	$customer->country_invoice = translate($db, $dbr, $customer->country_invoice, $auction->getMyLang(), 'country', 'name');
	$smarty->assign('customer', $customer);
    $smarty->display('invoice.tpl');
} elseif (requestVar('labels','','POST') || requestVar('labeldefault','','POST')) {

    $order = Order::listAll($db, $dbr, $auction->get('auction_number'), $auction->get('txnid'));
    		    	$shipping_plan_id = $dbr->getOne("SELECT value
				FROM translation
				WHERE table_name = 'offer'
				AND field_name = 'shipping_plan_id'
				AND language = '".$auction->get('siteid')."'
				AND id = ".$offer->get('offer_id'));
	$costs = ShippingPlan::getCostsByCountry($db, $dbr, $shipping_plan_id, CountryToCountryCode($auction->get('country_shipping')));
    $invoice = new Invoice($db, $dbr, $auction->get('invoice_number'));
//	var_dump($invoice);exit;
	$def_method = ($auction->get('payment_method')==2) ? $costs->cod_company_id : $costs->company_id;
	if ($def_method)
	    $offer->set('default_shipping_method', $def_method);
    $smarty->assign('offer', $offer->data);
    $volume = 0;
    $nitems = 0;
    $weight = 0;
    foreach ($order as $item) {
        $volume += $item->quantity * $item->volume_per_single_unit;
        $nitems += $item->quantity;
        $weight += $item->quantity * $item->weight_per_single_unit;
    }
    $smarty->assign('delivery_date', '0');
    $smarty->assign('delivery_date_title', 'Shipping date');
    $invoice = new Invoice($db, $dbr, $auction->get('invoice_number'));
    $total = $invoice->get('total_price') + $invoice->get('total_shipping');
    $smarty->assign('total', number_format($total, 2));
    $smarty->assign('nitems', $nitems);
    $smarty->assign('volume', number_format($volume, 2));
	$auction->data->country_invoice = translate($db, $dbr, $auction->data->country_invoice, $auction->getMyLang());
	$auction->data->country_shipping = translate($db, $dbr, $auction->data->country_shipping, $auction->getMyLang());
    $smarty->assign('auction', $auction->data);
	$cost = new ShippingCost($db, $dbr, $shipping_cost_id);
	$mid = $cost->get('shipping_method_id');
	$method = new ShippingMethod($db, $dbr, $_POST['shipping_method_id']);

		\label\HandlerFabric::handle($method->get('label_type'), $auction, $label_config)
				->setParam('total_price', $total)
				->setParam('DB', $db)
				->setParam('method', $method)
				->action();

    if ($_POST['labeldefault']) {
        $smarty->assign('methods', ShippingMethod::listArray($db, $dbr));
		$methodsall = ShippingMethod::listAll($db, $dbr);
        $smarty->assign('methodsall', $methodsall);
		$smarty->assign('frankatur', $methodsall[0]->default_frankatur);
        $smarty->assign('nomenu', true);
		$offer = new Offer($db, $dbr, $auction->get('offer_id'));
    		    	$shipping_plan_id = $dbr->getOne("SELECT value
				FROM translation
				WHERE table_name = 'offer'
				AND field_name = 'shipping_plan_id'
				AND language = '".$auction->get('siteid')."'
				AND id = ".$offer->get('offer_id'));
		$plan = new ShippingPlan($db, $dbr, $shipping_plan_id);
#		echo '$plan='.$shipping_plan_id;
		$cost = new ShippingCost($db, $dbr, $plan->get('shipping_cost_id'));
#		echo '$cost='.$plan->get('shipping_cost_id');
		$shipping_cost_id = ShippingPlan::findLeaf($db, $dbr, $shipping_plan_id, CountryToCountryCode($auction->get('country_shipping')));
#		echo '$shipping_cost_id='.$shipping_cost_id;
		$cost = new ShippingCost($db, $dbr, $shipping_cost_id);
        $smarty->assign('plan', $plan->data);
        $smarty->assign('cost', $cost->data);

        $mid = $cost->get('shipping_method_id');
        $method = new ShippingMethod($db, $dbr, $mid);
        $labels = $method->get('labels');

#		echo '$mid='.$mid;
		$info = new SellerInfo($db, $dbr, $auction->get('username'), $deflang);
        $vars = array(
            'delivery_date' => '0',
            'method' =>  $method->data,
            'weight' =>  '',
            'volume' =>  '',
            '_company'   =>  $auction->get('company_shipping'),
            '_gender'   =>  $english[$auction->get('gender_shipping')],
            '_firstname'   =>  $auction->get('firstname_shipping'),
            '_name'   =>  $auction->get('name_shipping'),
            '_street' =>  $auction->get('street_shipping'),
            '_house' =>  $auction->get('house_shipping'),
            '_city'   =>  $auction->get('zip_shipping').' '.$auction->get('city_shipping'),
            '__city'   =>  $auction->get('city_shipping'),
            '__zip'   =>  $auction->get('zip_shipping'),
            '_country'=>  $auction->get('country_shipping'),
            '_tel'=>  $auction->get('tel_shipping'),
            'tel'  =>  $auction->get('tel_shipping'),
            'phone'  =>  $auction->get('tel_shipping'),
            'info'   =>  '',
            'frankatur'   =>  $method->get('default_frankatur'),
            'Paket'   =>  '',
            'Halbpalette'   =>  '',
            'Palette'   =>  '',
            'total' =>  number_format($total, 2),
            'nitems'=>  $nitems,
            'auction'=>$auction->data,
            'contact_name'=>$info->get('contact_name'),
            'seller_email'=>$info->get('email'),
        );
        PrinterLog::Log($db, $dbr, $auction->get('auction_number'), $auction->get('txnid'), $loggedUser->get('username'), 'Printed shipping labels '.$labels);
        include "labels.php";
    } elseif (!$_POST['produce']) {
        $smarty->assign('methods', ShippingMethod::listArray($db, $dbr));
		$methodsall = ShippingMethod::listAll($db, $dbr);
        $smarty->assign('methodsall', $methodsall);
		$smarty->assign('frankatur', $methodsall[0]->default_frankatur);
        $smarty->assign('nomenu', true);
		$offer = new Offer($db, $dbr, $auction->get('offer_id'));
    		    	$shipping_plan_id = $dbr->getOne("SELECT value
				FROM translation
				WHERE table_name = 'offer'
				AND field_name = 'shipping_plan_id'
				AND language = '".$auction->get('siteid')."'
				AND id = ".$offer->get('offer_id'));
//		$shipping_plan_id = ShippingPlan::findLeaf($db, $dbr, $shipping_plan_id, CountryToCountryCode($auction->get('country_shipping')));
		$plan = new ShippingPlan($db, $dbr, $shipping_plan_id);
//		echo 'shipping_plan_id='.$shipping_plan_id; 
		$cost = new ShippingCost($db, $dbr, $plan->get('shipping_cost_id'));
		$shipping_cost_id = ShippingPlan::findLeaf($db, $dbr, $shipping_plan_id, CountryToCountryCode($auction->get('country_shipping')));
		$cost = new ShippingCost($db, $dbr, $shipping_cost_id);
//		echo "<br>cost=$shipping_cost_id"; 
        $smarty->assign('plan', $plan->data);
        $smarty->assign('cost', $cost->data);
        if (serverToLocal(date('Y-m-d H:i:s'), $timediff) < $auction->get('delivery_date_customer'))	
		   $smarty->assign('errortext', "WARNING: Shipping date is on ".$auction->get('delivery_date_customer'));
		$smarty->assign('genders', array($english[170]=>$english[170], $english[171]=>$english[171]));
		if(isset($_POST['shipping_method_id']) && !empty($_POST['shipping_method_id']))
			$smarty->assign('shipping_method_id', intval($_POST['shipping_method_id']));
        $smarty->display('labels.tpl');
    } else {
        $mid = requestVar('method');
        $method = new ShippingMethod($db, $dbr, $mid);
        $labels = $method->get('labels');
		$info = new SellerInfo($db, $dbr, $auction->get('username'), $deflang);
        $vars = array(
            'delivery_date' => $_POST['date'] ? $_POST['date'] : '0',
            'method' =>  $method->data,
            'weight' =>  $_POST['weight'],
            'volume' =>  $_POST['volume'],
            '_company'   =>  utf8_decode($_POST['company']),
            '_gender'   =>  utf8_decode($_POST['gender']),
            '_firstname'   =>  utf8_decode($_POST['firstname']),
            '_name'   =>  utf8_decode($_POST['name']),
            '_street' =>  utf8_decode($_POST['street']),
            '_house' =>  utf8_decode($_POST['house']),
            '_city'   =>  utf8_decode($_POST['city']),
            '__city'   =>  utf8_decode($_POST['__city']),
            '__zip'   =>  utf8_decode($_POST['__zip']),
            '_country'=>  utf8_decode($_POST['country']),
            '_tel'=>  $_POST['tel'],
            'tel'  =>  $_POST['tel'],
            'phone'  =>  $_POST['tel'],
            'info'   =>  $_POST['info'],
            'frankatur'   =>  $_POST['frankatur'],
            'Paket'   =>  $_POST['Paket'],
            'Halbpalette'   =>  $_POST['Halbpalette'],
            'Palette'   =>  $_POST['Palette'],
            'description1'   =>  $_POST['description1'],
            'pallets1'   =>  $_POST['pallets1'],
            'cartons1'   =>  $_POST['cartons1'],
            'qty_on_pallet1'   =>  $_POST['qty_on_pallet1'],
            'description2'   =>  $_POST['description2'],
            'pallets2'   =>  $_POST['pallets2'],
            'cartons2'   =>  $_POST['cartons2'],
            'qty_on_pallet2'   =>  $_POST['qty_on_pallet2'],
            'description3'   =>  $_POST['description3'],
            'pallets3'   =>  $_POST['pallets3'],
            'cartons3'   =>  $_POST['cartons3'],
            'qty_on_pallet3'   =>  $_POST['qty_on_pallet3'],
            'description4'   =>  $_POST['description4'],
            'pallets4'   =>  $_POST['pallets4'],
            'cartons4'   =>  $_POST['cartons4'],
            'qty_on_pallet4'   =>  $_POST['qty_on_pallet4'],
            'description5'   =>  $_POST['description5'],
            'pallets5'   =>  $_POST['pallets5'],
            'cartons5'   =>  $_POST['cartons5'],
            'qty_on_pallet5'   =>  $_POST['qty_on_pallet5'],
            'DNN'   =>  $_POST['DNN'],
            'quote'   =>  $_POST['quote'],
            'total' =>  number_format($total, 2),
            'nitems'=>  $nitems,
            'auction'=>$auction->data,
            'contact_name'=>$info->get('contact_name'),
            'seller_email'=>$info->get('email'),
        );
        PrinterLog::Log($db, $dbr, $auction->get('auction_number'), $auction->get('txnid'), $loggedUser->get('username'), 'Printed shipping labels '.$labels);
        include "labels.php";
    }
} else {
    $invoice = new Invoice($db, $dbr, $auction->get('invoice_number'));
    if ($resend = requestVar('resend','','POST')) {
		$lang = $auction->get("lang");
		if ($resend == 'mass_doc_after_ticket_open') {
			$auction->mass_doc_rma();
			header('Location: auction.php?number='.$auction->get('auction_number') . '&txnid=' . $auction->get('txnid'));
        	exit;
		}
		if ($resend == 'mass_doc_win') {
			$auction->mass_doc_win();
			header('Location: auction.php?number='.$auction->get('auction_number') . '&txnid=' . $auction->get('txnid'));
        	exit;
		}
		if ($resend == 'mass_doc') {
			$auction->mass_doc();
			header('Location: auction.php?number='.$auction->get('auction_number') . '&txnid=' . $auction->get('txnid'));
        	exit;
		}
		$auction->data->shop_id = $shop_id;
        $email_result = standardEmail($db, $dbr, $auction, $resend);
		if (!$email_result) 
		    $smarty->assign('errortext', 'SMTP Error');	
        $smarty->assign('resent', $resend);
    }
    if ($_GET['newinvoice']) {
		$new_auction_number = $dbr->getOne(
	            "SELECT IFNULL(MAX(auction_number+1), 1) FROM auction
				WHERE txnid = 3 
				and not IFNULL(custom_auction_number,0)");
		$already_exists = $dbr->getOne(
	            "SELECT count(*) FROM auction 
				WHERE auction_number=$new_auction_number");
		while ($already_exists) {
			$new_auction_number++;
			$already_exists = $dbr->getOne(
		            "SELECT count(*) FROM auction 
					WHERE auction_number=$new_auction_number");
		}
	    $new_auction = new Auction($db, $dbr);
	    $new_auction->set('response_username', $loggedUser->get('username'));
	    $new_auction->set('username', $auction->get('username'));
	    $new_auction->set('username_buyer', $auction->get('username_buyer'));
	    $new_auction->set('email', $auction->get('email'));
	    $new_auction->set('auction_number', $new_auction_number);
	    $new_auction->set('end_time', ServerTolocal(date("Y-m-d H:i:s"), $timediff));
	    $new_auction->set('txnid', $auction->get('txnid'));
	    $new_auction->set('server', $auction->get('server'));
	    $new_auction->set('offer_id', 0);
	    $new_auction->set('rma_id', $rma_id);
	    $new_auction->set('siteid', $auction->get('siteid'));
	    $new_auction->set('customer_id', $auction->get('customer_id')); # asked by "Thomas Redner" <redner@beliani.de>
	    $new_auction->set('process_stage', STAGE_ORDERED);
	    $new_auction->set('company_invoice', $auction->get('company_invoice'));
	    $new_auction->set('company_shipping', $auction->get('company_shipping'));
	    $new_auction->set('gender_invoice', $auction->get('gender_invoice'));
	    $new_auction->set('gender_shipping', $auction->get('gender_shipping'));
	    $new_auction->set('firstname_invoice', $auction->get('firstname_invoice'));
	    $new_auction->set('firstname_shipping', $auction->get('firstname_shipping'));
	    $new_auction->set('name_invoice', $auction->get('name_invoice'));
	    $new_auction->set('name_shipping', $auction->get('name_shipping'));
	    $new_auction->set('street_invoice', $auction->get('street_invoice'));
	    $new_auction->set('street_shipping', $auction->get('street_shipping'));
	    $new_auction->set('house_invoice', $auction->get('house_invoice'));
	    $new_auction->set('house_shipping', $auction->get('house_shipping'));
	    $new_auction->set('zip_invoice', $auction->get('zip_invoice'));
	    $new_auction->set('zip_shipping', $auction->get('zip_shipping'));
	    $new_auction->set('city_invoice', $auction->get('city_invoice'));
	    $new_auction->set('city_shipping', $auction->get('city_shipping'));
	    $new_auction->set('country_invoice', $auction->get('country_invoice'));
	    $new_auction->set('country_shipping', $auction->get('country_shipping'));
	    $new_auction->set('email_invoice', $auction->get('email_invoice'));
	    $new_auction->set('email_shipping', $auction->get('email_shipping'));
	    $new_auction->set('tel_invoice', $auction->get('tel_invoice'));
	    $new_auction->set('tel_shipping', $auction->get('tel_shipping'));
	    $new_auction->set('cel_invoice', $auction->get('cel_invoice'));
	    $new_auction->set('cel_shipping', $auction->get('cel_shipping'));
	    $new_auction->set('based_on_auction_id', $auction->get('id'));
	//	print_r($new_auction);
	    $new_auction->update();
        header('Location: auction.php?number='.$new_auction->get('auction_number') . '&txnid=' . $new_auction->get('txnid'));
        exit;
    }
    if (requestVar('noask', '', 'POST')) {
        $auction->set('rating_reminder_sent', 1);
        $auction->update();
        header('Location: auction.php?number='.$auction->get('auction_number') . '&txnid=' . $auction->get('txnid'));
        exit;
    }
    if (requestVar('ready_to_pickup', '', 'POST')) {
        $auction->set('ready_to_pickup', 1);
        $auction->set('process_stage', STAGE_READY_TO_PICKUP);
        $auction->set('pickup_warehouse', requestVar('pickup_warehouse', 0, 'POST'));
        $auction->set('delivery_date_real', date('Y-m-d H:i:s'));
        $auction->update();
        standardEmail($db, $dbr, $auction, 'ready_to_pickup');
        header('Location: auction.php?number='.$auction->get('auction_number') . '&txnid=' . $auction->get('txnid'));
        exit;
    }
    if (requestVar('pickedup', '', 'POST')) {
        $auction->set('pickedup', 1);
        $auction->set('process_stage', STAGE_WAIT_FOR_RATING);
        $auction->set('delivery_date_real', date('Y-m-d H:i:s'));
        $auction->update();
        header('Location: auction.php?number='.$auction->get('auction_number') . '&txnid=' . $auction->get('txnid'));
        exit;
    }
    if (isset($_POST['tracking_number_submit'])) {
		$user = new User($db, $dbr, $_POST['username']);
		foreach($_POST['quantity_max'] as $order_id=>$quantity_max) {
			if ($_POST['quantity2process'][$order_id]<$_POST['quantity_max'][$order_id]) {
				Order::split_order($db, $dbr, $order_id, $_POST['quantity_max'][$order_id]-$_POST['quantity2process'][$order_id]);
			}
		} // split orders
		if (isset($_POST['createnumber']) || isset($_POST['shipnumbers'])) {
			foreach($_POST['tracking_number'] as $key=>$tracking_number) {
			    if (strlen($tracking_number) || (int)$_POST['method'][$key]) {
					$packet_id = (int)$_POST['packet_id'][$key];
					$method = (int)$_POST['method'][$key];
			        $tn_id = $auction->addTrackingNumber($tracking_number, $method, 
						serverToLocal(date('Y-m-d H:i:s'), $timediff), $user->get('username'), $packet_id);
					if ($tn_id) foreach ($_POST['pack'] as $order_id) {
							$r = $db->query("insert into tn_orders SET tn_id=$tn_id, order_id = $order_id");     
					  		if (PEAR::isError($r)) aprint_r($r);
					}
			    }
			} // foreach number
		} // if create number
		if (isset($_POST['shipnumbers']) && $_POST['shipnumbers']) {
			foreach ($_POST['ready2pickup'] as $order_id) {
				$q = "UPDATE orders SET ready2pickup=1
					WHERE IFNULL(send_warehouse_id,0) and id = $order_id";
				$r = $db->query($q);     
		  		if (PEAR::isError($r)) aprint_r($r);
			} // if send some articles
			if ($_POST['shipnumbers']==2) {
				standardEmail($db, $dbr, $auction, 'ready_to_pick_up_details');
			}
			if ($_POST['shipnumbers']==1) {
				foreach ($_POST['send'] as $order_id) {
					$q = "UPDATE orders SET sent=1, delivery_username='".$loggedUser->get('username')."' 
						WHERE IFNULL(send_warehouse_id,0) and IFNULL(reserve_warehouse_id,0) and id = $order_id";
					$r = $db->query($q);
			  		if (PEAR::isError($r)) aprint_r($r);
					// send_SERVICEQUITTUNG for all TN of this article, but only once
					$r = $dbr->getAll("select tn.* 
						from tracking_numbers tn
						join tn_orders tno on tno.tn_id=tn.id
						WHERE tno.order_id = $order_id AND tn.sent=0
						");
    				if (PEAR::isError($r)) aprint_r($r);
					foreach ($r as $tn) {
						$already_sent_this_method = $dbr->getOne("select count(*) from tracking_numbers tn
							WHERE tn.auction_number=".$auction->get('auction_number')." 
							and tn.txnid=".$auction->get('txnid')." 
							and tn.sent=1 and tn.shipping_method=".$tn->shipping_method);
						if ($already_sent_this_method) {
							$r = $db->query("UPDATE tracking_numbers SET sent=1 WHERE id = ".$tn->id);
			  				if (PEAR::isError($r)) aprint_r($r);
	  					} elseif (standardEmail($db, $dbr, $auction, 'send_SERVICEQUITTUNG', $tn->id)) {
							$r = $db->query("UPDATE tracking_numbers SET sent=1 WHERE id = ".$tn->id);
			  				if (PEAR::isError($r)) aprint_r($r);
						} // if sent ok
					} // foreach tn
				} // foreach sent article
				if (count($_POST['send'])) 	{
				/*if (!$auction->get('dontsend_marked_as_Shipped')) moved to standardEmail*/
					standardEmail($db, $dbr, $auction, 'mark_as_shipped');
				}
		    	mark_auction_as_shipped($db, $dbr, $auction, $user);
			} // if send some articles
		} // if ship
		header('Location: auction.php?number='.$auction->get('auction_number').'&txnid='.$auction->get('txnid'));
		exit;
	}
		if (isset($_FILES['tndoc']['name']) && $_FILES['tndoc']['name']) {
			$doc = file_get_contents($_FILES['tndoc']['tmp_name']);
			$name = basename ($_FILES['tndoc']['name']);
			$r = $db->query("update tracking_numbers set doc_name='$name', doc='".mysql_escape_string (base64_encode($doc))."'
			where id=".$_POST['tnid']);
            		if (PEAR::isError($r)) aprint_r($r->getMessage());
    	    header('Location: auction.php?number='.$auction->get('auction_number').'&txnid='.$auction->get('txnid'));
	        exit;
      	} // pic upload

if ($debug) echo '7: '.round((getmicrotime()-$time),3).'<br>';if ($debug) $time = getmicrotime();

    if (isset($_POST['send'])) {
        $method = new ShippingMethod($db, $dbr, $_POST['method']);
		$auction->data->email_shipping = $method->get('sd_email');
		$auction->data->email_invoice = $method->get('sd_email');
		$auction->data->email = $method->get('sd_email');
        if ($_POST['send']=='Send invoice') {
			$r = standardEmail($db, $dbr, $auction, 'send_invoice_shippingcompany');
		}
        if ($_POST['send']=='Send Packing list') {
			$r = standardEmail($db, $dbr, $auction, 'send_packing_list_shippingcompany');
		}
		if (!$r) {
			$smarty->assign('errortext', 'Email was not sent');
		} else {
	   	    header('Location: auction.php?number='.$auction->get('auction_number').'&txnid='.$auction->get('txnid'));
    	    exit;
		}
	}	


    if ($account = requestVar('account','')) {
        $date = $_POST['Date_Year'] . '-' . $_POST['Date_Month'] . '-' . $_POST['Date_Day'];
        $total = Payment::total($db, $dbr, $auction->get('auction_number'), $auction->get('txnid'));
        $amount = requestVar('amount', '');
		if (isset($_POST['safer'])) {
							$token = generate_password(20);
							$r = $db->query("insert into _tmp_pp_token set
									token='$token',
									orderid='".base64_encode(serialize(array_merge($_REQUEST
										, array('AMOUNT'=>number_format($amount,2)))))."'
							");
							$saferpay_attribs = array(
								'ORDERID' => $token,//base64_encode(serialize($_REQUEST)),
								'TOKEN' => urlencode("test info"),
								'AMOUNT' => ceil($amount*100),
								'DESCRIPTION' => urlencode('custom payment'),
								'CURRENCY' => siteToSymbol($auction->data->siteid),
						    	'SUCCESSLINK' => "http://".$_SERVER['HTTP_HOST']."/auction.php?checkout=3", /* return URL if payment successful */
						    	'FAILLINK' => "http://".$_SERVER['HTTP_HOST']."/auction.php?number=".$auction->get('auction_number').'&txnid='.$auction->get('txnid'), /* return URL if payment failed */
						    	'BACKLINK' => "http://".$_SERVER['HTTP_HOST']."/auction.php?number=".$auction->get('auction_number').'&txnid='.$auction->get('txnid'), /* return URL if user cancelled */
						    	'NOTIFYADDRESS' => urlencode("baserzas@gmail.com"),
							);
							$saferpay_url="https://www.saferpay.com/hosting/CreatePayInit.asp?ACCOUNTID=".$info->get('saferpay_account');
							foreach ($saferpay_attribs as $key=>$value) $saferpay_url.="&$key=$value";
							$resurl = file_get_contents($saferpay_url);
						   	header("Location: $resurl");
				    	    exit;
		}
		if (isset($_POST['paypal'])) {
							$token = generate_password(20);
							$info->getPayPal($API_UserName, $API_Password, $API_Signature);
							// Set request-specific fields.
							$paymentAmount = urlencode(number_format($amount,2));
							$currencyID = urlencode(siteToSymbol($auction->data->siteid));
							$paymentType = urlencode('Sale');				// or 'Sale' or 'Order'
							$returnURL = urlencode("http://".$_SERVER['HTTP_HOST']."/auction.php?number=".$auction->get('auction_number').'&txnid='.$auction->get('txnid')."&checkout=4");
							$cancelURL = urlencode("http://".$_SERVER['HTTP_HOST']."/auction.php?number=".$auction->get('auction_number').'&txnid='.$auction->get('txnid'));
							
							// Add request-specific fields to the request string.
							$langs = getLangsArray($db, $dbr);
							$nvpStr = "&Amt=$paymentAmount&ReturnUrl=$returnURL&CANCELURL=$cancelURL&PAYMENTACTION=$paymentType&CURRENCYCODE=$currencyID&LOCALECODE=".strtoupper(substr($langs[$auction->get('lang')], 0, 2));
							
							// Execute the API operation; see the PPHttpPost function above.
							$httpParsedResponseAr = PPHttpPost('SetExpressCheckout', $nvpStr);
							
							if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"])) {
								// Redirect to paypal.com.
								$token = urldecode($httpParsedResponseAr["TOKEN"]);
								$payPalURL = "https://www.paypal.com/webscr&cmd=_express-checkout&token=$token";
								if("sandbox" === $environment || "beta-sandbox" === $environment) {
									$payPalURL = "https://www.$environment.paypal.com/webscr&cmd=_express-checkout&token=$token";
								}
								$r = $db->query("insert into _tmp_pp_token set
									token='$token',
									orderid='".base64_encode(serialize(array_merge($_REQUEST
										, array('AMOUNT'=>number_format($amount,2)))))."'
									");
								header("Location: $payPalURL");
								exit;
							} else  {
								if ($debug) echo '$nvpStr='.$nvpStr.'<br>';
								exit('SetExpressCheckout failed: ' . print_r($httpParsedResponseAr, true));
							}
		}
        if ($amount != '') {
            $due = $invoice->get('total_price') + $invoice->get('total_shipping') + $invoice->get('total_cod');
            if (/*($total < $due) && */(($total + $amount) >= $due) 
					&& (int)$dbr->getOne("select send_payment_confirmation from accounts where number='$account'")) {
                $auction->set('paid', 1);
                if ($auction->get('process_stage') < STAGE_PAID) {
                    $auction->set('process_stage', STAGE_PAID);
                }
                $auction->update();
				$template = $dbr->getOne("select email_template_thanks from payment_method where `code`='".$auction->get('payment_method')."'");
                standardEmail($db, $dbr, $auction, $template);
            }
            $payment = new Payment($db, $dbr);
			$r = VAT::get_vat_attribs($db, $dbr, $auction);
//			print_r($r); die();
            if (!PEAR::isError($r)) {
	            $payment->set('vat_percent', (strlen($auction->get('customer_vat')) ? 0 : $r->vat_percent));
	            $payment->set('vat_account_number', (strlen($auction->get('customer_vat')) ? $r->out_vat : $r->vat_account_number));
	            $payment->set('selling_account_number', (strlen($auction->get('customer_vat')) ? $r->out_selling : $r->selling_account_number));
			};	
            $payment->set('auction_number', $auction->get('auction_number'));
            $payment->set('txnid', $auction->get('txnid'));
            $payment->set('amount', $amount);
            $payment->set('account', $account);
            $payment->set('payment_date', $date.' '.date("H:i:s"));
			$payment->set('listingfee', 0);
            $payment->set('username', $loggedUser->get('username'));
            $payment->set('comment', mysql_escape_string(requestVar('paycomment','','POST')));
            $payment->update();
		    setcookie("payment_default_date",$date);
		    setcookie("default_account",$account);
			$invoice->recalcOpenAmount();
        }
        header('Location: auction.php?number='.$auction->get('auction_number').'&txnid=' . $auction->get('txnid'));
        exit;
    }
    if ($amount = $_POST['feepayamount']) {
		$account = requestVar('feepayaccount','','POST');
        $date = $_POST['Date_Year'] . '-' . $_POST['Date_Month'] . '-' . $_POST['Date_Day'];
            $payment = new Payment($db, $dbr);
			$r = VAT::get_vat_attribs($db, $dbr, $auction);
            if (!PEAR::isError($r)) {
	            $payment->set('vat_percent', (strlen($auction->get('customer_vat')) ? 0 : $r->vat_percent));
	            $payment->set('vat_account_number', strlen($_POST['feevataccount']) ? ($_POST['feevataccount']) 
					: (strlen($auction->get('customer_vat')) ? $r->out_vat : $r->vat_account_number));
	            $payment->set('selling_account_number', strlen($_POST['feesellingaccount']) ? ($_POST['feesellingaccount']) 
					: (strlen($auction->get('customer_vat')) ? $r->out_selling : $r->selling_account_number));
			};	
            $payment->set('auction_number', $auction->get('auction_number'));
            $payment->set('txnid', $auction->get('txnid'));
            $payment->set('amount', $amount);
            $payment->set('account', $account);
            $payment->set('listingfee', 1);
            $payment->set('payment_date', $date.' '.date("H:i:s"));
            $payment->set('username', $loggedUser->get('username'));
            $payment->set('comment', mysql_escape_string(requestVar('feepaycomment','','POST')));
            $payment->update();
		    setcookie("payment_default_date_free",$date);
		    setcookie("default_account_free",$account);
		    setcookie("default_vat_account_free",strlen($_POST['feevataccount']) ? ($_POST['feevataccount']) 
					: (strlen($auction->get('customer_vat')) ? $r->out_vat : $r->vat_account_number));
		    setcookie("default_selling_account_free",strlen($_POST['feesellingaccount']) ? ($_POST['feesellingaccount']) 
					: (strlen($auction->get('customer_vat')) ? $r->out_selling : $r->selling_account_number));
        header('Location: auction.php?number='.$auction->get('auction_number').'&txnid=' . $auction->get('txnid'));
        exit;
    }
    if (isset($_POST['dontsend_marked_as_Shipped'])) {
		$dontsend_marked_as_Shipped = $_POST['dontsend_marked_as_Shipped'];
		$auction->set('dontsend_marked_as_Shipped', $dontsend_marked_as_Shipped);
		$auction->update();
        header('Location: auction.php?number='.$auction->get('auction_number').'&txnid=' . $auction->get('txnid'));
        exit;
    }
    if (isset($_POST['recalculate'])) {
		calcAuction($db, $dbr, $auction);
		header('Location: auction.php?number='.$auction->get('auction_number').'&txnid=' . $auction->get('txnid'));
        exit;
    }
    if ($CRM_Ticket = requestVar('CRM_Ticket','','POST')) {
//        $date = $_POST['Date_Year'] . '-' . $_POST['Date_Month'] . '-' . $_POST['Date_Day'];
            $rma = new Rma($db, $dbr, $auction);
            $rma->set('create_date', ServerTolocal(date("Y-m-d H:i:s"), $timediff));
			$rma->set('responsible_uname', $loggedUser->get('username'));
            $rma->update();
        header('Location: rma.php?rma_id='.$rma->get('rma_id').'&number='.$auction->get('auction_number').'&txnid=' . $auction->get('txnid'));
        exit;
    }
    if (isset($_POST['subinvoice'])) {
        list ($main_auction_number, $main_txnid) = explode('/', requestVar('main_number'));
		$auction->makeSubinvoiceFor($main_auction_number, $main_txnid);
        header('Location: auction.php?number='.$auction->get('auction_number').'&txnid=' . $auction->get('txnid'));
        exit;
    }
    if (isset($_POST['releasesubinvoice'])) {
		Auction::releaseSubinvoice($db, $dbr, $auction->get('auction_number'), $auction->get('txnid'));
        header('Location: auction.php?number='.$auction->get('auction_number').'&txnid=' . $auction->get('txnid'));
        exit;
    }

if ($debug) echo '8: '.round((getmicrotime()-$time),3).'<br>';if ($debug) $time = getmicrotime();

	$rmas = Rma::listAll($db, $dbr, $auction);
if ($debug) echo '8.1: '.round((getmicrotime()-$time),3).'<br>';if ($debug) $time = getmicrotime();

    if ($listingfeeamount = $_POST['listingfeeamount']) {
		$date = localToServer(date(date('Y-m-d'), time()), $timediff);
		if ($listingfeeamount)
			$auction->OpenListingFee($db, $dbr, $date, $listingfeeamount, $loggedUser->get('username'));
        header('Location: auction.php?number='.$auction->get('auction_number').'&txnid='.$auction->get('txnid'));
        exit;
    }
if ($debug) echo '8.2: '.round((getmicrotime()-$time),3).'<br>';if ($debug) $time = getmicrotime();
    
	$payments = Payment::listAll($db, $dbr, $auction->get('auction_number'), $auction->get('txnid'));
    $payment_total = 0;
    if (count($payments)) foreach ($payments as $i => $payment) {
        $payment_total += $payment->amount;
        $payments[$i]->payment_date = substr(serverToLocal($payments[$i]->payment_date, $timediff), 0, 10);
    }
if ($debug) echo '8.3: '.round((getmicrotime()-$time),3).'<br>';if ($debug) $time = getmicrotime();
	$feepayments = Payment::listAll($db, $dbr, $auction->get('auction_number'), $auction->get('txnid'), 1);
    $feepayment_total = 0;
    if (count($feepayments)) foreach ($feepayments as $i => $payment) {
        $feepayment_total += $payment->amount;
        $feepayments[$i]->payment_date = substr(serverToLocal($payments[$i]->payment_date, $timediff), 0, 10);
    }
if ($debug) echo '8.4: '.round((getmicrotime()-$time),3).'<br>';if ($debug) $time = getmicrotime();
	$labelLog = $dbr->getAll("select tl.updated, u.username username
		, al.*
		from auction_label al
		left join total_log tl on tl.table_name='auction_label' and field_name='id'
			and tl.TableID=al.id
		left  join users u on u.system_username=tl.username
		where al.auction_number=$number and al.txnid=$txnid
		order by al.id");
	$smarty->assign('labelLog',$labelLog);
if ($debug) echo '8.5: '.round((getmicrotime()-$time),3).'<br>';if ($debug) $time = getmicrotime();
    $printerLog = PrinterLog::listAll($db, $dbr, $auction->get('auction_number'), $auction->get('txnid'));
if ($debug) echo '8.6: '.round((getmicrotime()-$time),3).'<br>';if ($debug) $time = getmicrotime();
    $emailLog =   EmailLog::listAll($db, $dbr, $auction->get('auction_number'), $auction->get('txnid'));
if ($debug) echo '8.7: '.round((getmicrotime()-$time),3).'<br>';if ($debug) $time = getmicrotime();
    foreach ($emailLog as $i => $entry) {
        $emailLog[$i]->date = serverToLocal($entry->date, $timediff);
    }
    $smsLog =   EmailLog::listAllSMS($db, $dbr, $auction->get('auction_number'), $auction->get('txnid'));
if ($debug) echo '8.8: '.round((getmicrotime()-$time),3).'<br>';if ($debug) $time = getmicrotime();
    $auctionLog =   AuctionLog::listAllbyAuction($db, $dbr, $auction->get('auction_number'), $auction->get('txnid'));
if ($debug) echo '8.9: '.round((getmicrotime()-$time),3).'<br>';if ($debug) $time = getmicrotime();
    foreach ($auctionLog as $i => $entry) {
        $auctionLog[$i]->time = serverToLocal($entry->time, $timediff);
    }

	$auction->data->shipping_method_str = $dbr->getOne("select name from payment_method 
		where code='".$auction->get('payment_method')."'");
//    $auction->data->end_time = serverToLocal($auction->data->end_time, 2/*$timediff*/);
	$auction->data->country_invoice = translate($db, $dbr, $auction->data->country_invoice, $auction->getMyLang());
	$auction->data->country_shipping = translate($db, $dbr, $auction->data->country_shipping, $auction->getMyLang());
if ($debug) echo '8.10: '.round((getmicrotime()-$time),3).'<br>';if ($debug) $time = getmicrotime();
    $smarty->assign('auction', $auction->data);
	$criteria['base_auction_number']=$auction->get('auction_number').'/'.$auction->get('txnid');
    ### for fast $second_chanced = Auction::findSecondChanced($db, $dbr,$criteria);
if ($debug) echo '8.11: '.round((getmicrotime()-$time),3).'<br>';if ($debug) $time = getmicrotime();
    $smarty->assign('second_chanced', $second_chanced);
    $smarty->assign('tracking_numbers', $auction->tracking_numbers);
    if ($method = $auction->get('shipping_method')) {
        $method = new ShippingMethod($db, $dbr, $method);
        $smarty->assign('method', $method->data);
    }
if ($debug) echo '8.12: '.round((getmicrotime()-$time),3).'<br>';if ($debug) $time = getmicrotime();
    if ($auction->get('delivery_date_customer') == '0000-00-00') {
        $smarty->assign('delivery_date', 'As soon as possible');
        $smarty->assign('delivery_date_title', 'Shipping date');
        $smarty->assign('delivery_date_reached', true);
    } else {
        $smarty->assign('delivery_date', $auction->get('delivery_date_customer'));
        $smarty->assign('delivery_date_title', 'Planned shipping date');
        $smarty->assign('delivery_date_reached', $auction->get('delivery_date_customer') <= date('Y-m-d'));
    }
    $smarty->assign('title', "Auftrag $number / $txnid");
    if ($auction->get('main_auction_number')) $smarty->assign('subtitle', " <font color='red'>(subinvoice)</font>");
    if ($dbr->getOne("select count(*) from auction where main_auction_number="
		.$auction->get('auction_number')." and main_txnid=".$auction->get('txnid'))) 
			$smarty->assign('subtitle', " <font color='red'>(main invoice)</font>");
    $smarty->assign('open_amount', number_format($invoice->get('open_amount'), 2));
    $smarty->assign('is_neighbour', 
		isZipInRange($db, $dbr, CountryToCountryCode($auction->get('country_shipping')), 'neighbours', $auction->get('zip_shipping')));
	$invoice->data->invoice_date_utf8 = utf8_encode(strftime($info->data->date_format_invoice, strtotime($invoice->data->invoice_date)));
    $smarty->assign('invoice', $invoice->data);
    $smarty->assign('payment_total', number_format($payment_total, 2));
    $smarty->assign('feepayment_total', number_format($feepayment_total, 2));
	$feeopen_amount = 0;
if ($debug) echo '8.13: '.round((getmicrotime()-$time),3).'<br>';if ($debug) $time = getmicrotime();
	$listingfees = $auction->GetListingFee();
if ($debug) echo '8.14: '.round((getmicrotime()-$time),3).'<br>';if ($debug) $time = getmicrotime();
	if (count($listingfees)) foreach ($listingfees as $p) {
		$feeopen_amount += $p->amount;
	};	
	if (count($feepayments)) foreach ($feepayments as $p) {
		$feeopen_amount -= $p->amount;
	};	

if ($debug) echo '9 1st: '.round((getmicrotime()-$time),3).'<br>';if ($debug) $time = getmicrotime();

    $smarty->assign('feeopen_amount', number_format($feeopen_amount, 2));
    $smarty->assign('payments', $payments);
    $smarty->assign('feepayments', $feepayments);
    $smarty->assign('rmas', $rmas);
    $smarty->assign('printerLog', $printerLog);
    $smarty->assign('emailLog', $emailLog);
    $smarty->assign('smsLog', $smsLog);
    $smarty->assign('auctionLog', $auctionLog);
    $smarty->assign('accounts', Account::listArray($db, $dbr, true));
    $smarty->assign("currency",siteToSymbol($auction->data->siteid));
#    $smarty->assign('template_names', $template_names);
    $smarty->assign('stage', stageName($auction->get('process_stage')));
    $smarty->assign('methods', ShippingMethod::listArray($db, $dbr));
    $smarty->assign('warehouses', Warehouse::listArray($db, $dbr));
    $smarty->assign('return', $_SERVER['REQUEST_URI']);
if ($debug) echo '9-1: '.round((getmicrotime()-$time),3).'<br>';if ($debug) $time = getmicrotime();
	if ($shop_id) {
		$loggedCustomer = $dbr->getRow("select * from customer where id=".$auction->get('customer_id'));
		$key = substr(md5($auction->data->auction_number.$auction->data->txnid.$auction->data->username
			.$loggedCustomer->email.$loggedCustomer->password), 0, 8);
		$order_url = 'http://'.$auction->get('server') . "/order/".$auction->get('auction_number')."-".$auction->get('txnid')."-{$key}/";
	} else {
	    $key = substr(md5($auction->get('auction_number').$auction->get('txnid').$auction->get('username')), 0, 8);
		$order_url = $siteURL . 'order.php?a='.$auction->get('auction_number').'&t='.$auction->get('txnid').'&k='.$key;
	}
if ($debug) echo '9-2: '.round((getmicrotime()-$time),3).'<br>';if ($debug) $time = getmicrotime();
    $smarty->assign('order_url', $order_url);
    $smarty->assign('_cgi_eBay',getParByName($db, $dbr, $auction->get('siteid'), "_cgi_eBay"));
	$smarty->assign('listingfees',$listingfees);
	$calcs = Auction::getCalcs($db, $dbr, $auction, 1); ///////////// 0.22
if ($debug) echo '9-3: '.round((getmicrotime()-$time),3).'<br>';if ($debug) $time = getmicrotime();
    $smarty->assign('calcs',$calcs);
	$sub_calcs = Auction::getCalcs($db, $dbr, $auction->data->subinvoices, 1); ///////////// 0.22
if ($debug) echo '9-4: '.round((getmicrotime()-$time),3).'<br>';if ($debug) $time = getmicrotime();
    $smarty->assign('sub_calcs',$sub_calcs);
	$total_calcs = Auction::getCalcs($db, $dbr, array_merge($auction->data->subinvoices, array($auction->data)), 1); ///////////// 0.22
if ($debug) echo '9-5: '.round((getmicrotime()-$time),3).'<br>';if ($debug) $time = getmicrotime();
    $smarty->assign('total_calcs',$total_calcs);
    $smarty->assign('end_time_90_reached', (time() - (strtotime($auction->get('end_time'))) >= 90*24*60*60));
	$smarty->assign('admin', $loggedUser->get('admin'));
	$smarty->assign('auction_delete', $loggedUser->get('auction_delete'));
// - for invoice
/*    $r = $dbr->getAssoc("SELECT CONCAT( auction_number, '/', txnid ), invoice_number
				FROM auction
				WHERE main_auction_number =".$auction->get('auction_number')."
				AND main_txnid =".$auction->get('txnid')); ////////// 0.2
    $subinvoices = array_map('intval', (array)$r);
	if (count($subinvoices)) {
    	$subinvoices = implode(',', $subinvoices);
	    $r = $db->query("SELECT SUM(total_price) total_price,
			SUM(total_shipping) total_shipping,
			SUM(total_cod) total_cod
			FROM invoice WHERE invoice_number IN (".$subinvoices.")");
		$sub = $r->fetchRow();	
		$invoice->data->total_price += $sub->total_price;
		$invoice->data->total_shipping += $sub->total_shipping;
		$invoice->data->total_cod += $sub->total_cod;
	};*/
	$vat = VAT::get_vat_percent($db, $dbr, $auction);
if ($debug) echo '9-6: '.round((getmicrotime()-$time),3).'<br>';if ($debug) $time = getmicrotime();
    $order = Order::listAll($db, $dbr, $auction->get('auction_number'), $auction->get('txnid'), 1
		, $auction->getMyLang(), '0,1',1); 
if ($debug) echo '9-7: '.round((getmicrotime()-$time),3).'<br>';if ($debug) $time = getmicrotime();
#    $allorder = Order::listAll($db, $dbr, $auction->get('auction_number'), $auction->get('txnid'), 1
#		, $auction->getMyLang(), '0,1');
	$allorder = $order;
	$allorder = array_filter($allorder, function($value){
		if($value->hide_in_order == 1) {
			return false;
		}
		return true;
	});

	$order = array_filter($order, function($value){
		if($value->hide_in_invoice == 1) {
			return false;
		}
		return true;
	});

	if ($debug) echo '9-8: '.round((getmicrotime()-$time),3).'<br>';if ($debug) $time = getmicrotime();
$warehouses_inactive = $dbr->getAssoc("select warehouse_id, inactive from warehouse where inactive");
foreach($allorder as $o) {
	unset($warehouses_inactive[$o->reserve_warehouse_id]);
	unset($warehouses_inactive[$o->send_warehouse_id]);
}
foreach($warehouses2shipNames as $warehouse_id=>$dummy) {
	if (isset($warehouses_inactive[$warehouse_id])) unset($warehouses2shipNames[$warehouse_id]);
}
$smarty->assign('warehouses2ship', $warehouses2shipNames);

if ($debug) echo '9 2nd: '.round((getmicrotime()-$time),3).'<br>';if ($debug) $time = getmicrotime();

	$info = new SellerInfo($db, $dbr, $auction->get('username'), $deflang);
if ($debug) echo '9.1: '.round((getmicrotime()-$time),3).'<br>';if ($debug) $time = getmicrotime();
	$info->data->country_name = translate($db, $dbr, $info->data->country_name, $auction->getMyLang(), 'country', 'name');
if ($debug) echo '9.2: '.round((getmicrotime()-$time),3).'<br>';if ($debug) $time = getmicrotime();
    $smarty->assign('sellerInfo', $info->data);
    $invoice->data->vat_shipping = round($info->get('vat') * $invoice->data->total_shipping / (100 + $info->get('vat')),2);
    $invoice->data->shipping_no_vat = $invoice->data->total_shipping - $invoice->data->vat_shipping;
	if (strlen($auction->get('customer_vat'))) $vat = 0;
    $smarty->assign('vat', $vat);
	$invoice->data->invoice_date_utf8 = utf8_encode(strftime($info->data->date_format_invoice, strtotime($invoice->data->invoice_date)));
    $smarty->assign('invoice', $invoice->data);
//    aprint_r($invoice);
    $auctiondata = $auction->data;
    $auctiondata->seller_email = $info->get('email');
	$auctiondata->vat_id = $info->get('vat_id');
	$sellerInfo_invoice_footer = substitute($info->getTemplate('invoice_footer'
		, $auction->getMyLang()/*SiteToCountryCode($auction->get('siteid'))*/), $auctiondata);
	$sellerInfo_invoice_footer = substitute($sellerInfo_invoice_footer, $info->data);
	$sellerInfo_invoice_footer = str_replace('[[seller_name]]',$info->data->seller_name, $sellerInfo_invoice_footer);
#	echo $sellerInfo_invoice_footer;
#	echo $lang; print_r($info->data);
    $smarty->assign('sellerInfo_invoice_footer', $sellerInfo_invoice_footer);
	$sellerInfo_packing_list_footer = substitute($info->getTemplate('packing_list_footer'
		, $auction->getMyLang()/*SiteToCountryCode($auction->get('siteid'))*/), $auctiondata);
	$sellerInfo_packing_list_footer = substitute($sellerInfo_packing_list_footer, $info->data);
    $smarty->assign('sellerInfo_packing_list_footer', $sellerInfo_packing_list_footer);
	foreach ($order as $key=>$item) {
        $article = new Article($db, $dbr, $item->article_id);
		$order[$key]->parcels = $article->parcels;
	}
if ($debug) echo '9.3: '.round((getmicrotime()-$time),3).'<br>';if ($debug) $time = getmicrotime();
    $smarty->assign('order', $order);
    
    $smarty->assign('allorder', $allorder);
    $auction->data->delivery_username = $dbr->getOne("select name from users where username='".$auction->data->delivery_username."'");
    $auction->data->deleted_by_name = $dbr->getOne("select name from users where username='".$auction->data->deleted_by."'");
    $auction->data->no_emails_by_name = $dbr->getOne("select name from users where username='".$auction->data->no_emails_by."'");
    $auction->data->complain_set_by_name = $dbr->getOne("select name from users where username='".$auction->data->complain_set_by."'");
	$auction->data->responsible_name = $dbr->getOne("select name from users where username='".$main_auction->data->responsible_uname."'");
if ($debug) echo '9.4: '.round((getmicrotime()-$time),3).'<br>';if ($debug) $time = getmicrotime();
    $smarty->assign("currency",siteToSymbol($auction->data->siteid));
    $smarty->assign("payments", Payment::listAll($db, $dbr, $auction->get('auction_number'), $auction->get('txnid')));
    $smarty->assign("feepayments", Payment::listAll($db, $dbr, $auction->get('auction_number'), $auction->get('txnid'), 1));
    $smarty->assign("rmas", Rma::listAll($db, $dbr, $auction));
if ($debug) echo '9.5: '.round((getmicrotime()-$time),3).'<br>';if ($debug) $time = getmicrotime();
// - for invoice
	$users = User::listArray($db, $dbr);
if ($debug) echo '9.6: '.round((getmicrotime()-$time),3).'<br>';if ($debug) $time = getmicrotime();
	$smarty->assign('users', $users);
	$vat_info = VAT::get_vat_attribs($db, $dbr, $auction);
	$accounts = Account::listArray($db, $dbr);	
if ($debug) echo '9.7: '.round((getmicrotime()-$time),3).'<br>';if ($debug) $time = getmicrotime();
	$vat_info->out_vat_v = $accounts[$vat_info->out_vat];	
	$vat_info->vat_account_number_v = $accounts[$vat_info->vat_account_number];	
	$vat_info->out_selling_v = $accounts[$vat_info->out_selling];	
	$vat_info->selling_account_number_v = $accounts[$vat_info->selling_account_number];	
	$smarty->assign('vat_info', $vat_info);
	$smarty->assign('_cgi_eBay_complain',getParByName($db, $dbr, $auction->get('siteid'), "_feedback_eBay"));
	$complain_succ = array(
	'' => '---',
	0 => 'No',
	1 => 'Yes'); 
	$smarty->assign('complain_succ', $complain_succ);
	if (0 && strlen($invoice->get('static_html'))) 
		$smarty->assign('static_html', base64_decode($invoice->get('static_html')));
/*	if (strlen($invoice->get('static_shipping_list_html'))) 
		$smarty->assign('static_shipping_list_html', base64_decode($invoice->get('static_shipping_list_html')));*/
	$smarty->assign('sellers', SellerInfo::listArray($db, $dbr));
	$smarty->assign('inss', Insurance::listAll($db, $dbr, $auction));
	if ($auction->get('rma_id')) {
		$row = $dbr->getRow('select auction_number, txnid from rma where rma_id='.$auction->get('rma_id'));
		$smarty->assign('rma_auction_number', $row->auction_number);
		$smarty->assign('rma_txnid', $row->txnid);
		if (strlen($row->auction_number)) $ratings = Rating::listAll($db, $dbr, $row->auction_number, $row->txnid);
	} else {
		$ratings = Rating::listAll($db, $dbr, $auction->get('auction_number'), $auction->get('txnid'));
	}
	$smarty->assign('rating_case_id', $ratings[0]->id);
if ($debug) echo '9.8: '.round((getmicrotime()-$time),3).'<br>';if ($debug) $time = getmicrotime();
//	print_r($ratings[0]);
	if ($auction->get('rma_id')) {
		if ($debug) echo 'select * 
	   	from auction join rma on auction.auction_number=rma.auction_number and auction.txnid=rma.txnid 
		   where rma.rma_id='.$auction->get('rma_id');
	   $source = $dbr->getRow('select * 
	   	from auction join rma on auction.auction_number=rma.auction_number and auction.txnid=rma.txnid 
		   where rma.rma_id='.$auction->get('rma_id'));
		if (PEAR::isError($source)) aprint_r($source);
	   list ($Y,$M,$D,$h,$m,$s) = preg_split('/[:\-\s]+/', $source->end_time);
	   $smarty->assign('source', $source);
	   $auction->data->rating_received = $source->rating_received;
	   $auction->data->rating_text_received = $source->rating_text_received;
	   $auction->data->rating_received_date = $source->rating_received_date;
	   $auction->data->rating_given = $source->rating_given;
	   $auction->data->rating_text_given = $source->rating_text_given;
	   $auction->data->rating_given_date = $source->rating_given_date;
	   $ratings_recived = Auction::listRatings($db, $dbr, $source->auction_number, $source->txnid, 'received');
	   $ratings_given = Auction::listRatings($db, $dbr, $source->auction_number, $source->txnid, 'given');
	} else {
		if ($debug) print_r($auction->data);
	   $ratings_recived = Auction::listRatings($db, $dbr, $auction->data->auction_number, $auction->data->txnid, 'received');
	   $ratings_given = Auction::listRatings($db, $dbr, $auction->data->auction_number, $auction->data->txnid, 'given');
	   list ($Y,$M,$D,$h,$m,$s) = preg_split('/[:\-\s]+/', $auction->get('end_time'));
	}   
	$smarty->assign('ratings_recived', $ratings_recived);
	$smarty->assign('ratings_given', $ratings_given);

if ($debug) echo '10: '.round((getmicrotime()-$time),3).'<br>';if ($debug) $time = getmicrotime();

		if ($auction->get('rating_received')==1) {
			$rating_received_color='green';
		} elseif ($auction->get('rating_received')==2) {
			$rating_received_color='black';
		} elseif ($auction->get('rating_received')==3 && $ratings[0]->resolved==1) {
			$rating_received_color='#FF7777';
		} elseif ($auction->get('rating_received')==3 && !$ratings[0]->resolved==1) {
			$rating_received_color='red';
		} else {
			$rating_received_color='';
		}
	$smarty->assign('rating_received_color', $rating_received_color);

	if (isset($_COOKIE["payment_default_date"])) $payment_default_date = $_COOKIE["payment_default_date"];
		if (!$payment_default_date) $payment_default_date = $auction->getLastPaymentDate();
		if (!$payment_default_date) $payment_default_date = ServerToGMT(date("Y-m-d H:i:s", time() - 24*60*60));
		$smarty->assign('payment_default_date', $payment_default_date);
	if (isset($_COOKIE["default_account"])) $default_account = $_COOKIE["default_account"];
		if (!$default_account) $default_account = $auction->getLastPaymentAccount();
		if (!$default_account) $default_account = $dbr->getOne('select min(number) from accounts where is_default');
		if ($info->get('default_account')) {
			$smarty->assign('default_account', $info->get('default_account'));
		} else {
			$smarty->assign('default_account', $default_account);
		}
	if (isset($_COOKIE["payment_default_date_free"])) $payment_default_date_free = $_COOKIE["payment_default_date_free"];
		if (!$payment_default_date_free) $payment_default_date_free = $auction->getLastPaymentDateFree();
		if (!$payment_default_date_free) $payment_default_date_free = ServerToGMT(date("Y-m-d H:i:s", time() - 24*60*60));
		$smarty->assign('payment_default_date_free', $payment_default_date_free);
	if (isset($_COOKIE["default_account_free"])) $default_account_free = $_COOKIE["default_account_free"];
		if (!$default_account_free) $default_account_free = $auction->getLastPaymentAccountFree();
		if (!$default_account_free) $default_account_free = $dbr->getOne('select min(number) from accounts where is_default');
		$smarty->assign('default_account_free', $default_account_free);
	if (isset($_COOKIE["default_vat_account_free"])) $default_vat_account_free = $_COOKIE["default_vat_account_free"];
		if (!$default_vat_account_free) $default_vat_account_free = $auction->getLastPaymentVatAccountFree();
		if (!$default_vat_account_free) $default_vat_account_free = $vat_info->vat_account_number;
		$smarty->assign('default_vat_account_free', $default_vat_account_free);
	if (isset($_COOKIE["default_selling_account_free"])) $default_selling_account_free = $_COOKIE["default_selling_account_free"];
		if (!$default_selling_account_free) $default_selling_account_free = $auction->getLastPaymentSellingAccountFree();
		if (!$default_selling_account_free) $default_selling_account_free = $vat_info->selling_account_number;
		$smarty->assign('default_selling_account_free', $default_selling_account_free);
	$smarty->assign('total_weight', $auction->getTotalWeight());
	$smarty->assign('total_volume', $auction->getTotalVolume());
        
	if (gmmktime($h, $m, $s, $M, $D+$info->get('possible_rating_until'), $Y)<time()) {
		$rating_date = 'Rating not possible';
		$rating_color = "#ff9933";
	} else {
		$rating_date = date('Y-m-d H:i:s', gmmktime($h, $m, $s, $M, $D+$info->get('possible_rating_until'), $Y));
		$rating_date .= ' ('.$dbr->getOne("select DATEDIFF('$rating_date', now())").' days left)';
		$rating_color = (gmmktime($h, $m, $s, $M, $D+90, $Y)<time()) ? 'green' : 'red' ;
	}
	$smarty->assign('rating_date', $rating_date);
	$smarty->assign('rating_color', $rating_color);
	$startComplainOver = (gmmktime($h, $m, $s, $M, $D+45, $Y)<time()) ? ' style="color:gray "' : '' ;
	$smarty->assign('startComplainOver', $startComplainOver);
	
	$resp = $dbr->getOne("SELECT username
		FROM (
		SELECT NULL rma_id, username, time
		FROM prologis_log.auction_log
		WHERE auction_number = $number
		AND txnid = $txnid
		UNION ALL 
		SELECT rma_id, responsible_uname username, create_date time
		FROM rma
		WHERE auction_number = $number
		AND txnid = $txnid
		)t
		ORDER BY rma_id DESC , time DESC");
	if (PEAR::isError($resp)) aprint_r($resp);
	if (isset($users[$resp])) $resp = $users[$resp];
	$resp = translate($db, $dbr, $resp, $deflang);
	$smarty->assign('resp', $resp);

	$smarty->assign('template_names', $dbr->getAssoc('select name, `desc` from template_names where not hidden and not old order by `desc`'));
	
if ($debug) echo '11: '.round((getmicrotime()-$time),3).'<br>';if ($debug) $time = getmicrotime();

    $smarty->assign('alias', $dbr->getOne('select name from offer_name where id='.$auction->get('name_id')));
if ($debug) echo '11.0.1: '.round((getmicrotime()-$time),3).'<br>';if ($debug) $time = getmicrotime();
	$auction->data->country_invoice = translate($db, $dbr, $auction->data->country_invoice, $auction->getMyLang(), 'country', 'name');
if ($debug) echo '11.0.2: '.round((getmicrotime()-$time),3).'<br>';if ($debug) $time = getmicrotime();
	$auction->data->country_shipping = translate($db, $dbr, $auction->data->country_shipping, $auction->getMyLang(), 'country', 'name');
if ($debug) echo '11.0.3: '.round((getmicrotime()-$time),3).'<br>';if ($debug) $time = getmicrotime();
    $smarty->assign('auction', $auction->data);
	$criteria['base_auction_number']=$auction->get('auction_number').'/'.$auction->get('txnid');
    ### for fast $second_chanced = Auction::findSecondChanced($db, $dbr,$criteria);
    $smarty->assign('second_chanced', $second_chanced);
if ($debug) echo '11.0.4: '.round((getmicrotime()-$time),3).'<br>';if ($debug) $time = getmicrotime();
	$comments = $auction->getComments();
if ($debug) echo '11.0.5: '.round((getmicrotime()-$time),3).'<br>';if ($debug) $time = getmicrotime();

	foreach($comments as $k=>$r) {
		$comments[$k]->comment = str_replace("\r\n",'<br>',$comments[$k]->comment);
		$comments[$k]->cid = str_replace("Ticket#", "", $comments[$k]->prefix);
	}

    $smarty->assign('comments', $comments);
    $smarty->assign('comments_show_from', count($comments)-Config::get($db, $dbr,'comments_per_page'));
	$smarty->assign('email_log_show_from', count($emailLog)-Config::get($db, $dbr,'email_log_per_page'));
    $smarty->assign('config', Config::getAll($db, $dbr));
    $smarty->assign('currency', siteToSymbol($auction->get('siteid')));
if ($debug) echo '11.1: '.round((getmicrotime()-$time),3).'<br>';if ($debug) $time = getmicrotime();
    $total = Payment::total($db, $dbr, $auction->get('auction_number'), $auction->get('txnid'));
if ($debug) echo '11.2: '.round((getmicrotime()-$time),3).'<br>';if ($debug) $time = getmicrotime();
    $due = $invoice->get('total_price') + $invoice->get('total_shipping') + $invoice->get('total_cod');
//	echo "total=$total; due=$due";
	if ($auction->get('lang')) {
		switch ($auction->get('lang')) {
			case 'polish':
				setlocale(LC_TIME, 'pl_PL');
				break;
			case 'german':
				setlocale(LC_TIME, 'de_DE');
				break;
			case 'english':
				setlocale(LC_TIME, 'en_EN');
				break;
			case 'french':
				setlocale(LC_TIME, 'fr_FR');
				break;
			case 'spanish':
				setlocale(LC_TIME, 'es_ES');
				break;
		}
	} else {
		if ($auction->get('siteid')==3)
			setlocale(LC_TIME, 'en_EN');
		elseif ($auction->get('siteid')==77)
			setlocale(LC_TIME, 'de_DE');
	}
	$smarty->assign('packets', $dbr->getAssoc('select id, name from tn_packets where dontshow=0 and inactive=0'));	
if ($debug) echo '11.3: '.round((getmicrotime()-$time),3).'<br>';if ($debug) $time = getmicrotime();

	if (0/*$offer_id = $auction->get('offer_id')*/) {
		$autos = op_Auto::listByOffer($db, $dbr, (int)$offer_id);
		$warehouses_table = Warehouse::listArray($db, $dbr);
		$warehouses_table[0] = 'Total';
		if (count($autos)) foreach ($autos as $id => $line) {
			$autos[$id]->pieces = array();
			$autos[$id]->available = array();
			$autos[$id]->reserved = array();
			$autos[$id]->sold90 = array();
			foreach ($warehouses_table as $wareid=>$dummy) {
				$fname = 'pieces_'.$wareid;
				if ($sort==$fname) 
					$smarty->assign('dirpieces', array($wareid => -$direction));
				$autos[$id]->pieces[$wareid] = $autos[$id]->$fname;
				$fname = 'available_'.$wareid;
				if ($sort==$fname) 
					$smarty->assign('diravailable', array($wareid => -$direction));
				$autos[$id]->available[$wareid] = $autos[$id]->$fname;
				$fname = 'reserved_'.$wareid;
				if ($sort==$fname) 
					$smarty->assign('dirreserved', array($wareid => -$direction));
				$autos[$id]->reserved[$wareid] = $autos[$id]->$fname;
				$fname = 'sold90_'.$wareid;
				if ($sort==$fname) 
					$smarty->assign('dirsold90', array($wareid => -$direction));
				$autos[$id]->sold90[$wareid] = $autos[$id]->$fname;
			}
			if ($autos[$id]->desired_daily < 0) $autos[$id]->sales_per_day = 'STOP';
			else
				$autos[$id]->sales_per_day =  max(((int)(ArticleHistory::getSoldCountMonth($db, $dbr, $autos[$id]->article_id, 1)/0.3)/100), $autos[$id]->desired_daily);
			$autos[$id]->sales_in_period = $autos[$id]->sales_per_day*$autos[$id]->period;
	  		$autos[$id]->quantity_to_order = ($autos[$id]->sales_per_day * ($autos[$id]->period+$reserve)) 
				- ($autos[$id]->available[0]
				+ $autos[$id]->order_in_prod_qnt
				+ $autos[$id]->order_on_way_qnt);
			if ($autos[$id]->active && $autos[$id]->quantity_to_order>0 && $autos[$id]->sales_per_day!='STOP')
				  $autos[$id]->bgcolor = Config::get($db, $dbr, 'op_active_color'); 
			elseif (!$autos[$id]->active && $autos[$id]->quantity_to_order>0 && $autos[$id]->sales_per_day!='STOP')
				  $autos[$id]->bgcolor = Config::get($db, $dbr, 'op_passive_color'); 
			elseif ($autos[$id]->active && $autos[$id]->quantity_to_order<=0)
				  $autos[$id]->bgcolor = Config::get($db, $dbr, 'op_active_noitems_color'); 
			elseif (!$autos[$id]->active && $autos[$id]->quantity_to_order<=0)
				  $autos[$id]->bgcolor = Config::get($db, $dbr, 'op_passive_noitems_color'); 
			$autos[$id]->volume_to_order = $autos[$id]->quantity_to_order * $autos[$id]->article_volume_per_single_unit;
		}
		$smarty->assign('autos', $autos);
		$smarty->assign('countautos', count($autos));
		$smarty->assign('warehouses_table', $warehouses_table);
	}	
	$smarty->assign('docs', Auction::getDocs($db, $dbr, $number, $txnid));
if ($debug) echo '11.4: '.round((getmicrotime()-$time),3).'<br>';if ($debug) $time = getmicrotime();
  	$offer = new Offer($db, $dbr, $auction->get('offer_id'));

if ($debug) echo '12: '.round((getmicrotime()-$time),3).'<br>';if ($debug) $time = getmicrotime();

	$default_warehouse = $auction->get('send_warehouse_id');
	if (!(int)$default_warehouse && $auction->get('offer_id')) $default_warehouse = $offer->get('default_warehouse_id');
	if (!(int)$default_warehouse) $default_warehouse = $info->get('default_warehouse_id');
	if (!(int)$default_warehouse) $default_warehouse = Warehouse::getDefault($db, $dbr);
    $smarty->assign('default_warehouse', $default_warehouse);

    $smarty->assign('info', $info);
	$smarty->assign('sofo_payments', $auction->sfPayments());
	$smarty->assign('pp_payments', $auction->ppPayments());
	$smarty->assign('ps_payments', $auction->psPayments());
	$smarty->assign('bill_payments', $auction->billPayments());
	$smarty->assign('gc_payments', $auction->gcPayments());
	$smarty->assign('bs_payments', $auction->bsPayments());
	$smarty->assign('p24_payments', $auction->p24Payments());
	
	$purchased='Auction';
	if ($txnid==2 || $txnid>3) {
		$purchased='Fix';
	} elseif ($txnid==1) {
		$purchased='Auction';
	} elseif ($txnid==3) {
		if ($auction->get('main_auction_number')) $aunumber=$auction->get('main_auction_number');
			else $aunumber=$auction->get('auction_number');
		$auserv = new Auction($db, $dbr, $aunumber, $txnid);
		if ($server = $auserv->get('server')) {
			$shop = $dbr->getRow("select id, name from shop where url='$server' or url='www.$server'
				or CONCAT('www.',url)='$server'");
			if (strlen($shop->name)) {
				$purchased='Shop: '.$shop->name;
				$shop_id = $shop->id;
				$src='';
			}
		} else {
				$purchased='Shop';
				$src='';
		}
	}
	$src = $dbr->getOne("select fget_AType(".$auction->get('auction_number').",".$auction->get('txnid').")");
	$smarty->assign('purchased', $purchased);
	$smarty->assign('shop_id', $shop_id);
	$smarty->assign('src', $src);
	$methodsall = ShippingMethod::listAll($db, $dbr);
	$smarty->assign('methodsall', $methodsall);
	$smarty->assign('reserve', 1);

    if ($_POST['new_ai_add'] && (int)$_POST['offer_id'] && (int)$_POST['new_sa_quantity']) {
			$new_auction_number = $dbr->getOne(
		            "SELECT IFNULL(MAX(auction_number+1), 1) FROM auction
					WHERE txnid = 3 
					and not IFNULL(custom_auction_number,0)");
				$already_exists = $dbr->getOne(
			            "SELECT count(*) FROM auction 
						WHERE auction_number=$new_auction_number");
				while ($already_exists) {
					$new_auction_number++;
					$already_exists = $dbr->getOne(
				            "SELECT count(*) FROM auction 
							WHERE auction_number=$new_auction_number");
				}
		    $new_auction = new Auction($db, $dbr);
		    $new_auction->set('response_username', $loggedUser->get('username'));
		    $new_auction->set('username', $auction->get('username'));
		    $new_auction->set('username_buyer', $auction->get('username_buyer'));
		    $new_auction->set('email', $auction->get('email'));
		    $new_auction->set('auction_number', $new_auction_number);
		    $new_auction->set('end_time', ServerTolocal(date("Y-m-d H:i:s"), $timediff));
		    $new_auction->set('txnid', $auction->get('txnid'));
		    $new_auction->set('quantity', (int)$_POST['new_sa_quantity']);
		    $new_auction->set('winning_bid', (int)$_POST['new_sa_price']);
		    $new_auction->set('customer_id', $auction->get('customer_id')); # asked by "Thomas Redner" <redner@beliani.de>
		    $new_auction->set('offer_id', (int)$_POST['offer_id']);
		    $new_auction->set('saved_id', (int)$_POST['saved_id']);
		    $new_auction->set('siteid', $auction->get('siteid'));
		    $new_auction->set('process_stage', STAGE_ORDERED);
		    $new_auction->set('company_invoice', $auction->get('company_invoice'));
		    $new_auction->set('company_shipping', $auction->get('company_shipping'));
		    $new_auction->set('gender_invoice', $auction->get('gender_invoice'));
		    $new_auction->set('gender_shipping', $auction->get('gender_shipping'));
		    $new_auction->set('firstname_invoice', $auction->get('firstname_invoice'));
		    $new_auction->set('firstname_shipping', $auction->get('firstname_shipping'));
		    $new_auction->set('name_invoice', $auction->get('name_invoice'));
		    $new_auction->set('name_shipping', $auction->get('name_shipping'));
		    $new_auction->set('street_invoice', $auction->get('street_invoice'));
		    $new_auction->set('street_shipping', $auction->get('street_shipping'));
		    $new_auction->set('house_invoice', $auction->get('house_invoice'));
		    $new_auction->set('house_shipping', $auction->get('house_shipping'));
		    $new_auction->set('zip_invoice', $auction->get('zip_invoice'));
		    $new_auction->set('zip_shipping', $auction->get('zip_shipping'));
		    $new_auction->set('city_invoice', $auction->get('city_invoice'));
		    $new_auction->set('city_shipping', $auction->get('city_shipping'));
		    $new_auction->set('country_invoice', $auction->get('country_invoice'));
		    $new_auction->set('country_shipping', $auction->get('country_shipping'));
		    $new_auction->set('email_invoice', $auction->get('email_invoice'));
		    $new_auction->set('email_shipping', $auction->get('email_shipping'));
		    $new_auction->set('tel_invoice', $auction->get('tel_invoice'));
		    $new_auction->set('tel_shipping', $auction->get('tel_shipping'));
		    $new_auction->set('cel_invoice', $auction->get('cel_invoice'));
		    $new_auction->set('cel_shipping', $auction->get('cel_shipping'));
		    $new_auction->set('main_auction_number', $auction->get('auction_number'));
		    $new_auction->set('main_txnid', $auction->get('txnid'));
		//	print_r($new_auction);
		    $new_auction->update();
	   	    header('Location: auction.php?number='.$number.'&txnid='.$txnid);
    	    exit;
	}

if ('POST' == $_SERVER['REQUEST_METHOD']) {
    header('Location: ' . $_POST['return'].(strlen($result)? '&error='.$result : ''));
    exit;
}

		$smarty->assign('shop_id', $shop_id);
		$qrystr = "select id, name from source_seller
			where not inactive and username='". $auction->get('username')."'
			order by name";
		$sources = $dbr->getAssoc($qrystr);
	   	$smarty->assign('sources', $sources);

	if ($auction->get('customer_id')) {
		$customer_lang = $dbr->getOne("select lang from customer".$au_type." where id='".$auction->get('customer_id')."'");
		$langs = getLangsArray($db, $dbr);
		$smarty->assign('customer_lang', $langs[$customer_lang]);
	}
		$q = "select u.username username
			, tl.updated
			from total_log tl
			left join users u on u.system_username=tl.username
			where tl.table_name='auction' and tl.field_name='shipping_username' and tl.tableid=".$auction->get('id')."
			order by tl.updated desc limit 1";
		$shipping_username = $dbr->getRow($q);
		if ($shipping_username->username) 
			$smarty->assign('shipping_username_changed', $shipping_username);
	$smarty->assign('pickup', in_array($auction->get('payment_method'), array(3,4))?1:0);

if ($debug) echo '13: '.round((getmicrotime()-$time),3).'<br>';if ($debug) $time = getmicrotime();

	$alarm = $dbr->getRow("select IFNULL(alarms.type,'auction') `type`, auction.id type_id
		, alarms.status, alarms.date, alarms.comment
		from auction
		left join alarms on alarms.type='auction' 
		and alarms.type_id=auction.id 
		and alarms.username='".$loggedUser->get('username')."'
		where auction.id=".$auction->get('id')."
		");
	$smarty->assign("alarm", $alarm);
	$alarms = $dbr->getAll("select alarms.id, IFNULL(alarms.type,'auction') `type`, auction.id type_id
		, alarms.status, alarms.date, alarms.comment
		, (select updated from total_log where table_name='alarms' and tableid=alarms.id limit 1) created
		, IFNULL(users.name, alarms.username) fullusername
		, users.username username
		from auction
		join alarms on alarms.type='auction' and alarms.type_id=auction.id
		left join users on alarms.username=users.username
		where auction.id=".$auction->get('id'));
	$smarty->assign("alarms", $alarms);

	if ($auction->data->txnid==3) $where_txnid=" and txnid=3 ";
		else $where_txnid=" and txnid<>3 ";
	if ($auction->data->customer_id) {
		$other_orders = $dbr->getAll("select auction_number, txnid 
		from auction where id<>{$auction->data->id} and customer_id={$auction->data->customer_id}
		and main_auction_number=0 and deleted=0
		$where_txnid");
		$smarty->assign('other_orders_from', count($other_orders)-10);
		$smarty->assign("other_orders", $other_orders);
	}
	$customer = $dbr->getRow("select * from customer{$src} where id={$auction->data->customer_id}");
	$smarty->assign("customer", $customer);
	
	$priority = $auction->data->priority;
	foreach($allorder as $order) {
		if (!$priority) $priority = $order->priority;
	}
	$smarty->assign("priority", $priority);
	
	$priority_row = $dbr->getRow("select tl.updated, IFNULL(u.name, tl.username) username
		from total_log tl
		left join users u on u.system_username=tl.username
		where table_name='auction' and field_name in ('priority_comment','priority') and tableid=".$auction->get('id')."
		order by updated desc limit 1");
	$smarty->assign("priority_row", $priority_row);
	$smarty->assign("today", date('Y-m-d'));
	if ($loggedUser->get('show_ware_distances')
			/*&& $invoice->get('open_amount')<=0 && $auction->get('delivery_date_real')=='0000-00-00 00:00:00'*/) {
		require_once 'XML/Unserializer.php';
		$near_text = array();
		$q = "select warehouse.*, CONCAT(warehouse.country_code, ': ', warehouse.name) fullname
			from warehouse 
			join customer_shipping2warehouse s2w on s2w.warehouse_id=warehouse.warehouse_id
			where (warehouse.limit_km or warehouse.limit_min)
			and s2w.country_code='".CountryToCountryCode($main_auction->data->country_shipping)."'
			/*and (warehouse.country_code='".$main_auction->data->country_shipping."' 
				or warehouse.country_code='".CountryToCountryCode($main_auction->data->country_shipping)."')*/";
		$seller_warehouses = $dbr->getAll($q);
		$mindist = 999999999999; $mikdistk = 0;
		foreach($seller_warehouses as $k=>$warehouse) {
			$auction_warehouse_distance = $dbr->getRow("select * from auction_warehouse_distance 
				where auction_number=".$main_auction->get('auction_number')." and txnid=".$main_auction->get('txnid')."
				 and warehouse_id=".$warehouse->warehouse_id);
			if ($auction_warehouse_distance->distance) {
				if (!($auction_warehouse_distance->distance*1)) {
					unset($seller_warehouses[$k]);
				} else {
					if ((1*$auction_warehouse_distance->distance) < $mindist) {
						$mindist = 1*$auction_warehouse_distance->distance; 
						$mikdistk = $k;
					}
					$seller_warehouses[$k]->distance = $auction_warehouse_distance->distance;
					$seller_warehouses[$k]->distance_txt = $seller_warehouses[$k]->distance." km";
					$seller_warehouses[$k]->duration = $auction_warehouse_distance->duration;
					$seller_warehouses[$k]->duration_txt = $seller_warehouses[$k]->duration." mins";
					if ($seller_warehouses[$k]->limit_km>=$auction_warehouse_distance->distance 
						|| $seller_warehouses[$k]->limit_min>=$auction_warehouse_distance->duration) {
						$seller_warehouses[$k]->color = 'red';
					} else {
						$seller_warehouses[$k]->color = 'black';
					}
				}
			} else {
				$seller_warehouses[$k]->distance_txt = 'Warehouse '.$warehouse->name.' still was not read or address incorrect';
			}
			if (($seller_warehouses[$k]->distance>0 && $seller_warehouses[$k]->distance<=$seller_warehouses[$k]->limit_km)) {
				$seller_warehouses_neigbourhood .= "Distance to ".$seller_warehouses[$k]->fullname." less than ".$seller_warehouses[$k]->limit_km." km;".'\n\r';
			}
			if (($seller_warehouses[$k]->duration>0 && $seller_warehouses[$k]->duration<=$seller_warehouses[$k]->limit_min)) {
				$seller_warehouses_neigbourhood .= "Duration to ".$seller_warehouses[$k]->fullname." less than ".$seller_warehouses[$k]->limit_min." minutes;".'\n\r';
			}
		} // foreach ware
//		$seller_warehouses[$mikdistk]->color = 'red';
	} // if ready to ship
//	print_r($seller_warehouses);
//	echo 'seller_warehouses_neigbourhood='.$seller_warehouses_neigbourhood;
	$smarty->assign("seller_warehouses", $seller_warehouses);
	$smarty->assign("seller_warehouses_neigbourhood", $seller_warehouses_neigbourhood);

	$is_shipped = $dbr->getOne("select count(*) from orders where manual not in (2,3,4) and sent and auction_number=".$auction->get('auction_number')
		." and txnid=".$auction->get('txnid'));
	$smarty->assign("is_shipped", $is_shipped);
	$payment_method = $dbr->getOne("select value from translation where id=
		(select id from payment_method where code='".$auction->get('payment_method')."')
		and table_name='payment_method' and field_name='name'
		and language='".$auction->getMyLang()."'");
	$smarty->assign("payment_method", $payment_method);

if ($debug) echo '14: '.round((getmicrotime()-$time),3).'<br>';if ($debug) $time = getmicrotime();

	$invoices = $dbr->getAll('select * from auction where based_on_auction_id='.$auction->get('id'));
	foreach ($invoices as $key=>$dummy) {
		$tnnumbers = $dbr->getAll("select IFNULL(u.name, n.username) as username, n.shipping_date, n.number, 
				p.name as packet_name, p.id packet_id
			from tracking_numbers n 
			LEFT JOIN tn_packets p ON p.id=n.packet_id
			LEFT JOIN users u ON  u.username=n.username
			where n.auction_number=".$invoices[$key]->auction_number
			." and n.txnid=".$invoices[$key]->txnid);
		$invoices[$key]->shipping = $tnnumbers;
		$invoices[$key]->shipping_data = $dbr->getRow("select IFNULL(u.name, i.shipping_username) shipping_username
		, fget_delivery_date_real(au.auction_number, au.txnid) delivery_date_real 
			from auction au 
			join invoice i on au.invoice_number=i.invoice_number
			LEFT JOIN users u ON  u.username=i.shipping_username
			where au.auction_number=".$invoices[$key]->auction_number
			." and au.txnid=".$invoices[$key]->txnid);
			if (PEAR::isError($invoices[$key]->shipping_data)) aprint_r($invoices[$key]->shipping_data);
	}
	$smarty->assign('invoices', $invoices);
	if ($auction->get('based_on_auction_id')) {
		$based_on = $dbr->getRow("select auction_number, txnid from auction where id=".$auction->get('based_on_auction_id'));
		$smarty->assign('based_on', $based_on);
	}

	$shipping_list_already_printed = $dbr->getOne("select COUNT(*) from printer_log where auction_number=".$auction->get('auction_number')."
		 and txnid=".$auction->get('txnid')." and `action` in ('Print shipping list', 'Print not sent packing list in PDF')");
	$smarty->assign("shipping_list_already_printed", $shipping_list_already_printed);

$langs = getLangsArray($db, $dbr);
$smarty->assign('langs', $langs);
	$smarty->assign('lang', $auction->getMyLang());

	$offers_array = $dbr->getAssoc("select offer_id, name
		from offer
		where not hidden and not old
		order by name");
    $smarty->assign('offers_array', $offers_array);

if ($debug) echo '15: '.round((getmicrotime()-$time),3).'<br>';if ($debug) $time = getmicrotime();
	
	$smarty->assign('ask_for_vat', $dbr->getOne("select value from config_api where par_id=22 and siteid=".$auction->get('siteid')));
	$smarty->assign('ask_for_state', $dbr->getOne("select value from config_api where par_id=23 and siteid=".$auction->get('siteid')));
	
	$smarty->assign('rma_notif', $dbr->getOne("select count(*) from rma_notif where obj='auction' and auction_id=".$auction->get('id')." and username='".$loggedUser->get('username')."'"));

	$voucher_log = EmailLog::listAll($db, $dbr, $auction->get('auction_number'), $auction->get('txnid'), " and template='ticket_voucher_email' ");
	foreach($voucher_log as $log_id=>$log) {
		list($voucher_log[$log_id]->code_id, $voucher_log[$log_id]->template_id) = explode('_', $voucher_log[$log_id]->notes);
		if ((int)$voucher_log[$log_id]->code_id) {
			$row = $dbr->getRow("select * from shop_promo_codes where id=".(int)$voucher_log[$log_id]->code_id);
			$voucher_log[$log_id]->name = $row->name;
			$voucher_log[$log_id]->code = $row->code;
		}
		if ((int)$voucher_log[$log_id]->template_id) {
			$voucher_log[$log_id]->code_template = $dbr->getOne("select name from shop_promo_template where id=".(int)$voucher_log[$log_id]->template_id);
			$voucher_log[$log_id]->code_template_shop_id = $dbr->getOne("select shop_id from shop_promo_template where id=".(int)$voucher_log[$log_id]->template_id);
		}
	}
	$smarty->assign('voucher_log', $voucher_log);

	$source_seller = $dbr->getRow("select * from source_seller
			where id=".$auction->get('source_seller_id'));
	$smarty->assign('source_seller', $source_seller);
	$voucher_days = array(30=>30, 90=>90, 180=>180, 360=>360, 720=>720);
	$smarty->assign('voucher_days', $voucher_days);
	$_config_api_par = $dbr->getAssoc("SELECT cap.id AS par_id, (
				SELECT MAX(ca.value)
				FROM config_api ca
				WHERE ca.par_id = cap.id
				AND ca.siteid = '".$auction->data->siteid."'
				) AS par_value
				FROM config_api_par cap
				where cap.id<>6");
	$smarty->assign('_config_api_par', $_config_api_par);
	$ss = $dbr->getRow("select * from source_seller where id=".$auction->get('source_seller_id'));
	$smarty->assign('ss', $ss);
	if ($auction->get('payment_method')==2) {
		$smarty->assign('subtitle', '<br><span style="color:red; font-weight:bold">Nachnahme / COD</span>');
	}
	$smarty->assign('personal', $dbr->getRow("select responsible_uname as responsible_uname from auction where id=".$auction->get('id')));
	$employees = $dbr->getAssoc("select id, concat(name, ' ', name2) from employee where inactive=0 order by 2");
	$smarty->assign('employees', $employees);

	$emp_id_log_row = $dbr->getRow("select tl.updated, IFNULL(u.username, tl.username) username
		from total_log tl
		left join users u on u.system_username=tl.username
		where table_name='auction' and field_name in ('emp_id') and tableid=".$auction->get('id')."
		order by updated desc limit 1");
	$smarty->assign("emp_id_log_row", $emp_id_log_row);

	$shipping_users = $dbr->getAssoc("select username, name from users where use_as_shipping_company<>0 and deleted=0 order by name");
	$smarty->assign("shipping_users", $shipping_users);
	
	$all_wwos = $dbr->getAssoc("select id, concat('WWO#',id) f1 from ww_order order by id");
	$smarty->assign("all_wwos", $all_wwos);
	$active_wwos = $dbr->getAssoc("select id, concat('WWO#',id) f1 from ww_order where closed=0 order by id");
	$smarty->assign("active_wwos", $active_wwos);
	$users_wwos = $dbr->getAssoc("select distinct u.username, u.name
		from ww_order wwo
		join users u on u.username=wwo.username
		where wwo.closed=0
		order by u.name");
	$smarty->assign("users_wwos", $users_wwos);

    $smarty->display('auction.tpl');

}

/*
    require('GoogleMapAPI.class.php');
    $map = new GoogleMapAPI('map');
    $map->setDSN('mysql://USER:PASS@localhost/GEOCODES');
    $map->setAPIKey('YOURGOOGLEMAPKEY'); 
	$geocode = $map->geoGetCoords('237 S 70th suite 220 Lincoln NE 68510');
        
        echo $geocode['lat'];
        echo $geocode['lon']; 
 geoGetDistance($lat1,$lon1,$lat2,$lon2,$unit)
    ---------------------------------------------

        This gets the distance between too coordinate points using the great
        circle formula. $unit can be M (miles),K (kilometers),N (nautical
        miles),I (inches), or F (feet). Default is M.   
        
        Example:
        
        $distance = $map->geoGetDistance($lat1,$lon1,$lat2,$lon2,$unit); 

*/
?>